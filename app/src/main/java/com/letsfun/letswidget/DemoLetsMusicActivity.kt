package com.letsfun.letswidget

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.letsfun.letswidget.demo.core.letsmusic.*
import com.letsfun.letswidget.databinding.DemoActivityLetsMusicBinding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity


class DemoLetsMusicActivity : BaseViewBindingActivity<DemoActivityLetsMusicBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun getViewBinding(): DemoActivityLetsMusicBinding {
        return DemoActivityLetsMusicBinding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_lets_music)
    }

    fun s01Click(view: View) {
        startActivity(Intent(this, DemoCoreS01Activity::class.java))
    }

    fun s02Click(view: View) {
        startActivity(Intent(this, DemoCoreS02Activity::class.java))
    }

    fun s03Click(view: View) {
        startActivity(Intent(this, DemoCoreS03Activity::class.java))
    }

    fun s04Click(view: View) {
        startActivity(Intent(this, DemoCoreS04Activity::class.java))
    }

    fun s05Click(view: View) {
        startActivity(Intent(this, DemoCoreS05Activity::class.java))
    }

    fun g01Click(view: View) {
        startActivity(Intent(this, DemoCoreG01Activity::class.java))
    }

    fun g02Click(view: View) {
        startActivity(Intent(this, DemoCoreG02Activity::class.java))
    }

    fun g03Click(view: View) {
        startActivity(Intent(this, DemoCoreG03Activity::class.java))
    }

    fun g04Click(view: View) {
        startActivity(Intent(this, DemoCoreG04Activity::class.java))
    }

    fun n01Click(view: View) {
        startActivity(Intent(this, DemoCoreN01Activity::class.java))
    }

    fun n02Click(view: View) {
        startActivity(Intent(this, DemoCoreN02Activity::class.java))
    }

    fun n03Click(view: View) {
        startActivity(Intent(this, DemoCoreN03Activity::class.java))
    }

    fun d01Click(view: View) {
        startActivity(Intent(this, DemoCoreD01Activity::class.java))
    }

    fun d02Click(view: View) {
        startActivity(Intent(this, DemoCoreD02Activity::class.java))
    }

    fun e01Click(view: View) {
        startActivity(Intent(this, DemoCoreE01Activity::class.java))
    }

    fun e02Click(view: View) {
        startActivity(Intent(this, DemoCoreE02Activity::class.java))
    }

    fun e03Click(view: View) {
        startActivity(Intent(this, DemoCoreE03Activity::class.java))
    }

    fun f01Click(view: View) {
        startActivity(Intent(this, DemoCoreF01Activity::class.java))
    }

    fun f02Click(view: View) {
        startActivity(Intent(this, DemoCoreF02Activity::class.java))
    }

    fun f03Click(view: View) {
        startActivity(Intent(this, DemoCoreF03Activity::class.java))
    }
}