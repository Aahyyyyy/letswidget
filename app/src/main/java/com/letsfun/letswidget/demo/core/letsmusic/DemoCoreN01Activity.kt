package com.letsfun.letswidget.demo.core.letsmusic

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityNavigationN01Binding
import com.letsfun.letswidget.demo.core.letsmusic.fragment.FragmentDemo
import com.letsfun.letswidget.demo.core.letsmusic.fragment.ViewPagerTestFragment
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.butler.LetsBaseAppButler
import com.letsfun.letswidget.sdk.core.toast.LetsToast

class DemoCoreN01Activity :BaseViewBindingActivity<DemoCoreActivityNavigationN01Binding>() {
    private var butlerItems: MutableList<LetsBaseAppButler.ButlerItem> = ArrayList()
    private val fragmentList = with(ArrayList<Fragment>()) {
        add(FragmentDemo("关注"))
        add(FragmentDemo("发现"))
        add(FragmentDemo("同城"))
        add(FragmentDemo("交友"))
        add(FragmentDemo("啊啊"))
        return@with this
    }

    override fun getViewBinding(): DemoCoreActivityNavigationN01Binding {
        return DemoCoreActivityNavigationN01Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_n01_title)
    }

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initAppButler()
        initButlers()
        initViewPagerView()

        val list = listOf(
            "关注",
            "推荐",
            "最新",
            "同城",
            "交友"
        )

        v.letsAppButler.letsCommunityButler.tabLayout.setMultiTabEmpty(list.size) {
                position ->  list[position]
        }
        v.letsAppButler.letsCommunityButler.tabLayout.selectTabView(0)
        v.letsAppButler.letsCommunityButler.tabLayout.setTabClickListener {
            LetsToast.showToast(it.toString(), Toast.LENGTH_SHORT)
        }

        val friendList = listOf(
            "通讯录",
            "消息"
        )

        v.letsAppButler.letsFriendButler.tabLayout.setMultiTabEmpty(friendList.size) {
                position ->  friendList[position]
        }
        v.letsAppButler.letsFriendButler.tabLayout.selectTabView(0)
        v.letsAppButler.letsFriendButler.tabLayout.setTabClickListener {
            LetsToast.showToast(it.toString(), Toast.LENGTH_SHORT)
        }
    }

    private fun initAppButler() {
        v.letsAppButler.letsBaseAppButler.setOnButlerListener(object : LetsBaseAppButler.OnButlerListener {
            override fun onButlerClicked(butlerItem: LetsBaseAppButler.ButlerItem, position: Int) {
                v.letsAppButler.updateButlerView()
                if (position == 1) {
                    v.letsAppButler.letsBaseAppButler.setMessageCount(3, 999)
                }
                if (position == 3) {
                    v.letsAppButler.letsBaseAppButler.setMessageCount(3, 0)
                }
            }

            override fun onButlerSelected(butlerItem: LetsBaseAppButler.ButlerItem, position: Int) {
            }
        })
        v.letsAppButler.letsBaseAppButler.setOnDoubleClickListener(object :
            LetsBaseAppButler.OnDoubleClickListener {
            override fun onDoubleClick(butlerItem: LetsBaseAppButler.ButlerItem, position: Int) {
                v.letsAppButler.showBaseCustomButler(position)
            }
        })
        v.letsAppButler.letsBaseAppButler.setOnInterruptSelectListener(object :
            LetsBaseAppButler.OnInterruptSelectListener {
            override fun onInterrupt(butlerItem: LetsBaseAppButler.ButlerItem, position: Int): Boolean {
                if (position == 0) {
                    Toast.makeText(context, "onInterrupt $position", Toast.LENGTH_SHORT).show()
                    return true
                }
                return false
            }
        })
    }

    private fun initButlers() {
        butlerItems.add(
            LetsBaseAppButler.ButlerItem(
                R.drawable.demo_core_cn_icon_discover_s, "发现", 0, null, "lottie/cn-discover.json"
            )
        )
        butlerItems.add(
            LetsBaseAppButler.ButlerItem(
                R.drawable.demo_core_cn_icon_friend_s, "朋友", 0, null, "lottie/cn-friends.json"
            )
        )
        butlerItems.add(
            LetsBaseAppButler.ButlerItem(
                R.drawable.demo_core_cn_icon_car_s, "", 0, null, "lottie/cn-car.json"
            )
        )
        butlerItems.add(
            LetsBaseAppButler.ButlerItem(
                R.drawable.demo_core_cn_icon_surprised_s, "惊喜", 0, null, "lottie/cn-surprised.json"
            )
        )
        butlerItems.add(
            LetsBaseAppButler.ButlerItem(
                R.drawable.demo_core_cn_icon_mine_s, "我的", 0, null, "lottie/cn-mine.json"
            )
        )

        v.letsAppButler.setButlerItems(butlerItems)
    }

    private fun initViewPagerView() {
        v.viewPager2.adapter = TestPageAdapter()
        v.viewPager2.isUserInputEnabled = false
        v.letsAppButler.letsBaseAppButler.setupWithViewPager(v.viewPager2)
    }

    inner class TestPageAdapter2 : FragmentStateAdapter(this) {
        override fun getItemCount(): Int {
            return fragmentList.size

        }

        override fun createFragment(position: Int): Fragment {
            return fragmentList[position]
        }
    }

    inner class TestPageAdapter : FragmentStateAdapter(this) {
        val fragments = listOf<Fragment>(
            ViewPagerTestFragment(),
            ViewPagerTestFragment(),
            ViewPagerTestFragment(),
            ViewPagerTestFragment(),
            ViewPagerTestFragment()
        )

        override fun getItemCount(): Int = butlerItems.size

        override fun createFragment(position: Int): Fragment {
            if (position == 1) {
                return ViewPagerTestFragment.newInstance(position.toString())
            } else {
                return fragments[position]
            }
        }
    }
}