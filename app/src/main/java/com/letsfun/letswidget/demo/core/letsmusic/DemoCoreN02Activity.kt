package com.letsfun.letswidget.demo.core.letsmusic

import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityNavigationN02Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class DemoCoreN02Activity : BaseViewBindingActivity<DemoCoreActivityNavigationN02Binding>() {
    override fun getViewBinding(): DemoCoreActivityNavigationN02Binding {
        return DemoCoreActivityNavigationN02Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_n02_title)
    }

}