package com.letsfun.letswidget.demo.core.letsmusic.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.letsfun.letswidget.R;

public class FragmentDemo extends Fragment {
    private String title;

    public FragmentDemo(String title) {
        this.title = title;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.demo_core_fragment_demo, container, false);
        TextView textView = view.findViewById(R.id.textView);
        textView.setText(title);

        return view;
    }

    public String getTitle() {
        return title;
    }
}
