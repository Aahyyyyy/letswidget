package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.view.View
import com.google.android.material.tabs.TabLayout
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityStandardS02Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class DemoCoreS02Activity : BaseViewBindingActivity<DemoCoreActivityStandardS02Binding>(), TabLayout.OnTabSelectedListener {
    override fun getViewBinding(): DemoCoreActivityStandardS02Binding {
        return DemoCoreActivityStandardS02Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_s02_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        tabLayout.addTab(tabLayout.newTab().setText("字号"))
        tabLayout.addTab(tabLayout.newTab().setText("行高"))
        tabLayout.addOnTabSelectedListener(this)

        showView(0)
    }

    private fun showView(position: Int) {
        hideAllViews()
        when (position) {
            0 -> v.vgPage0.visibility = View.VISIBLE
            1 -> v.vgPage1.visibility = View.VISIBLE
        }
    }

    private fun hideAllViews() {
        v.vgPage0.visibility = View.GONE
        v.vgPage1.visibility = View.GONE
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        showView(tab!!.position)
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }
}