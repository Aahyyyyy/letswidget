package com.letsfun.letswidget.demo.core.letsmusic.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.letsfun.letswidget.R

class ViewPagerTestFragment : Fragment() {
    companion object {
        fun newInstance(text: String): Fragment {
            return ViewPagerTestFragment().apply {
                val bundle = Bundle()
                bundle.putString("text", text)
                arguments = bundle
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.demo_core_fragment_viewpager_test, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val mText = arguments?.getString("text")
//        view.findViewById<TextView>(R.id.text).apply {
//            text = mText
//        }
    }
}