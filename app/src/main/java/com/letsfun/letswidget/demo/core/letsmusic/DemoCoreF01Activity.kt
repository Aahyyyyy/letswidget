package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.view.View
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityFeedbackF01Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class DemoCoreF01Activity : BaseViewBindingActivity<DemoCoreActivityFeedbackF01Binding>() {
    override fun getViewBinding(): DemoCoreActivityFeedbackF01Binding {
        return DemoCoreActivityFeedbackF01Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_f01_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun onPageLoading(view: View) {
        DemoCoreF01SubActivity.startActivity(this, DemoCoreF01SubActivity.LOADING_TYPE11)
    }

    fun onToastLoading1(view: View) {
        DemoCoreF01SubActivity.startActivity(this, DemoCoreF01SubActivity.LOADING_TYPE21)
    }

    fun onToastLoading2(view: View) {
        DemoCoreF01SubActivity.startActivity(this, DemoCoreF01SubActivity.LOADING_TYPE22)
    }

}