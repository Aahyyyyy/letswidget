package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.view.View
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityDisplayD02Binding
import com.letsfun.letswidget.sdk.core.badge.LetsBadgeFactory
import com.letsfun.letswidget.sdk.core.badge.LetsBadgeType
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class DemoCoreD02Activity : BaseViewBindingActivity<DemoCoreActivityDisplayD02Binding>() {
    override fun getViewBinding(): DemoCoreActivityDisplayD02Binding {
        return DemoCoreActivityDisplayD02Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_d02_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBadges()
    }

    private fun initBadges() {
        val rpBadge1 = LetsBadgeFactory.create(LetsBadgeType.RED_POINT_24, this, v.ivRedPoint1)
        // 只能使用代码隐藏带badge的view，不支持在xml中隐藏
        val rpBadge2 = LetsBadgeFactory.create(LetsBadgeType.RED_POINT_50, this, v.ivRedPoint2).setVisibility(View.GONE)
        val rpBadge3 = LetsBadgeFactory.create(LetsBadgeType.RED_POINT_50, this, v.ivRedPoint3)

        val rnBadge1 = LetsBadgeFactory.create(LetsBadgeType.RED_NUMBER_24, this, v.ivRedNumber1)
        rnBadge1.badgeNumber = 8
        val rnBadge2 = LetsBadgeFactory.create(LetsBadgeType.RED_NUMBER_24, this, v.ivRedNumber2)
        rnBadge2.badgeNumber = 58
        val rnBadge3 = LetsBadgeFactory.create(LetsBadgeType.RED_NUMBER_24, this, v.ivRedNumber3)
        rnBadge3.badgeNumber = 101
        val rnBadge4 = LetsBadgeFactory.create(LetsBadgeType.RED_NUMBER_50, this, v.ivRedNumber4)
        rnBadge4.badgeNumber = 8
        val rnBadge5 = LetsBadgeFactory.create(LetsBadgeType.RED_NUMBER_50, this, v.ivRedNumber5)
        rnBadge5.badgeNumber = 58
        val rnBadge6 = LetsBadgeFactory.create(LetsBadgeType.RED_NUMBER_50, this, v.ivRedNumber6)
        rnBadge6.badgeNumber = 101

        LetsBadgeFactory.create(LetsBadgeType.IDENTITY_50, this, v.ivIdentity, R.drawable.lets_widget_core_icon_user_vip)

        val rnBadge7 = LetsBadgeFactory.create(LetsBadgeType.RED_POINT_CENTER, this, v.tvUnReadPoint)
        val rnBadge8 = LetsBadgeFactory.create(LetsBadgeType.RED_NUMBER_CENTER, this, v.tvUnReadCount)
        rnBadge8.badgeNumber = 58
        val rnBadge9 = LetsBadgeFactory.create(LetsBadgeType.CUSTOM_COLOR_NUMBER_CENTER, this, v.tvUnReadCountCustomColor, colorResId = R.color.lets_widget_core_color_bg_disable)
        rnBadge9.badgeNumber = 9
    }
}