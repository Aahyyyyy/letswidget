package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityStandardS04Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class DemoCoreS04Activity : BaseViewBindingActivity<DemoCoreActivityStandardS04Binding>() {
    override fun getViewBinding()= DemoCoreActivityStandardS04Binding.inflate(layoutInflater)

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_s04_title);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}