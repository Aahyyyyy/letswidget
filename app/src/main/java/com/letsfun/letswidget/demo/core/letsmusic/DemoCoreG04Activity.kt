package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.view.View
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityGlobalG04Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.base.immersionbar.ImmersionBar

class DemoCoreG04Activity : BaseViewBindingActivity<DemoCoreActivityGlobalG04Binding>() {
    private lateinit var originImmersionBar: ImmersionBar

    override fun getViewBinding(): DemoCoreActivityGlobalG04Binding {
        return DemoCoreActivityGlobalG04Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_g04_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        originImmersionBar = getImmersionBar()
        v.vPrimaryMask.setOnClickListener {
            it.visibility = View.GONE
            v.letsTitleBar.visibility = View.VISIBLE
            originImmersionBar
                .fitsSystemWindows(true)
                .fullScreen(false)
                .init()
        }
        v.vSecondaryMask.setOnClickListener {
            it.visibility = View.GONE
            v.letsTitleBar.visibility = View.VISIBLE
            originImmersionBar
                .fitsSystemWindows(true)
                .fullScreen(false)
                .init()
        }
    }

    override fun getImmersionBar(): ImmersionBar {
        return super.getImmersionBar()
            .fitsSystemWindows(true)
            .fullScreen(false)
            .titleBar(findViewById(R.id.letsTitleBar))
    }

    fun showPrimaryMask(view: View) {
        v.vPrimaryMask.visibility = View.VISIBLE
        originImmersionBar
            .fitsSystemWindows(false)
            .fullScreen(true)
            .init()
        v.letsTitleBar.visibility = View.GONE
    }

    fun showSecondaryMask(view: View) {
        v.vSecondaryMask.visibility = View.VISIBLE
        originImmersionBar
            .fitsSystemWindows(false)
            .fullScreen(true)
            .init()
        v.letsTitleBar.visibility = View.GONE
    }
}