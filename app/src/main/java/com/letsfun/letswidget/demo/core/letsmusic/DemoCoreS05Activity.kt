package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.util.Log
import com.letsfun.letswidget.lib.core.locale.LetsDurationToken
import com.letsfun.letswidget.lib.core.locale.LetsLangCode
import com.letsfun.letswidget.lib.core.locale.LetsLocaleHelper
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityStandardS05Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import java.text.DateFormat
import java.util.*

class DemoCoreS05Activity : BaseViewBindingActivity<DemoCoreActivityStandardS05Binding>() {
    companion object {
        private const val ONE_MINUTE = 60 * 1000
        private const val ONE_HOUR = ONE_MINUTE * 60
        private const val ONE_DAY = ONE_HOUR * 24
        private const val ONE_WEAK = ONE_DAY * 7
        private const val formatStr = "YYYYMMdd HH:mm"
        const val TAG = "locale_date"

        private val LOCALE_NO: Locale = LetsLocaleHelper.getLocale(LetsLangCode.NB_NO)
        private val LOCALE_DK: Locale = LetsLocaleHelper.getLocale(LetsLangCode.DA_DK)
        private val LOCALE_NL: Locale = LetsLocaleHelper.getLocale(LetsLangCode.NL_NL)
        private val LOCALE_SE: Locale = LetsLocaleHelper.getLocale(LetsLangCode.SV_SE)
        private val LOCALE_DE: Locale = LetsLocaleHelper.getLocale(LetsLangCode.DE_DE)
    }

    override fun getViewBinding(): DemoCoreActivityStandardS05Binding {
        return DemoCoreActivityStandardS05Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_s05_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        v.tvRelativeTime.text = showRelativeTime() + showDurationTime()
        v.tvAbsoluteTime.text = showAbsoluteDateTime()
        v.tvCurrency.text = showCurrency()
    }

    private fun showRelativeTime(): String {
        val sb = StringBuilder()
        sb.append("相对时间 \n")
        sb.append(
            LetsLocaleHelper.getRelativeDateTime(
                System.currentTimeMillis() - 5 * 1000,
                Locale.CHINA
            )
        )
            .append("\n")
        sb.append(
            LetsLocaleHelper.getRelativeDateTime(
                System.currentTimeMillis() - 5 * 60 * 1000,
                Locale.CHINA
            )
        )
            .append("\n")
        sb.append(
            LetsLocaleHelper.getRelativeDateTime(
                System.currentTimeMillis() - 13 * 60 * 60 * 1000,
                Locale.CHINA
            )
        ).append("\n")
        sb.append(
            LetsLocaleHelper.getRelativeDateTime(
                System.currentTimeMillis() - 2 * 24 * 60 * 60 * 1000,
                Locale.CHINA
            )
        ).append("\n")
        sb.append(
            LetsLocaleHelper.getRelativeDateTime(
                System.currentTimeMillis() - 15 * 24 * 60 * 60 * 1000,
                Locale.CHINA
            )
        ).append("\n")
        sb.append(
            LetsLocaleHelper.getRelativeDateTime(
                System.currentTimeMillis() - 365.toLong() * 24 * 60 * 60 * 1000,
                Locale.CHINA
            )
        ).append("\n")

        val relativeTime = sb.toString()
        Log.v(TAG, relativeTime)
        return relativeTime
    }

    private fun showDurationTime(): String {
        val sb = StringBuilder()
        sb.append("时间段 \n")
        sb.append(
            LetsLocaleHelper.localizeDurationFormat(
                2 * ONE_DAY.toLong(),
                Locale.CHINA,
                LetsDurationToken.TIME_DURATION_DDD,
            )
        ).append("\n")
        sb.append(
            LetsLocaleHelper.localizeDurationFormat(
                2 * ONE_DAY.toLong(),
                Locale.CHINA,
                LetsDurationToken.TIME_DURATION_HH,
            )
        ).append("\n")
        sb.append(
            LetsLocaleHelper.localizeDurationFormat(
                2 * ONE_DAY.toLong(),
                Locale.CHINA,
                LetsDurationToken.TIME_DURATION_MM,
            )
        ).append("\n")
        sb.append(
            LetsLocaleHelper.localizeDurationFormat(
                2 * ONE_DAY.toLong() + 2* ONE_HOUR + ONE_MINUTE,
                Locale.CHINA,
            )
        ).append("\n")
        sb.append(
            LetsLocaleHelper.localizeDurationFormat(
                2 * ONE_DAY.toLong() + 2* ONE_HOUR + ONE_MINUTE,
                Locale.CHINA,
                LetsDurationToken.TIME_DURATION_HHHMMSS
            )
        ).append("\n").append("\n")

        val durationTime = sb.toString()
        Log.v(TAG, durationTime)
        return durationTime
    }

    private fun showAbsoluteDateTime(): String {
        val sb = StringBuilder()
        val now = System.currentTimeMillis()
        sb.append("绝对时间：\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"yyyyMMdd",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"MMMdd",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"MMdd",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"yyyyMMM",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"yyyyMMddHHmm",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"yyyyMMddHHhh",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"HHhh",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"yyyyMMddHHmmss",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"yyyyMMddWWW",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.localizedDateFormat(now,"",Locale.CHINA)).append("\n")
        sb.append(LetsLocaleHelper.formatDate(now,Locale.CHINA, DateFormat.SHORT)).append("\n")
        sb.append(LetsLocaleHelper.formatDate(now,Locale.CHINA, DateFormat.LONG)).append("\n")
        sb.append(LetsLocaleHelper.formatDate(now,Locale.CHINA, DateFormat.FULL)).append("\n")

        val absoluteTime = sb.toString()
        Log.v(TAG, absoluteTime)
        return absoluteTime
    }

    private fun showCurrency(): String {
        val sb = StringBuilder()
        val number = 12345612.34560000
        val number2 = 12.00000
        sb.append("人民币：\n")
        sb.append(
            LetsLocaleHelper.formatCurrency(number, "CNY", Locale.CHINA)
        ).append("\n")
        val thousands = sb.toString()
        Log.v("thousands", thousands)
        return thousands
    }

}