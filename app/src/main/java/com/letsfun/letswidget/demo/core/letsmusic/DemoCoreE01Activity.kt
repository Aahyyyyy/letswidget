package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityEditE01Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class DemoCoreE01Activity : BaseViewBindingActivity<DemoCoreActivityEditE01Binding>() {
    override fun getViewBinding(): DemoCoreActivityEditE01Binding {
        return DemoCoreActivityEditE01Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_e01_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}