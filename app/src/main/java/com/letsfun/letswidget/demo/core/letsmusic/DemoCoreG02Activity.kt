package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityGlobalG02Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.view.span.letsLinkText

class DemoCoreG02Activity : BaseViewBindingActivity<DemoCoreActivityGlobalG02Binding>() {
    override fun getViewBinding(): DemoCoreActivityGlobalG02Binding {
        return DemoCoreActivityGlobalG02Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_g02_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        v.tv2.text = "请点击用户协议查看具体细节"
        v.tv2.letsLinkText("用户协议") {
            println("点击了用户协议")
        }
    }
}