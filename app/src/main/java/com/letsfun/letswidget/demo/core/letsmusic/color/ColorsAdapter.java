package com.letsfun.letswidget.demo.core.letsmusic.color;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RawRes;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.letsfun.letswidget.R;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ColorsAdapter extends RecyclerView.Adapter<ColorsViewHolder> {

    private List<Color> colorList;
    private Context context;

    public ColorsAdapter(Context context) {
        this.context = context;
        parse();
    }

    private synchronized void parse() {
        String jsonFile = readRawResource(R.raw.demo_core_colors);
        Color[] array = new Gson().fromJson(jsonFile, Color[].class);
        colorList = Arrays.asList(array);
    }

    private String readRawResource(@RawRes int res) {
        return readStream(context.getResources().openRawResource(res));
    }

    private String readStream(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @NonNull
    @Override
    public ColorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ColorsViewHolder holder = new ColorsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.demo_core_item_colors, parent, false));
        holder.setIsRecyclable(false);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ColorsViewHolder holder, int position) {
        Color colorInfo = colorList.get(position);
        int colorId = context.getResources().getIdentifier(colorInfo.getKey(), "color", context.getPackageName());
        holder.getTvColor().setText(String.format("#%06X", (0xFFFFFF & ContextCompat.getColor(context, colorId))));
        holder.getTvNick().setText(colorInfo.getKey().replace("lets_widget_core_", ""));
        holder.getTvDesc().setText(colorInfo.getDescription());;
        holder.getClColor().setBackgroundColor(ContextCompat.getColor(context, colorId));

        if (ContextCompat.getColor(context, colorId) == android.graphics.Color.parseColor("#FFFFFF")) {//白色
            holder.getVRect().setVisibility(View.VISIBLE);
        } else {
            holder.getVRect().setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return colorList.size();
    }
}
