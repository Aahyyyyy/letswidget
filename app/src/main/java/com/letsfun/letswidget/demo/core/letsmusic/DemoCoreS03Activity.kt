package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityStandardS03Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.shadow.LetsRoundedShadowDrawable
import com.letsfun.letswidget.sdk.core.shadow.LetsRoundedShadowDrawable.Companion.SHADOW_MAP
import com.letsfun.letswidget.sdk.core.shadow.LetsRoundedShadowDrawable.Companion.SHADOW_OVERLAY

class DemoCoreS03Activity: BaseViewBindingActivity<DemoCoreActivityStandardS03Binding>() {
    override fun getViewBinding()= DemoCoreActivityStandardS03Binding.inflate(layoutInflater)

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_s03_title);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        v.root.clipChildren = false
        v.clShadow2.background = LetsRoundedShadowDrawable(this, SHADOW_OVERLAY)
        v.clShadow3.background = LetsRoundedShadowDrawable(this, SHADOW_MAP)
    }
}