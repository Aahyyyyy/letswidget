package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityEditE02Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class DemoCoreE02Activity : BaseViewBindingActivity<DemoCoreActivityEditE02Binding>() {
    override fun getViewBinding(): DemoCoreActivityEditE02Binding {
        return DemoCoreActivityEditE02Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_e02_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

}