package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.view.View
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityDisplayD01Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.view.LetsAvatar

class DemoCoreD01Activity : BaseViewBindingActivity<DemoCoreActivityDisplayD01Binding>() {
    private var showBorder = true;

    override fun getViewBinding(): DemoCoreActivityDisplayD01Binding {
        return DemoCoreActivityDisplayD01Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_d01_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        v.imgAvatar4.setUserType(LetsAvatar.UserType.TYPE_USER)
        v.imgAvatar5.setUserType(LetsAvatar.UserType.TYPE_USER_VIP)
    }

    fun showBorder(view: View) {
        showBorder = !showBorder
        v.imgAvatar1.setShowBorder(showBorder)
        v.imgAvatar2.setShowBorder(showBorder)
        v.imgAvatar3.setShowBorder(showBorder)
        v.imgAvatar4.setShowBorder(showBorder)
        v.imgAvatar5.setShowBorder(showBorder)
    }

    private var index = 0
    fun changeType(view: View) {
        if (view is LetsAvatar) {
            when (index) {
                0 -> view.setUserType(LetsAvatar.UserType.TYPE_NONE)
                1 -> view.setUserType(LetsAvatar.UserType.TYPE_USER)
                2 -> view.setUserType(LetsAvatar.UserType.TYPE_USER_VIP)
                3 -> view.setUserType(LetsAvatar.UserType.TYPE_CUSTOM)
                else -> index = -1
            }
        }
        index++
    }
}