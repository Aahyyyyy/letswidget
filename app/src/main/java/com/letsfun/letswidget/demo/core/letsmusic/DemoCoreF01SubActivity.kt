package com.letsfun.letswidget.demo.core.letsmusic

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityFeedbackF01SubBinding
import com.letsfun.letswidget.lib.core.utils.ThreadUtils
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.loading.LetsPageLoading
import com.letsfun.letswidget.sdk.core.loading.LetsToastLoading

class DemoCoreF01SubActivity : BaseViewBindingActivity<DemoCoreActivityFeedbackF01SubBinding>() {
    companion object {
        const val LOADING_TYPE11 = "LOADING_TYPE11"
        const val LOADING_TYPE21 = "LOADING_TYPE21"
        const val LOADING_TYPE22 = "LOADING_TYPE22"
        const val LOADING_TYPE31 = "LOADING_TYPE31"
        const val LOADING_TYPE32 = "LOADING_TYPE32"
        const val LOADING_TYPE33 = "LOADING_TYPE33"
        const val LOADING_TYPE41 = "LOADING_TYPE41"
        const val LOADING_TYPE42 = "LOADING_TYPE42"
        const val LOADING_TYPE51 = "LOADING_TYPE51"

        fun startActivity(context: Context, type: String) {
            context.startActivity(
                Intent(
                    context,
                    DemoCoreF01SubActivity::class.java
                ).putExtra("type", type)
            )
        }
    }

    override fun getViewBinding(): DemoCoreActivityFeedbackF01SubBinding {
        return DemoCoreActivityFeedbackF01SubBinding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_f01_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showLoading()
    }

    private fun hideAllViews() {
        v.btnRefresh.visibility = View.GONE
    }

    private fun showLoading() {
        hideAllViews()
        when (intent.getStringExtra("type")) {
            LOADING_TYPE11 -> {
                showLoadingType11()
            }
            LOADING_TYPE21 -> {
                showLoadingType21()
            }
            LOADING_TYPE22 -> {
                showLoadingType22()
            }
        }
    }

    private fun showLoadingType11() {
        v.btnRefresh.visibility = View.VISIBLE
        val containerView: FrameLayout = window.decorView as FrameLayout
        val pageLoading = LetsPageLoading(containerView)
        pageLoading.show()
        ThreadUtils.get().executeMain({
            pageLoading.dismiss()
            finish()
        }, 5000)
    }

    private fun showLoadingType21() {
        v.btnRefresh.visibility = View.VISIBLE
        val toastLoading = LetsToastLoading(this)
        toastLoading.show("加载中")
        ThreadUtils.get().executeMain({
            toastLoading.showCustomer(
                "加载失败",
                R.drawable.lets_widget_core_icon_fail_xl,
                R.color.lets_widget_core_color_red_10
            )
        }, 5000)
    }

    private fun showLoadingType22() {
        v.btnRefresh.visibility = View.VISIBLE
        val toastLoading = LetsToastLoading(this)
        toastLoading.show("加载中")
        ThreadUtils.get().executeMain({
            toastLoading.showDone("加载成功")
        }, 5000)
        ThreadUtils.get().executeMain({
            toastLoading.dismiss()
            finish()
        }, 10000)
    }

    fun onRefresh(view: View) {
        showLoading()
    }



}