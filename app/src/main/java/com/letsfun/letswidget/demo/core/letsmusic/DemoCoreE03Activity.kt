package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityEditE03Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.edittext.LetsClearableEditText
import com.letsfun.letswidget.sdk.core.edittext.LetsPasswordEditText
import com.letsfun.letswidget.sdk.core.edittext.listener.OnIconClickListener
import com.letsfun.letswidget.sdk.core.edittext.listener.OnVerifyCodeListener
import com.letsfun.letswidget.sdk.core.view.LgCountDownTextView

class DemoCoreE03Activity : BaseViewBindingActivity<DemoCoreActivityEditE03Binding>() {
    override fun getViewBinding(): DemoCoreActivityEditE03Binding {
        return DemoCoreActivityEditE03Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_e03_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initEditText()
        initSearchEditText()
    }

    private fun initEditText() {
        initEditText1()
        initEditText2()
        initClearableEditText()
        initPasswordEditText()
        initVerifyCodeEditText()
    }

    private fun initSearchEditText() {
        v.search1.apply {
            getEditText().setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Toast.makeText(context, "搜索${this.editContent}", Toast.LENGTH_SHORT).show()
                }
                false
            }
        }

        v.search2.apply {
            editContent = "这个没有clear图标"
        }

        v.search3.apply {
            setActionClickListener {
                Toast.makeText(context, "点击了取消", Toast.LENGTH_SHORT).show()
            }
        }

        v.search4.apply {
            editContent = "这个有clear图标"
            setActionClickListener {
                Toast.makeText(context, "点击了取消", Toast.LENGTH_SHORT).show()
            }
        }

        v.search6.apply {
            editHint = "这个没有clear图标"
        }

        v.search7.apply {
            setActionClickListener {
                Toast.makeText(context, "搜索${this.editContent}", Toast.LENGTH_SHORT).show()
            }
        }

        v.search8.apply {
            editHint = "这个没有clear图标"
            setActionClickListener {
                Toast.makeText(context, "搜索${this.editContent}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initEditText1() {
        v.editText1.setLeadIcon(R.drawable.lets_widget_core_icon_user_vip)
        v.editText1.setOnLeadIconClickListener(object : OnIconClickListener {
            override fun onIconClicked() {
                Toast.makeText(context, "LeadIcon", Toast.LENGTH_SHORT).show()
            }
        })
        v.editText1.setTailIcon(R.drawable.lets_widget_core_icon_user_vip, R.drawable.lets_widget_core_icon_user_vip)
        v.editText1.setOnTailPrimaryIconClickListener(object : OnIconClickListener {
            override fun onIconClicked() {
                Toast.makeText(context, "TailPrimaryIcon", Toast.LENGTH_SHORT).show()
            }
        })
        v.editText1.setOnTailSecondaryIconClickListener(object : OnIconClickListener {
            override fun onIconClicked() {
                Toast.makeText(context, "TailSecondaryIcon", Toast.LENGTH_SHORT).show()
            }
        })
        v.editText1.setErrorHint("这是个提示")
    }

    private fun initEditText2() {
        v.editText2.setLeadIcon(R.drawable.lets_widget_core_icon_user_vip)
        v.editText2.setTailIcon(R.drawable.lets_widget_core_icon_user_vip, R.drawable.lets_widget_core_icon_user_vip)
    }

    private fun initClearableEditText() {
        v.editText3.setSecondaryIcon(R.drawable.lets_widget_core_icon_user_vip)
        v.editText3.setOnSecondaryIconClickListener(object : OnIconClickListener {
            override fun onIconClicked() {
                Toast.makeText(context, "TailSecondaryIcon", Toast.LENGTH_SHORT).show()
                v.editText3.setSecondaryIcon(0)
            }
        })
        v.editText3.setOnTextClearListener(object :
            LetsClearableEditText.OnTextClearListener {
            override fun onInterruptClear(): Boolean {
                return super.onInterruptClear()
            }

            override fun onCleared() {
                Toast.makeText(context, "Cleared", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun initPasswordEditText() {
        v.editText4.error = "这是个错误"
        v.editText4.setOnTogglePasswordModeListener(object :
            LetsPasswordEditText.OnToggleEyeListener {
            override fun toggle(isPasswordMode: Boolean) {
                Toast.makeText(context, isPasswordMode.toString(), Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun initVerifyCodeEditText() {
        v.editText5.setOnSendClickListener {
            this@DemoCoreE03Activity.v.editText5.setError("")
            this@DemoCoreE03Activity.v.editText5.startCountDown(10,
                object : LgCountDownTextView.OnCountDownListener {
                    override fun onTick(currentTimeInSeconds: Long) {

                    }

                    override fun onFinish() {
                        this@DemoCoreE03Activity.v.editText5.setError("这里有错误")
                    }
                })
        }
        v.editText5.setOnVerifyCodeListener(object : OnVerifyCodeListener {
            override fun onTextChanged(code: String) {
            }

            override fun onComplete(code: String) {
                Toast.makeText(context, code, Toast.LENGTH_SHORT).show()
            }
        })
    }
}