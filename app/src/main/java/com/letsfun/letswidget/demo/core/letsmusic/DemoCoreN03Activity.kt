package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityNavigationN03Binding
import com.letsfun.letswidget.demo.core.letsmusic.fragment.FragmentDemo
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.toast.LetsToast
import com.letsfun.letswidget.sdk.core.utils.UiUtils


class DemoCoreN03Activity : BaseViewBindingActivity<DemoCoreActivityNavigationN03Binding>() {
    override fun getViewBinding(): DemoCoreActivityNavigationN03Binding {
        return DemoCoreActivityNavigationN03Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_n03_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var fragmentList = with(ArrayList<Fragment>()) {
            add(FragmentDemo("关注"))
            add(FragmentDemo("推荐"))
            return@with this
        }

        object : FragmentStateAdapter(this) {
            override fun getItemCount(): Int {
                return fragmentList.size

            }

            override fun createFragment(position: Int): Fragment {
                return fragmentList[position]
            }
        }.also { v.viewPager.adapter = it }

        v.letsTabLayout.setMultiTabViewPager(v.viewPager) { position ->
            (fragmentList[position] as FragmentDemo).title
        }

        v.letsTabLayout.setTabClickListener {
            LetsToast.showToast("tab $it clicked")
        }
    }

}