package com.letsfun.letswidget.demo.core.letsmusic.color

import androidx.annotation.Keep

@Keep
data class Color(var key: String, var description: String)