package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityStandardS01Binding
import com.letsfun.letswidget.demo.core.letsmusic.color.ColorsAdapter
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class DemoCoreS01Activity : BaseViewBindingActivity<DemoCoreActivityStandardS01Binding>() {
    override fun getViewBinding(): DemoCoreActivityStandardS01Binding {
        return DemoCoreActivityStandardS01Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_s01_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layoutManager = LinearLayoutManager(this)
        v.rvColors.layoutManager = layoutManager
        v.rvColors.adapter = ColorsAdapter(this)
    }
}