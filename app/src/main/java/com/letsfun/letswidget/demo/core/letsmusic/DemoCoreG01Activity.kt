package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.tabs.TabLayout
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityGlobalG01Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.view.LetsStatefulButton

class DemoCoreG01Activity : BaseViewBindingActivity<DemoCoreActivityGlobalG01Binding>(), TabLayout.OnTabSelectedListener {
    override fun getViewBinding(): DemoCoreActivityGlobalG01Binding {
        return DemoCoreActivityGlobalG01Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_g01_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        tabLayout.addTab(tabLayout.newTab().setText("Default"))
        tabLayout.addTab(tabLayout.newTab().setText("Medium"))
        tabLayout.addTab(tabLayout.newTab().setText("Small"))
        tabLayout.addTab(tabLayout.newTab().setText("文字"))
        tabLayout.addTab(tabLayout.newTab().setText("状态"))

        tabLayout.addOnTabSelectedListener(this)

        showView(0)

        initLoadingButton()
        initStatefulButton()
        initAddButton()
    }

    private fun showView(position: Int) {
        hideAllViews()
        when (position) {
            0 -> v.tabPage0.visibility = View.VISIBLE
            1 -> v.tabPage1.visibility = View.VISIBLE
            2 -> v.tabPage2.visibility = View.VISIBLE
            3 -> v.tabPage3.visibility = View.VISIBLE
            4 -> v.tabPage4.visibility = View.VISIBLE
        }
    }

    private fun hideAllViews() {
        v.tabPage0.visibility = View.GONE
        v.tabPage1.visibility = View.GONE
        v.tabPage2.visibility = View.GONE
        v.tabPage3.visibility = View.GONE
        v.tabPage4.visibility = View.GONE
    }

    private fun initLoadingButton() {
        v.btnLoading1.buttonStyle = R.style.LetsWidgetCoreButtonBase_Primary
        v.btnLoading2.buttonStyle = R.style.LetsWidgetCoreButtonBase_Secondary
        v.btnLoading3.buttonStyle = R.style.LetsWidgetCoreButtonBase_Danger
        v.btnLoading1.btnText = "Loading Button Primary"
        v.btnLoading2.btnText = "Loading Button Secondary"
        v.btnLoading3.btnText = "Loading Button Danger"

        v.btnLoading1.onLoadingButtonClick = {
            if (v.btnLoading1.isLoading()) {
                v.btnLoading1.endLoading()
            } else {
                v.btnLoading1.startLoading()
            }
        }

        v.btnLoading2.onLoadingButtonClick = {
            if (v.btnLoading2.isLoading()) {
                v.btnLoading2.endLoading()
            } else {
                v.btnLoading2.startLoading()
            }
        }

        v.btnLoading3.onLoadingButtonClick = {
            if (v.btnLoading3.isLoading()) {
                v.btnLoading3.endLoading()
            } else {
                v.btnLoading3.startLoading()
            }
        }

        v.btnLoading4.apply {
            onLoadingButtonClick = {
                if (isLoading()){
                    endLoading()
                } else {
                    startLoading()
                }
            }
        }

        v.btnLoading5.apply {
            onLoadingButtonClick = {
                if (isLoading()){
                    endLoading()
                } else {
                    startLoading()
                }
            }
        }
    }

    private fun initStatefulButton() {
        v.btnStateful1.apply {
            onBeforeClickListener = {
                curState = LetsStatefulButton.STATE_CLICK_LOADING
                postDelay(1000){
                    curState = LetsStatefulButton.STATE_CLICK_AFTER
                    text = "State L B"
                }
            }
        }

        v.btnStateful3.apply {
            onBeforeClickListener = {
                curState = LetsStatefulButton.STATE_CLICK_LOADING
                postDelay(1000){
                    curState = LetsStatefulButton.STATE_CLICK_AFTER
                    text = "State M B"
                }
            }
        }

        v.btnStateful4.apply {
            text = "State M after"
            onAfterClickListener = {

            }
        }

        v.btnStateful5.apply {
            isEnabled = false
            text = "State M disable"
        }

        v.btnStateful6.apply {
            text = "关注"
            onBeforeClickListener = {
                curState = LetsStatefulButton.STATE_CLICK_LOADING
                postDelay(1000){
                    curState = LetsStatefulButton.STATE_CLICK_AFTER
                    text = "已关注"
                }
            }
        }

        v.btnStateful7.apply {
            text = "State S after"
            onAfterClickListener = {

            }
        }

        v.btnStateful8.apply {
            btnSize = LetsStatefulButton.SIZE_S
            text = "State S disable"
        }

        v.btnStateful9.apply {
            text = "自定义文字颜色&边框"
            normalTextColor = R.color.demo_core_color_selector_state_text
            afterTextColor = R.color.lets_widget_core_color_blue_6
            onBeforeClickListener = {
                curState = LetsStatefulButton.STATE_CLICK_LOADING
                setBackgroundResource(R.drawable.lets_widget_core_shape_btn_danger_small)
                postDelay(1000){
                    curState = LetsStatefulButton.STATE_CLICK_AFTER
                }
            }
            onAfterClickListener = {
                curState = LetsStatefulButton.STATE_CLICK_UNABLE
            }
            setProgressTint(ContextCompat.getColor(this@DemoCoreG01Activity, R.color.lets_widget_core_color_red_5))
        }
    }

    private fun initAddButton() {
        v.btnAddPrimary.apply {
            text = "Add Button Primary"
            setOnClickListener {
            }
        }

        v.btnAddSecondary.apply {
            text = "Add Button Secondary"
            setOnClickListener {
            }
        }
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        showView(tab!!.position)
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }
}