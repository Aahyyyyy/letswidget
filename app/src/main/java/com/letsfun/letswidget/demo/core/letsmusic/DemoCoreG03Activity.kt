package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityGlobalG03Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class DemoCoreG03Activity : BaseViewBindingActivity<DemoCoreActivityGlobalG03Binding>() {
    override fun getViewBinding(): DemoCoreActivityGlobalG03Binding {
        return DemoCoreActivityGlobalG03Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_g03_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

}