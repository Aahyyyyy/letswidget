package com.letsfun.letswidget.demo.core.letsmusic.color

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.letsfun.letswidget.R

class ColorsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var clColor: RoundRelativeLayout = itemView.findViewById(R.id.rlColor)
    var vRect: View = itemView.findViewById(R.id.vRect)
    var tvColor: TextView = itemView.findViewById(R.id.tvColor)
    var tvNick: TextView = itemView.findViewById(R.id.tvNickname)
    var tvDesc: TextView = itemView.findViewById(R.id.tvDesc)
}