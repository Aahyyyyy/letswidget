package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.view.View
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityFeedbackF02Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.toast.LetsToast

class DemoCoreF02Activity : BaseViewBindingActivity<DemoCoreActivityFeedbackF02Binding>() {
    override fun getViewBinding(): DemoCoreActivityFeedbackF02Binding {
        return DemoCoreActivityFeedbackF02Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_f02_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun showSingleToast(view: View) {
        LetsToast.showToast("单行提示")
    }

    fun showMultipleToast(view: View) {
        LetsToast.showToast("This is a toast this is a toast this is a toast")
    }

}