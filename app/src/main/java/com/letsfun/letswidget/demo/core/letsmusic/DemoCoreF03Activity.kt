package com.letsfun.letswidget.demo.core.letsmusic

import android.os.Bundle
import android.view.View
import com.letsfun.letswidget.R
import com.letsfun.letswidget.databinding.DemoCoreActivityFeedbackF03Binding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity
import com.letsfun.letswidget.sdk.core.toast.LetsToast
import com.letsfun.letswidget.sdk.dialog.LetsCommonDialog
import com.letsfun.letswidget.sdk.dialog.LetsConfirmDialog

class DemoCoreF03Activity : BaseViewBindingActivity<DemoCoreActivityFeedbackF03Binding>() {
    override fun getViewBinding(): DemoCoreActivityFeedbackF03Binding {
        return DemoCoreActivityFeedbackF03Binding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return getString(R.string.demo_core_f03_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun onClick(view: View) {
        when(view.id) {
            R.id.button1 -> {
                showConfirmDialog1()
            }
            R.id.button21 -> {
                LetsCommonDialog(this).apply {
                    showCommonDialog(
                        title ="",
                        message = "我是不带title的dialog",
                        leftBtnText = "取消",
                        rightBtnText = "确定",
                        leftClick = { dismiss() },
                        rightClick = {
                            dismiss()
                            LetsToast.showToast("click right")
                        }
                    )
                }
            }
            R.id.button22 -> {
                LetsCommonDialog(this).apply {
                    showCommonDialog(
                        title = "通用type2",
                        message = "此处是描述文案此处是描述文案此处是描述文案",
                        leftBtnText = "取消",
                        rightBtnText = "确定",
                        leftClick = { },
                        rightClick = { LetsToast.showToast("click right") },
                        type = 2,
                        linkText = "更多须知",
                        linkClick = { LetsToast.showToast("click link") },
                        isTitle = true,
                        isWarning = true
                    )
                }
            }

            R.id.button23 -> {
                LetsCommonDialog(this).apply {
                    show()//dialog要先show才会调用onCreate
                    tvAgree.text = "I have read and accept."
                    showCommonDialog(
                        "通用type3", "你试试似乎四海hi啊hi啊嗨嗨嗨事实上横说竖说",
                        "取消", "确定", {
                        }, {
                            if (isCheckAgree()) {
                                LetsToast.showToast("confirm")
                            } else {
                                LetsToast.showToast("请先同意协议")
                            }
                        }, type = 3
                    )
                }
            }

            R.id.button24 -> {
                LetsCommonDialog(this).apply {
                    show()//dialog要先show才会调用onCreate
                    tvAgree.text = "I have read and accept."
                    centerAgree(true)
                    showCommonDialog(
                        "通用type3", "你试试似乎四海hi啊hi啊嗨嗨嗨事实上横说竖说",
                        "取消", "确定", {
                        }, {
                            if (isCheckAgree()) {
                                LetsToast.showToast("confirm")
                            } else {
                                LetsToast.showToast("请先同意协议")
                            }
                        }, type = 3
                    )
                }
            }

            R.id.button25 -> {
                val commonDialog = LetsCommonDialog(this)
                commonDialog.show()
                commonDialog.title("builder通用type3").agreeText("用户隐私协议").message("this is message").type(3)
                    .rightBtnText("confirm").centerAgree(true).rightClick {
                        if (commonDialog.isCheckAgree()) {
                            LetsToast.showToast("confirm")
                        } else {
                            LetsToast.showToast("请先同意协议")
                        }

                    }.showSingleDialog()
            }

            R.id.button26 -> {
                LetsCommonDialog(this).apply {
                    showSingleButtonDialog(
                        "通用", "你试试似乎四海hi啊hi啊嗨嗨嗨事实上横说竖说",
                        "确定", {
                            dismiss()
                        }, type = 2, isWarning = true, linkText = "更多须知", linkClick = {
                            LetsToast.showToast("click link")
                        }
                    )
                }
            }
        }
    }

    private fun showConfirmDialog1() {
        LetsConfirmDialog(this).apply {
            title = "Delete this album?"
            message = "You’ll lose all photos and media"
            negativeText = "取消"
            positiveText = "确定"
            cancelableOnTouchOutside = false
            negativeListener = {
                LetsToast.showToast("取消")
            }
            positiveListener = {
                LetsToast.showToast("确定")
            }
            dismissListener = {
            }
        }.show()
    }

}