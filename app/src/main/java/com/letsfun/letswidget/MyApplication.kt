package com.letsfun.letswidget

import android.app.Activity
import android.app.Application
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.FrameLayout
import com.letsfun.letswidget.lib.core.AppContext
import com.letsfun.letswidget.lib.core.locale.LetsLocaleHelper
import com.letsfun.letswidget.sdk.core.base.immersionbar.ImmersionBar

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AppContext.setApp(this)
        AppContext.setProd(false)
    }

    private val activityCallbacks = object : ActivityLifecycleCallbacks {
        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            /**
             * 参照国内现状，若app的activity无统一的BaseActivity，又要全局设置navigationBar颜色为页面颜色可参照此处示例
             * 合理方式应该是（需和设计讨论）：app设置固定主色调，navigationBar颜色在base中设置成主色调
             * 全屏页面，如拍摄类需fragment单独设置fullScreen
             */
            activity.window.decorView.post {
                try {
                    val drawable =
                        activity.window.decorView.findViewById<FrameLayout>(android.R.id.content)
                            .getChildAt(0).background
                    if (drawable is ColorDrawable) {
                        ImmersionBar.with(activity).navigationBarColorInt(drawable.color).init()
                    }
                } catch (e: Throwable) {
                    ImmersionBar.with(activity)
                        .navigationBarColor(R.color.lets_widget_core_transparent).init()
                }
            }
            LetsLocaleHelper.updateConfig(activity)
            LetsLocaleHelper.updateConfig(AppContext.getApp())
        }

        override fun onActivityStarted(activity: Activity) {
        }

        override fun onActivityResumed(activity: Activity) {
        }

        override fun onActivityPaused(activity: Activity) {
        }

        override fun onActivityStopped(activity: Activity) {
        }

        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        }

        override fun onActivityDestroyed(activity: Activity) {
        }

    }
}