package com.letsfun.letswidget

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.letsfun.letswidget.databinding.ActivityMainBinding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class MainActivity : BaseViewBindingActivity<ActivityMainBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        v.letsTitleBar.ivNavigation.visibility = View.GONE
    }

    override fun getViewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    override fun getPageTitle(): String {
        return "LetsWidget"
    }

    fun toLetsMusic(view: View?) {
        startActivity(Intent(this@MainActivity, DemoLetsMusicActivity::class.java))
    }
}