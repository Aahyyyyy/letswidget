package com.letsfun.letswidget.sdk.core.edittext.base

import com.letsfun.letswidget.sdk.core.edittext.listener.OnIconClickListener

interface IBaseEditText {
    fun setLeadIcon(iconResId: Int)

    fun setTailIcon(primaryIcon: Int, secondaryIcon: Int)

    fun setOnLeadIconClickListener(onClickListener: OnIconClickListener?)

    fun setOnTailPrimaryIconClickListener(onClickListener: OnIconClickListener?)

    fun setOnTailSecondaryIconClickListener(onClickListener: OnIconClickListener?)

    fun invalidateView()

    fun postInvalidateView()
}