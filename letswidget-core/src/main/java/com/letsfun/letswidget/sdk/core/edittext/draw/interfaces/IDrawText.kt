package com.letsfun.letswidget.sdk.core.edittext.draw.interfaces

import android.graphics.PointF

interface IDrawText : IDraw {
    fun setText(text: String)

    fun setPosition(position: PointF)

    fun getText(): CharSequence
}