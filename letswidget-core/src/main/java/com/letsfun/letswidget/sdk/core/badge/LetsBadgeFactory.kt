package com.letsfun.letswidget.sdk.core.badge

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.letsfun.letswidget.sdk.core.badge.impl.LetsBadgeImpl
import com.letsfun.letswidget.sdk.core.badge.interfaces.ILetsBadge
import com.letsfun.letswidget.sdk.core.badge.wrapper.*
import java.lang.IllegalArgumentException

object LetsBadgeFactory {

    /**
     * @param type 类型为[LetsBadgeType]
     * @param resId 身份徽标图片
     *
     */
    fun create(type: String, context: Context, targetView: View, @DrawableRes resId: Int? = null): ILetsBadge {
        when (type) {
            LetsBadgeType.RED_POINT_24 ->
                return LetsBadgeRedPoint24(context, targetView)
            LetsBadgeType.RED_POINT_50 ->
                return LetsBadgeRedPoint50(context, targetView)
            LetsBadgeType.RED_POINT_CENTER ->
                return LetsBadgeRedPointCenter(context, targetView)

            LetsBadgeType.RED_NUMBER_24 ->
                return LetsBadgeRedNumber24(context, targetView)
            LetsBadgeType.RED_NUMBER_50 ->
                return LetsBadgeRedNumber50(context, targetView)
            LetsBadgeType.RED_NUMBER_CENTER ->
                return LetsBadgeRedNumberCenter(context, targetView)

            LetsBadgeType.IDENTITY_50 ->
                return LetsBadgeIdentity50(context, targetView, resId!!)
        }
        throw IllegalArgumentException("Unsupported badge type")
    }

    /**
     * @param type 类型为[LetsBadgeType]
     * @param badgeView 在XML中创建的QBadgeView
     */
    fun create(type: String, badgeView: LetsBadgeImpl): ILetsBadge {
        when (type) {
            LetsBadgeType.RED_POINT_24 ->
                return LetsBadgeRedPoint24(null, null, badgeView)
            LetsBadgeType.RED_POINT_50 ->
                return LetsBadgeRedPoint50(null, null, badgeView)
            LetsBadgeType.RED_NUMBER_50 ->
                return LetsBadgeRedNumber50(null, null, badgeView)
            LetsBadgeType.RED_POINT_CENTER ->
                return LetsBadgeRedPointCenter(null, null, badgeView)
            LetsBadgeType.RED_NUMBER_CENTER->
                return LetsBadgeRedNumberCenter(null, null, badgeView)
        }
        throw IllegalArgumentException("Unsupported badge type")
    }

    /**
     * @param type 类型为[LetsBadgeType]
     * @param colorResId 支持自定义颜色
     */
    @SuppressLint("NewApi")
    fun create(type: String, context : Context, targetView: View, @DrawableRes resId: Int? = null, @ColorRes colorResId: Int): ILetsBadge {
        when (type) {
            LetsBadgeType.CUSTOM_COLOR_NUMBER_CENTER ->
                return LetsBadgeCustomBgColorNumberCenter(context, targetView, bgColor = context.getColor(colorResId))
        }
        throw IllegalArgumentException("Unsupported badge type")
    }


    /**
     * @param type 类型为[LetsBadgeType]
     * @param badgeView 在XML中创建的QBadgeView
     */
    fun create(type: String, badgeView: LetsBadgeImpl, color: Int): ILetsBadge {
        when (type) {
            LetsBadgeType.CUSTOM_COLOR_NUMBER_CENTER ->
                return LetsBadgeCustomBgColorNumberCenter(null, null, badgeView = badgeView, bgColor = color)
        }
        throw IllegalArgumentException("Unsupported badge type")
    }
}