package com.letsfun.letswidget.sdk.core.edittext

import android.annotation.SuppressLint
import android.content.Context
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.View
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.edittext.listener.OnIconClickListener

@SuppressLint("NewApi")
class LetsPasswordEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.editTextStyle
) : LetsClearableEditText(context, attrs, defStyleAttr) {

    //保存Theme中默认字体
    private var defaultTypeface = typeface

    private val onEyeClickListener: OnIconClickListener by lazy {
        object : OnIconClickListener {
            override fun onIconClicked() {
                val isShowPassWord = isInPasswordMode()
                showPassword(isShowPassWord)
                onToggleEyeListener?.toggle(isShowPassWord)
            }
        }
    }

    private var onToggleEyeListener: OnToggleEyeListener? = null

    init {
        inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        fixFontIssue(false)

        showPassword(false)

        setOnSecondaryIconClickListener(onEyeClickListener)
    }

    private fun showPassword(showPassword: Boolean) {
        if (showPassword) {
            setSecondaryIcon(R.drawable.lets_widget_core_icon_eye_16_open)
            inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
            fixFontIssue(true)
        } else {
            setSecondaryIcon(R.drawable.lets_widget_core_icon_eye_16_close)
            inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            fixFontIssue(false)
        }
        setSelection(length())
    }

    /**
     * 密码模式下，系统会把字体修改为MONOSPACE等宽字体，覆盖了默认字体，在此还原为Theme默认字体，
     * 同时把密码符号修改为*
     */
    private fun fixFontIssue(passwordVisible: Boolean) {
        typeface = defaultTypeface
        transformationMethod = if (passwordVisible) null else BiggerDotPasswordTransformationMethod
    }

    private object BiggerDotPasswordTransformationMethod : PasswordTransformationMethod() {

        override fun getTransformation(source: CharSequence, view: View): CharSequence {
            return PasswordCharSequence(super.getTransformation(source, view))
        }

        private class PasswordCharSequence(
            val transformation: CharSequence
        ) : CharSequence by transformation {
            override fun get(index: Int): Char = if (transformation[index] == DOT) {
                BIGGER_DOT
            } else {
                transformation[index]
            }
        }

        private const val DOT = '\u2022'
        private const val BIGGER_DOT = '*'
    }

    private fun isInPasswordMode(): Boolean {
        return (inputType and InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD != InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
    }

    fun setOnTogglePasswordModeListener(listener: OnToggleEyeListener) {
        this.onToggleEyeListener = listener
    }

    interface OnToggleEyeListener {
        fun toggle(isEyeShow: Boolean)
    }
}