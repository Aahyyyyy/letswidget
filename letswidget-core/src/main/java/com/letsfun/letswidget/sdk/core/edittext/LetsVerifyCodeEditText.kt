package com.letsfun.letswidget.sdk.core.edittext

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnKeyListener
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.edittext.base.IVerifyCodeEditText
import com.letsfun.letswidget.sdk.core.edittext.listener.OnVerifyCodeListener
import com.letsfun.letswidget.sdk.core.ext.activity
import com.letsfun.letswidget.sdk.core.utils.SoftInputUtils
import com.letsfun.letswidget.sdk.core.view.LgCountDownTextView

class LetsVerifyCodeEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), IVerifyCodeEditText {
    private val tvCodes = Array<TextView?>(MAX_NUM) { null }
    private val cursorViews = Array<View?>(MAX_NUM) { null }
    private val codes = mutableListOf<String>()
    private var edtContainer: EditText
    private var lcdTextView: LgCountDownTextView
    private var tvError: TextView

    private var onVerifyCodeListener: OnVerifyCodeListener? = null

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.lets_widget_core_layout_edittext_verify_code, this)
        edtContainer = findViewById(R.id.edt_container)
        lcdTextView = findViewById(R.id.lcdTextView)
        tvError = findViewById(R.id.tvError)
        tvCodes[0] = findViewById(R.id.textView0)
        tvCodes[1] = findViewById(R.id.textView1)
        tvCodes[2] = findViewById(R.id.textView2)
        tvCodes[3] = findViewById(R.id.textView3)
        tvCodes[4] = findViewById(R.id.textView4)
        tvCodes[5] = findViewById(R.id.textView5)

        cursorViews[0] = findViewById(R.id.view0)
        cursorViews[1] = findViewById(R.id.view1)
        cursorViews[2] = findViewById(R.id.view2)
        cursorViews[3] = findViewById(R.id.view3)
        cursorViews[4] = findViewById(R.id.view4)
        cursorViews[5] = findViewById(R.id.view5)

        edtContainer.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.isNotEmpty() && codes.size < MAX_NUM) {
                    edtContainer.setText("")
                    setCode(editable.toString())
                }
            }
        })

        // 监听验证码删除按键
        edtContainer.setOnKeyListener(OnKeyListener { _: View?, keyCode: Int, keyEvent: KeyEvent ->
            if (keyCode == KeyEvent.KEYCODE_DEL && keyEvent.action == KeyEvent.ACTION_DOWN && codes.size > 0) {
                codes.removeLast()
                cursorViews[codes.size]?.isSelected = false
                showCode()
                return@OnKeyListener true
            }
            false
        })
        edtContainer.requestFocus()
        SoftInputUtils.showSoftInput(getContext(), edtContainer)
    }

    override fun setOnSendClickListener(listener: OnClickListener) {
        lcdTextView.setOnClickListener(listener)
    }

    override fun startCountDown(expiredSeconds: Long, listener: LgCountDownTextView.OnCountDownListener?) {
        lcdTextView.startCountDown(expiredSeconds, listener)
    }

    override fun stopCountDown() {
        lcdTextView.stopCountDown()
    }

    override fun setError(errorText: String?) {
        tvError.text = errorText
    }

    override fun setOnVerifyCodeListener(listener: OnVerifyCodeListener?) {
        onVerifyCodeListener = listener
    }

    override fun clearCodes() {
        codes.clear()
        showCode()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        context.activity?.apply {
            SoftInputUtils.hideSoftInput(this)
        }
    }

    private fun setCode(code: String) {
        if (TextUtils.isEmpty(code)) {
            return
        }
        for (element in code) {
            if (codes.size < MAX_NUM) {
                codes.add(element.toString())
                cursorViews[codes.size - 1]?.isSelected = true
            }
        }
        showCode()
    }

    private fun showCode() {
        for (i in 0 until MAX_NUM) {
            val textView: TextView? = tvCodes[i]
            if (codes.size > i) {
                textView?.text = codes[i]
            } else {
                textView?.text = ""
            }
        }
        val codeText = codes.joinToString("")
        if (codes.size <= MAX_NUM) {
            onVerifyCodeListener?.onTextChanged(codeText)
        }
        if (codes.size == MAX_NUM) {
            onVerifyCodeListener?.onComplete(codeText)
        }
    }

    companion object {
        const val MAX_NUM = 6
    }
}