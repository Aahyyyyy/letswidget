package com.letsfun.letswidget.sdk.core.tablayout

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.HorizontalScrollView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager2.widget.ViewPager2
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.ext.setBold
import com.letsfun.letswidget.sdk.core.utils.UiUtils

/**
 * 如果需要单独设置TextView，需要使用getTabContainer和getChildAt获取TextView，
 * 通过tabViewTextColor和tabViewTextColorSelected设置颜色，设置完以后调用refresh刷新
 */
@SuppressLint("NewApi")
class LetsTabLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : HorizontalScrollView(context, attrs, defStyle) {
    companion object {
        private const val DEFAULT_DISTRIBUTE_EVENLY = false
        private const val TAB_VIEW_TEXT_SIZE_DP = 12
        private const val TAB_CLICKABLE = true
    }

    private var tabView: LetsTabView
    private val titleOffset: Int
    private var distributeEvenly: Boolean
    private val tabViewBackgroundResId: Int

    private var viewPager: ViewPager2? = null
    private val internalTabClickListener: InternalTabClickListener?
    private var tabClickListener: ((position: Int) -> Unit)? = null

    var tabViewTextColor: Int
    var tabViewTextColorSelected: Int
    var paddingStart = 10f
    var paddingTop = 10f
    var paddingBottom = 10f
    var paddingEnd = 10f
    var tabViewTextSize: Float
    var tabViewTextSizeSelected = 0f

    init {
        // 设置为true会拉伸子view，子view的布局参数会被忽略，如果需要靠左显示设置为true
        isFillViewport = false
        // Disable the Scroll Bar
        isHorizontalScrollBarEnabled = false

        val tabBackgroundResId = NO_ID
        val textSize = UiUtils.dp2px(context, TAB_VIEW_TEXT_SIZE_DP.toFloat()).toFloat()
        val distributeEvenly = DEFAULT_DISTRIBUTE_EVENLY
        val clickable = TAB_CLICKABLE
        val titleOffset = UiUtils.dp2px(context, 0f)

        val a = context.obtainStyledAttributes(attrs, R.styleable.LetsTabLayout, defStyle, 0)
        tabViewTextColor = a.getColor(R.styleable.LetsTabLayout_android_textColor, 0)
        tabViewTextColorSelected = a.getColor(R.styleable.LetsTabLayout_lets_tl_selectedTextColor, 0)
        tabViewTextSize = a.getDimension(R.styleable.LetsTabLayout_android_textSize, textSize)
        tabViewTextSizeSelected = a.getDimension(R.styleable.LetsTabLayout_lets_tl_selectedTextSize, textSize)
        a.recycle()

        this.titleOffset = titleOffset
        tabViewBackgroundResId = tabBackgroundResId
        internalTabClickListener = if (clickable) InternalTabClickListener() else null
        this.distributeEvenly = distributeEvenly

        val params = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        tabView = LetsTabView(context)
        addView(tabView, params)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        // Ensure first scroll
        if (changed && viewPager != null) {
            scrollToTab(viewPager!!.currentItem, 0f)
        }
    }

    /**
     * Set the same weight for tab
     */
    fun setDistributeEvenly(distributeEvenly: Boolean) {
        this.distributeEvenly = distributeEvenly
    }

    /**
     * 多个tab，使用viewpager设置
     */
    fun setMultiTabViewPager(viewPager: ViewPager2?, titleCallback: (position: Int) -> String) {
        tabView.removeAllViews()
        this.viewPager = viewPager
        if (viewPager != null && viewPager.adapter != null) {
            viewPager.registerOnPageChangeCallback(onPageChangeListener)
            populateTabView(titleCallback)
        }
    }

    /**
     * 添加tab，不和viewPager绑定
     */
    fun setMultiTabEmpty(count: Int, titleCallback: (position: Int) -> String) {
        tabView.removeAllViews()
        populateTabView(titleCallback, count)
    }

    /**
     * 跳转选择的tab position
     */
    fun selectTabView(pos: Int) {
        for (i in 0 until tabView.childCount) {
            val textView = tabView.getChildAt(i) as TextView
            if (i == pos) {
                tabView.onViewPagerPageChanged(i, 0f)
                setTextViewSelected(textView, true)
                if (viewPager != null) {
                    viewPager!!.currentItem = i
                } else {
                    post {//文字布局固定以后再滑动
                        scrollToTab(pos, 0f)
                    }
                }
                tabClickListener?.invoke(i)
            } else {
                if (null == viewPager) {
                    setTextViewSelected(textView, false)
                }
            }
        }
    }

    /**
     * 设置tab点击事件
     */
    fun setTabClickListener(onClickListener: (position: Int) -> Unit) {
        tabClickListener = onClickListener
    }

    /**
     * 获取tab容器，通过getChildAt得到子view
     */
    fun getTabContainer(): ViewGroup {
        return tabView
    }

    fun refresh() {
        var i = 0
        val size = tabView.childCount
        while (i < size) {
            val v = tabView.getChildAt(i)
            setTextViewSelected(v, viewPager!!.currentItem == i, false)
            i++
        }
    }

    private fun populateTabView(titleCallback: (position: Int) -> String, viewCount: Int = -1) {
        val count = if (viewCount != -1) viewCount else viewPager!!.adapter!!.itemCount
        for (i in 0 until count) {
            val textView = createDefaultTabView(titleCallback.invoke(i), i)
            if (internalTabClickListener != null) {
                textView.setOnClickListener(internalTabClickListener)
            }
            tabView.addView(textView)
        }
        var position = -1
        var i = 0
        val size = tabView.childCount
        while (i < size) {
            if (viewPager != null && i == viewPager!!.currentItem) {
                position = i
            }
            val v = tabView.getChildAt(i)
            setTextViewSelected(v, position == i, false)
            i++
        }
    }

    private fun createDefaultTabView(title: CharSequence?, index: Int): TextView {
        val textView = TextView(context)
        textView.gravity = Gravity.CENTER or Gravity.BOTTOM
        textView.text = title
        textView.setTextColor(tabViewTextColor)
        if (index == 0) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, tabViewTextSizeSelected)
        } else {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, tabViewTextSize)
        }
        textView.includeFontPadding = false
        textView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (tabViewBackgroundResId != NO_ID) {
            textView.setBackgroundResource(tabViewBackgroundResId)
        }
        textView.setBold(true)
        return textView
    }

    /**
     * 滚动tab，目前的策略是居中显示
     */
    private fun scrollToTab(tabIndex: Int, positionOffset: Float) {
        val tabViewChildCount = tabView.childCount
        if (tabViewChildCount == 0 || tabIndex < 0 || tabIndex >= tabViewChildCount) {
            return
        }

        val isLayoutRtl = TabLayoutUtils.isLayoutRtl(this)
        val selectedTab = tabView.getChildAt(tabIndex)
        val widthPlusMargin = TabLayoutUtils.getWidth(selectedTab) + TabLayoutUtils.getMarginHorizontally(selectedTab)
        var extraOffset = (positionOffset * widthPlusMargin).toInt()
        if (tabView.isIndicatorAlwaysInCenter) {
            if (0f < positionOffset && positionOffset < 1f) {
                val nextTab = tabView.getChildAt(tabIndex + 1)
                val selectHalfWidth =
                    TabLayoutUtils.getWidth(selectedTab) / 2 + TabLayoutUtils.getMarginEnd(selectedTab)
                val nextHalfWidth = TabLayoutUtils.getWidth(nextTab) / 2 + TabLayoutUtils.getMarginStart(nextTab)
                extraOffset = Math.round(positionOffset * (selectHalfWidth + nextHalfWidth))
            }
            val firstTab = tabView.getChildAt(0)
            var x: Int
            if (isLayoutRtl) {
                val first = TabLayoutUtils.getWidth(firstTab) + TabLayoutUtils.getMarginEnd(firstTab)
                val selected = TabLayoutUtils.getWidth(selectedTab) + TabLayoutUtils.getMarginEnd(selectedTab)
                x = TabLayoutUtils.getEnd(selectedTab) - TabLayoutUtils.getMarginEnd(selectedTab) - extraOffset
                x -= (first - selected) / 2
            } else {
                val first = TabLayoutUtils.getWidth(firstTab) + TabLayoutUtils.getMarginStart(firstTab)
                val selected = TabLayoutUtils.getWidth(selectedTab) + TabLayoutUtils.getMarginStart(selectedTab)
                x = TabLayoutUtils.getStart(selectedTab) - TabLayoutUtils.getMarginStart(selectedTab) + extraOffset
                x -= (first - selected) / 2
            }
            smoothScrollTo(x, 0)
            return
        }
        var x: Int
        x = if (isLayoutRtl) {
                if (tabIndex > 0 || positionOffset > 0) titleOffset else 0
            } else {
                if (tabIndex > 0 || positionOffset > 0) -titleOffset else 0
            }
        val end = TabLayoutUtils.getEnd(selectedTab)
        val endMargin = TabLayoutUtils.getMarginEnd(selectedTab)
        x += if (isLayoutRtl) {
                end + endMargin - extraOffset - width + TabLayoutUtils.getPaddingHorizontally(this)
            } else {
                end + endMargin + extraOffset
            }
        x -= (TabLayoutUtils.getWidth(selectedTab) +
                (this.width - TabLayoutUtils.getWidth(selectedTab) + selectedTab.paddingStart + selectedTab.paddingEnd) / 2) // 居中显示
        if (x < 0) x = 0
        smoothScrollTo(x, 0)
    }

    private fun setTextViewSelected(
        view: View?,
        selected: Boolean,
        animation: Boolean = true
    ) {
        if (view != null) {
            if (view is TextView) {
                if (selected) {
                    zoomOutTextView(view, animation)
                } else {
                    zoomInTextView(view, animation)
                }
                view.setTextColor(
                    if (selected) tabViewTextColorSelected else tabViewTextColor
                )
            }
        }
    }

    private fun zoomOutTextView(
        textView: TextView,
        animation: Boolean = true
    ) {
        textView.setPadding(
            UiUtils.dp2px(context, paddingStart - 1),
            UiUtils.dp2px(context, paddingTop),
            UiUtils.dp2px(context, paddingEnd),
            UiUtils.dp2px(context, paddingBottom - 1)
        )
        textView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT
        )
        if (animation && textView.textSize < tabViewTextSizeSelected) {
            startTextViewAnimation(
                textView,
                tabViewTextSizeSelected / tabViewTextSize,
                tabViewTextSize
            )
        }
    }

    private fun zoomInTextView(
        textView: TextView,
        animation: Boolean = true
    ) {
        textView.setPadding(
            UiUtils.dp2px(context, paddingStart),
            UiUtils.dp2px(context, paddingTop),
            UiUtils.dp2px(context, paddingEnd),
            UiUtils.dp2px(context, paddingBottom)
        )
        textView.layoutParams = LinearLayout.LayoutParams(//底部对齐
            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT
        )
        if (animation && textView.textSize > tabViewTextSize) {//只对大字体进行缩放，原来就是小字体的不缩放
            startTextViewAnimation(
                textView,
                tabViewTextSize / tabViewTextSizeSelected,
                tabViewTextSizeSelected
            )
        }
    }

    //固定住tab的高度，不让其跟随动画变化
    private fun resetTabHeight(fixed: Boolean) {
        tabView.layoutParams.apply {
            if (fixed && tabView.height != 0) {
                height = tabView.height
            } else {
                height = LayoutParams.WRAP_CONTENT
            }
        }
    }

    private fun startTextViewAnimation(textView: TextView, endScale: Float, startSize: Float) {
        val valueAnimator = ValueAnimator.ofFloat(1f, endScale)
        valueAnimator.addListener(
        object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
                resetTabHeight(true)
            }

            override fun onAnimationEnd(animation: Animator) {
                resetTabHeight(false)
            }

            override fun onAnimationCancel(animation: Animator) {
                resetTabHeight(false)
            }

            override fun onAnimationRepeat(animation: Animator) {}

        })
        valueAnimator.addUpdateListener {
            textView.setTextSize(
                TypedValue.COMPLEX_UNIT_PX,
                startSize * (it.animatedValue as Float)
            )
        }
        valueAnimator.duration = 300
        valueAnimator.start()
    }

    private val onPageChangeListener = object : ViewPager2.OnPageChangeCallback() {
        private var scrollState = 0
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
            super.onPageScrolled(position, positionOffset, positionOffsetPixels)
            val tabViewChildCount = tabView.childCount
            if (tabViewChildCount == 0 || position < 0 || position >= tabViewChildCount) {
                return
            }
        }

        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            tabView.onViewPagerPageChanged(position, 0f)
            var i = 0
            val size = tabView.childCount
            while (i < size) {
                val v = tabView.getChildAt(i)
                setTextViewSelected(v, position == i)
                i++
            }
            post {//文字布局固定以后再滑动
                scrollToTab(position, 0f)
            }
        }

        override fun onPageScrollStateChanged(state: Int) {
            super.onPageScrollStateChanged(state)
            scrollState = state
        }
    }

    private class LetsTabView(context: Context) : LinearLayout(context) {
        companion object {
            private const val GRAVITY_BOTTOM = 0
            private const val GRAVITY_TOP = 1
            private const val GRAVITY_CENTER = 2

            private const val DEFAULT_INDICATOR_WITHOUT_PADDING = false
            private const val DEFAULT_INDICATOR_IN_CENTER = true
            private const val AUTO_WIDTH = -1
            private const val DEFAULT_INDICATOR_WIDTH = 20f
            private const val DEFAULT_INDICATOR_THICKNESS = 5f
            private const val DEFAULT_INDICATOR_PADDING_BOTTOM = 3f
            private const val DEFAULT_INDICATOR_GRAVITY = GRAVITY_BOTTOM
            private const val DEFAULT_INDICATOR_RADIUS = 10f
        }

        private val drawDecorationAfterTab: Boolean

        private var lastPosition = 0
        private var selectedPosition = 0
        private var selectionOffset = 0f

        private val indicatorRectF = RectF()
        private val indicatorWithoutPadding: Boolean
        val isIndicatorAlwaysInCenter: Boolean
        private var indicatorWidth: Int
        private var indicatorThickness: Int
        private val indicatorPaddingBottom: Int
        private val indicatorGravity: Int
        private val indicatorColor: Int
        private val indicatorCornerRadius: Float
        private val indicatorPaint: Paint

        init {
            setWillNotDraw(false)

            var drawDecorationAfterTab = true
            var indicatorWithoutPadding = DEFAULT_INDICATOR_WITHOUT_PADDING
            var isIndicatorAlwaysInCenter = DEFAULT_INDICATOR_IN_CENTER
            var indicatorWidth = UiUtils.dp2px(context, DEFAULT_INDICATOR_WIDTH)
            var indicatorThickness = UiUtils.dp2px(context, DEFAULT_INDICATOR_THICKNESS)
            var indicatorPaddingBottom = UiUtils.dp2px(context, DEFAULT_INDICATOR_PADDING_BOTTOM)
            var indicatorGravity = DEFAULT_INDICATOR_GRAVITY
            var indicatorColor = context.getColor(R.color.lets_widget_core_color_basic_primary)
            var indicatorCornerRadius = UiUtils.dp2px(context, DEFAULT_INDICATOR_RADIUS).toFloat()

            this.drawDecorationAfterTab = drawDecorationAfterTab
            this.indicatorWithoutPadding = indicatorWithoutPadding
            this.isIndicatorAlwaysInCenter = isIndicatorAlwaysInCenter
            this.indicatorWidth = indicatorWidth
            this.indicatorThickness = indicatorThickness
            this.indicatorPaddingBottom = indicatorPaddingBottom
            this.indicatorGravity = indicatorGravity
            this.indicatorColor = indicatorColor
            this.indicatorCornerRadius = indicatorCornerRadius
            indicatorPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        }

        fun setIndicatorWidth(width: Float) {
            indicatorWidth = UiUtils.dp2px(context, width)
            invalidate()
        }

        fun setIndicatorThickness(thickness: Float) {
            indicatorThickness = UiUtils.dp2px(context, thickness)
            invalidate()
        }

        fun onViewPagerPageChanged(position: Int, positionOffset: Float) {
            selectedPosition = position
            selectionOffset = positionOffset
            if (positionOffset == 0f && lastPosition != selectedPosition) {
                lastPosition = selectedPosition
            }
            invalidate()
        }

        override fun onDraw(canvas: Canvas) {
            if (!drawDecorationAfterTab) {
                drawDecoration(canvas)
            }
        }

        override fun dispatchDraw(canvas: Canvas) {
            super.dispatchDraw(canvas)
            if (drawDecorationAfterTab) {
                drawDecoration(canvas)
            }
        }

        private fun drawDecoration(canvas: Canvas) {
            val height = height
            val tabCount = childCount
            val isLayoutRtl = TabLayoutUtils.isLayoutRtl(this)

            if (tabCount > 1) {
                val selectedTab = getChildAt(selectedPosition)
                val selectedStart = TabLayoutUtils.getStart(selectedTab, indicatorWithoutPadding)
                val selectedEnd = TabLayoutUtils.getEnd(selectedTab, indicatorWithoutPadding)
                val left: Int
                val right: Int
                if (isLayoutRtl) {
                    left = selectedEnd
                    right = selectedStart
                } else {
                    left = selectedStart
                    right = selectedEnd
                }
                val color = indicatorColor
                val thickness = indicatorThickness.toFloat()
                drawIndicator(canvas, left, right, height, thickness, color)
            }
        }

        private fun drawIndicator(
            canvas: Canvas, left: Int, right: Int, height: Int, thickness: Float,
            color: Int
        ) {
            if (indicatorThickness <= 0 || indicatorWidth == 0) {
                return
            }

            val center: Float
            val top: Float
            val bottom: Float
            when (indicatorGravity) {
                GRAVITY_TOP -> {
                    center = indicatorThickness / 2f
                    top = center - thickness / 2f
                    bottom = center + thickness / 2f
                }
                GRAVITY_CENTER -> {
                    center = height / 2f
                    top = center - thickness / 2f
                    bottom = center + thickness / 2f
                }
                GRAVITY_BOTTOM -> {
                    center = height - indicatorThickness / 2f
                    top = center - thickness / 2f - indicatorPaddingBottom
                    bottom = center + thickness / 2f - indicatorPaddingBottom
                }
                else -> {
                    center = height - indicatorThickness / 2f
                    top = center - thickness / 2f
                    bottom = center + thickness / 2f
                }
            }
            indicatorPaint.color = color
            if (indicatorWidth == AUTO_WIDTH) {
                indicatorRectF[left.toFloat(), top, right.toFloat()] = bottom
            } else {
                val padding = (Math.abs(left - right) - indicatorWidth) / 2f
                indicatorRectF[left + padding, top, right - padding] = bottom
            }
            if (indicatorCornerRadius > 0f) {
                canvas.drawRoundRect(
                    indicatorRectF, indicatorCornerRadius,
                    indicatorCornerRadius, indicatorPaint
                )
            } else {
                canvas.drawRect(indicatorRectF, indicatorPaint)
            }
        }

    }

    private inner class InternalTabClickListener : OnClickListener {
        override fun onClick(v: View) {
            for (i in 0 until tabView.childCount) {
                val sv = tabView.getChildAt(i)
                if (v === sv) {
                    tabView.onViewPagerPageChanged(i, 0f)
                    setTextViewSelected(sv as TextView, true)
                    if (viewPager != null) {
                        viewPager!!.currentItem = i
                    } else {
                        post {//文字布局固定以后再滑动
                            scrollToTab(i, 0f)
                        }
                    }
                    tabClickListener?.invoke(i)
                } else {
                    if (null == viewPager) {
                        setTextViewSelected(sv as TextView, false)
                    }
                }
            }
        }
    }
}