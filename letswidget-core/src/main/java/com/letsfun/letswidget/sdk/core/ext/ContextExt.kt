package com.letsfun.letswidget.sdk.core.ext

import android.app.Activity
import android.content.Context
import com.letsfun.letswidget.sdk.core.utils.ActivityUtils

val Context.activity: Activity?
    get() = ActivityUtils.scanForActivity(this)