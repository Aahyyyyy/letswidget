package com.letsfun.letswidget.sdk.core.butler.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils

class LetsMeButler(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    init {
        LayoutInflater.from(context).inflate(R.layout.lets_widget_core_layout_custom_me_butler, this)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val measureWidth = MeasureSpec.getSize(widthMeasureSpec)
        val measureHeight = UiUtils.dp2px(context, 50f)

        setMeasuredDimension(measureWidth, measureHeight)
    }
}