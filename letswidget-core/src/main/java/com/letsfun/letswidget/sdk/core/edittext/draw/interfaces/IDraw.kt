package com.letsfun.letswidget.sdk.core.edittext.draw.interfaces

import android.graphics.Canvas
import android.graphics.Paint

interface IDraw {
    fun onDraw(canvas: Canvas?)

    fun getDrawPaint(): Paint
}