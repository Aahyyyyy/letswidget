package com.letsfun.letswidget.sdk.core.loading

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.animation.AccelerateDecelerateInterpolator
import java.util.*

class LetsTwoDots : LetsBaseDot() {
    var offsetX = FloatArray(2)
    override fun draw(canvas: Canvas, paint: Paint) {
        val radius = height.toFloat() / 8
        val centerY = height.toFloat() / 2
        for (i in 0..1) {
            if (i == 0) {
                paint.color = Color.parseColor("#F5605B")
            } else {
                paint.color = Color.parseColor("#6BC6FF")
            }
            canvas.drawCircle(offsetX[i], centerY, radius, paint)
        }
    }

    @SuppressLint("NewApi")
    override fun onCreateAnimators(): ArrayList<ValueAnimator> {
        val animators = ArrayList<ValueAnimator>()
        val startX = width.toFloat() / 40 * 7
        for (i in 0..1) {
            var translateXAnim = ValueAnimator.ofFloat(startX, width - startX, startX)
            if (i == 1) {
                translateXAnim = ValueAnimator.ofFloat(width - startX, startX, width - startX)
            }
            translateXAnim.duration = 1200
            translateXAnim.interpolator = AccelerateDecelerateInterpolator()
            translateXAnim.repeatCount = -1
            addUpdateListener(translateXAnim) { animation ->
                offsetX[i] = animation.animatedValue as Float
                postInvalidate()
            }
            animators.add(translateXAnim)
        }
        return animators
    }
}