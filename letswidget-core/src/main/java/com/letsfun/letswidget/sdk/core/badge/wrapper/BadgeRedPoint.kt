package com.letsfun.letswidget.sdk.core.badge.wrapper

import android.content.Context
import android.view.Gravity
import android.view.View
import com.letsfun.letswidget.sdk.core.badge.interfaces.ILetsBadge
import com.letsfun.letswidget.sdk.core.badge.impl.LetsBadgeImpl

class LetsBadgeRedPoint24(context: Context?, targetView: View?, badgeView: ILetsBadge = LetsBadgeImpl(context)) :
    ILetsBadge by badgeView {
    init {
        badgeView.bindTarget(targetView)
        badgeView.isShowShadow = false
        badgeView.setBadgePadding(5f, true)
        badgeView.setGravityOffset(0f, 3f, true)
        badgeView.badgeNumber = -1
    }
}

class LetsBadgeRedPoint50(context: Context?, targetView: View?, badgeView: ILetsBadge = LetsBadgeImpl(context)) :
    ILetsBadge by badgeView {
    init {
        badgeView.bindTarget(targetView)
        badgeView.isShowShadow = false
        badgeView.setBadgePadding(5f, true)
        badgeView.setGravityOffset(3f, 0f, true)
        badgeView.badgeNumber = -1
    }
}

class LetsBadgeRedPointCenter(
    context: Context?,
    targetView: View?,
    badgeView: ILetsBadge = LetsBadgeImpl(context),
    xOffSet: Float = 0f,
    yOffSet: Float = 0f,
    padding: Float = 5f,
) : ILetsBadge by badgeView {
    init {
        badgeView.bindTarget(targetView)
        badgeView.isShowShadow = false
        badgeView.setBadgePadding(padding, true)
        badgeView.setGravityOffset(xOffSet, yOffSet, true)
        badgeView.badgeGravity = Gravity.CENTER or Gravity.END
        badgeView.badgeNumber = -1
    }
}