package com.letsfun.letswidget.sdk.core.edittext.draw

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.text.TextUtils
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.edittext.draw.interfaces.IDrawText
import com.letsfun.letswidget.sdk.core.utils.UiUtils

class ErrorTextImpl(val view: TextView) : IDrawText {

    companion object {
        const val ERROR_TEXT_HEIGHT = 26f
        const val ERROR_TEXT_SIZE = 13f
    }

    private var position: PointF = PointF()

    private val paint: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            color = ContextCompat.getColor(view.context, R.color.lets_widget_core_color_red_6)
            textSize = view.context.resources.getDimension(R.dimen.lets_widget_core_font_size_10)
        }
    }

    private val paintHintText: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            color =
                ContextCompat.getColor(view.context, R.color.lets_widget_core_color_text_tertiary)
            textSize = view.context.resources.getDimension(R.dimen.lets_widget_core_font_size_10)
        }
    }
    private var errorText: String = ""
    private var errorTextHint: String = ""

    override fun setText(text: String) {
        errorText = text
    }

    fun setHintText(textHint: String) {
        errorTextHint = textHint
    }

    override fun setPosition(position: PointF) {
        this.position = position
    }

    override fun getText(): CharSequence = errorText

    override fun getDrawPaint(): Paint {
        return paint
    }

    fun isActive(): Boolean {
        return !TextUtils.isEmpty(errorText)
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {
        val offsetY = UiUtils.dp2px(view.context, (ERROR_TEXT_HEIGHT - ERROR_TEXT_SIZE + 1) / 2)
        val x = view.scrollX
        val y = view.height + view.scrollY - offsetY

        setPosition(PointF(x.toFloat(), y.toFloat()))

        if (!TextUtils.isEmpty(errorText)) {
            canvas?.drawText(errorText, position.x, position.y, paint)
        } else if (!TextUtils.isEmpty(errorTextHint)) {
            canvas?.drawText(errorTextHint, position.x, position.y, paintHintText)
        }
    }
}