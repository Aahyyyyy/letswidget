package com.letsfun.letswidget.sdk.core.badge.impl

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils
import com.letsfun.letswidget.sdk.core.view.LetsBadge

@SuppressLint("CustomViewStyleable")
class BadgeIconView(context: Context, var dotType: Int = DEFAULT_ICON_TYPE, attrs: AttributeSet? = null) :
    AppCompatImageView(context, attrs) {

    companion object {
        const val LEVEL_20_SIZE = 20f
        const val LEVEL_26_SIZE = 26f

        const val DEFAULT_ICON_TYPE = LetsBadge.DotType.LEVEL_20
    }

    private var size: Int = 0

    init {
        if (attrs != null) {
            context.obtainStyledAttributes(attrs, R.styleable.LetsBadgeIconView).apply {
                dotType = getInteger(R.styleable.LetsBadgeIconView_lets_badge_icon_type, dotType)
                recycle()
            }
        }
        when (dotType) {
            LetsBadge.DotType.LEVEL_20 -> {
                size = UiUtils.dp2px(context, LEVEL_20_SIZE)
                setBackgroundResource(R.drawable.lg_widget_core_icon_nio_20)
            }
            LetsBadge.DotType.LEVEL_26 -> {
                size = UiUtils.dp2px(context, LEVEL_26_SIZE)
                setBackgroundResource(R.drawable.lg_widget_core_icon_nio_26)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        layoutParams.width = size
        layoutParams.height = size
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }
}