package com.letsfun.letswidget.sdk.core.edittext.base

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.text.InputType
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.edittext.draw.BaseLineImpl
import com.letsfun.letswidget.sdk.core.edittext.draw.ErrorTextImpl
import com.letsfun.letswidget.sdk.core.edittext.icon.LeadDrawableImpl
import com.letsfun.letswidget.sdk.core.edittext.icon.TailDrawableImpl
import com.letsfun.letswidget.sdk.core.edittext.listener.OnIconClickListener
import com.letsfun.letswidget.sdk.core.utils.UiUtils

@SuppressLint("CustomViewStyleable", "NewApi")
open class LetsBaseEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.editTextStyle
) : AppCompatEditText(context, attrs, defStyleAttr), IBaseEditText {

    companion object {
        const val DRAWABLE_PADDING = 10f
        const val PADDING_TOP = 15f
        const val PADDING_BOTTOM = 5f

        const val INVALID_RES_ID = 0
        const val DISABLED_ICON_ALPHA = 0.1f
    }

    private var needDrawableRefresh: Boolean = true

    private var leadIcon: Int = INVALID_RES_ID

    private var errorTextHint: String? = null

    private var isInvalidating: Boolean = false

    private val leadDrawableImpl: LeadDrawableImpl by lazy {
        LeadDrawableImpl(this)
    }

    private val tailDrawableImpl: TailDrawableImpl by lazy {
        TailDrawableImpl(this)
    }

    private val errorTextImpl: ErrorTextImpl by lazy {
        ErrorTextImpl(this)
    }

    private val baseLineImpl: BaseLineImpl by lazy {
        BaseLineImpl(this)
    }

    init {
        background = null
        this.setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            context.resources.getDimension(R.dimen.lets_widget_core_text_size_content)
        )
        this.setTextColor(ContextCompat.getColorStateList(context, R.color.lets_widget_core_color_select_edittext_text))
        setHintTextColor(
            ContextCompat.getColor(
                context,
                R.color.lets_widget_core_color_text_tertiary
            )
        )

        if (attrs != null) {
            context.obtainStyledAttributes(attrs, R.styleable.LetsEditText).apply {
                leadIcon = getInt(R.styleable.LetsEditText_lets_edittext_leadIcon, 0)
                errorTextHint = getString(R.styleable.LetsEditText_lets_edittext_errorTextHint)
                recycle()
            }
        }

        this.setLeadIcon(leadIcon)
        setErrorHint(errorTextHint)
    }

    override fun setLeadIcon(@DrawableRes iconResId: Int) {
        if (leadDrawableImpl.setIcon(iconResId)) {
            needDrawableRefresh = true
            postInvalidateView()
        }
    }

    override fun setTailIcon(@DrawableRes primaryIcon: Int, @DrawableRes secondaryIcon: Int) {
        if (tailDrawableImpl.setIcon(primaryIcon, secondaryIcon)) {
            needDrawableRefresh = true
            postInvalidateView()
        }
    }

    // 重写原生setError方法，禁用原生方法以避免功能冲突
    override fun setError(error: CharSequence?) {
        errorTextImpl.setText(error?.toString() ?: "")
        postInvalidateView()
    }

    override fun getError(): CharSequence {
        return errorTextImpl.getText()
    }

    fun setErrorHint(errorHint: String?) {
        errorTextImpl.setHintText(errorHint ?: "")
        postInvalidateView()
    }

    @Deprecated("原生setError已禁用", ReplaceWith("setError(error)"))
    override fun setError(error: CharSequence?, icon: Drawable?) {
        setError(error)
    }

    override fun setOnLeadIconClickListener(onClickListener: OnIconClickListener?) {
        leadDrawableImpl.setOnIconClickListener(onClickListener)
    }

    override fun setOnTailPrimaryIconClickListener(onClickListener: OnIconClickListener?) {
        tailDrawableImpl.setOnPrimaryIconClickListener(onClickListener)
    }

    override fun setOnTailSecondaryIconClickListener(onClickListener: OnIconClickListener?) {
        tailDrawableImpl.setOnSecondaryIconClickListener(onClickListener)
    }

    override fun invalidateView() {
        if (isEnabled) {
            leadDrawableImpl.setAlpha(1f)
            tailDrawableImpl.setAlpha(1f)
        } else {
            leadDrawableImpl.setAlpha(DISABLED_ICON_ALPHA)
            tailDrawableImpl.setAlpha(DISABLED_ICON_ALPHA)
        }

        var leadDrawable: Drawable? = null
        var tailDrawable: Drawable? = null
        if (needDrawableRefresh) {
            leadDrawable = leadDrawableImpl.getDrawable()
            tailDrawable = tailDrawableImpl.getDrawable()
        }
        setCompoundDrawables(leadDrawable, null, tailDrawable, null)
        compoundDrawablePadding = UiUtils.dp2px(context, DRAWABLE_PADDING)
    }

    override fun postInvalidateView() {
        if (!isInvalidating) {
            isInvalidating = true
            post {
                isInvalidating = false
                invalidateView()
            }
        }
    }

    private fun updatePadding() {
        val topPadding = UiUtils.dp2px(context, PADDING_TOP)
        val bottomPadding = UiUtils.dp2px(context, PADDING_BOTTOM + ErrorTextImpl.ERROR_TEXT_HEIGHT)

        setPadding(0, topPadding, 0, bottomPadding)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        updatePadding()
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        baseLineImpl.setColor(getBaseLineColor())
        baseLineImpl.onDraw(canvas)

        if (isEnabled) {
            errorTextImpl.onDraw(canvas)
        }
    }

    private fun getBaseLineColor(): Int {
        return ContextCompat.getColor(
            context, if (!isEnabled) {
                R.color.lets_widget_core_color_border_divider_primary
            } else if (isFocused && errorTextImpl.isActive()) {
                // Error提示
                R.color.lets_widget_core_color_support_error_4
            } else if (isFocused) {
                R.color.lets_widget_core_color_basic_primary
            } else {
                R.color.lets_widget_core_color_border_disable
            }
        )
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (leadDrawableImpl.dealWithTouchEvent(event)) {
            return true
        } else if (tailDrawableImpl.dealWithTouchEvent(event)) {
            return true
        }
        return super.onTouchEvent(event)
    }

    private fun isInPasswordMode(): Boolean {
        return (inputType and InputType.TYPE_TEXT_VARIATION_PASSWORD == InputType.TYPE_TEXT_VARIATION_PASSWORD)
    }
}