package com.letsfun.letswidget.sdk.core.edittext.base

import android.view.View
import com.letsfun.letswidget.sdk.core.edittext.listener.OnVerifyCodeListener
import com.letsfun.letswidget.sdk.core.view.LgCountDownTextView

interface IVerifyCodeEditText {
    /**
     * 设置发送按钮点击监听
     */
    fun setOnSendClickListener(listener: View.OnClickListener)

    /**
     * 开始倒计时
     *
     * @param expiredSeconds 倒计时长(s)
     * @param listener 倒计时回调监听
     */
    fun startCountDown(expiredSeconds: Long, listener: LgCountDownTextView.OnCountDownListener?)

    /**
     * 停止倒计时
     * 一般情况不需要调用，倒计时会自动停止
     */
    fun stopCountDown()

    /**
     * 设置error提示文案
     * 如果需要清除，设置为"" 或者 null
     */
    fun setError(errorText: String?)

    /**
     * 验证码输入监听回调
     *
     */
    fun setOnVerifyCodeListener(listener: OnVerifyCodeListener?)

    /**
     * 清除验证码输入框
     */
    fun clearCodes()
}