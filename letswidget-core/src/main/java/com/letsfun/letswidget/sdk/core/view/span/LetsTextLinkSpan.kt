package com.letsfun.letswidget.sdk.core.view.span

import android.annotation.SuppressLint
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorRes
import com.letsfun.letswidget.lib.core.AppContext
import com.letsfun.letswidget.sdk.core.R

class LetsTextLinkSpan: UnderlineSpan() {

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        ds.color = ds.linkColor
        ds.isUnderlineText = false
        ds.clearShadowLayer()
    }

}

@SuppressLint("NewApi")
fun TextView.letsLinkText(colorText: String, @ColorRes colorRes:Int = R.color.lets_widget_core_color_text_link_selector, click:(()->Unit)?=null) {
    this.highlightColor = Color.TRANSPARENT
    val style = SpannableStringBuilder()
    val index = text.indexOf(colorText,0)
    style.append(text)
    val clickableSpan = object : ClickableSpan(){
        override fun onClick(widget: View) {
            click?.invoke()
        }
    }
    style.setSpan(clickableSpan, index, index + colorText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    val linkSpan = LetsTextLinkSpan()
    style.setSpan(linkSpan, index, index + colorText.length, Spanned.SPAN_MARK_MARK)
    val foregroundColorSpan = ForegroundColorSpan(AppContext.getApp().getColor(colorRes))
    style.setSpan(foregroundColorSpan, index, index + colorText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    movementMethod = LinkMovementMethod.getInstance()
    text = style
}
