package com.letsfun.letswidget.sdk.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils

@SuppressLint("NewApi")
abstract class LetsBaseDialog : Dialog {

    constructor(context: Context) : super(context, R.style.LetsBaseDialog)
    constructor(context: Context, themeResId: Int) : super(context, themeResId)
    constructor(
        context: Context,
        cancelable: Boolean,
        cancelListener: DialogInterface.OnCancelListener?
    ) : super(context, cancelable, cancelListener)

    private lateinit var customView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        customView = LayoutInflater.from(context).inflate(customResourceId, null)
        setContentView(customView)
        initWindowAttr()
    }

    /**
     * 设置 window 的属性，比如显示动画、位置
     */
    private fun initWindowAttr() {
        window?.attributes?.apply {
            width = getDialogWidth()
            height = getDialogHeight()
            gravity = windowGravity
        }
        window?.decorView?.setBackgroundColor(getColor(android.R.color.transparent))
        if (!isNeedMaxHeight()) return
        when (windowGravity) {
            Gravity.BOTTOM -> {
                window?.attributes?.windowAnimations = R.style.BaseDialogBottomAnim
                (customView.layoutParams as FrameLayout.LayoutParams).topMargin =
                    windowTopMargin ?: getDimensionPixelSize(R.dimen.lets_widget_core_dialog_margin_top_bottom)
            }
            Gravity.TOP -> (customView.layoutParams as FrameLayout.LayoutParams).bottomMargin =
                windowBottomMargin ?: getDimensionPixelSize(R.dimen.lets_widget_core_dialog_margin_top_bottom)
            Gravity.CENTER -> {
                window?.attributes?.windowAnimations = R.style._dialog_center_anim_style
                val marginTopBottom =
                    (getScreenHeight() - getDimensionPixelSize(R.dimen.lets_dialog_max_height)) / 2
                (customView.layoutParams as FrameLayout.LayoutParams).apply {
                    topMargin = windowTopMargin ?: marginTopBottom
                    bottomMargin = windowBottomMargin ?: marginTopBottom
                }
            }
        }
    }

    private var startY = 0f
    private var moveY = 0f
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val touchCancel = super.onTouchEvent(event)
        if (touchCancel){
            return true
        }
        if (canScrollCancel && window?.decorView != null){
            val decorView = window!!.decorView
            if (isOutOfBounds(event, decorView)) {
                return false
            }
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_DOWN -> {
                    startY = event.y
                }
                MotionEvent.ACTION_MOVE -> {
                    moveY = event.y - startY
                    decorView.scrollBy(0, -moveY.toInt())
                    startY = event.y
                    if (decorView.scrollY > 0) { //避免向上拖动
                        decorView.scrollTo(0, 0)
                    }
                }
                MotionEvent.ACTION_UP -> {
                    if (decorView.scrollY < -decorView.height / 5 && moveY > 0) { //如果滑动距离达到高度的5分之一 就关闭Dialog
                        dismiss()
                    }
                    decorView.scrollTo(0, 0)
                }
            }
        }
        return false
    }

    /**
     * 是否在view区域以外
     */
    private fun isOutOfBounds(event: MotionEvent, view: View): Boolean {
        val x = event.x.toInt()
        val y = event.y.toInt()
        val slop = ViewConfiguration.get(context).scaledWindowTouchSlop
        return (x < -slop || y < -slop || x > view.width + slop || y > view.height + slop)
    }

    protected open fun isNeedMaxHeight(): Boolean = true

    protected open fun getDialogWidth(): Int =
        if (windowGravity == Gravity.CENTER) (UiUtils.dp2px(context, 320f)) else ViewGroup.LayoutParams.MATCH_PARENT

    protected open fun getDialogHeight(): Int = ViewGroup.LayoutParams.WRAP_CONTENT

    protected fun getScreenWidth(): Int = context.resources.displayMetrics.widthPixels

    protected fun getScreenHeight(): Int = context.resources.displayMetrics.heightPixels

    protected fun getDimensionPixelSize(@DimenRes resId: Int): Int =
        context.resources.getDimensionPixelSize(resId)

    protected fun getColor(@ColorRes resId: Int): Int = context.resources.getColor(resId, null)

    protected fun getDrawable(@DrawableRes resId: Int): Drawable =
        context.resources.getDrawable(resId, null)

    protected fun getVisibility(@StringRes resId: Int): Int =
        if (resId == 0) View.GONE else View.VISIBLE

    protected fun getVisibility(value: String?): Int =
        if (value == null) View.GONE else View.VISIBLE

    /**
     * 设置 Dialog 的 ContentId
     * @return Dialog 的 ContentId
     */
    protected abstract val customResourceId: Int

    /**
     * 获取 Dialog 窗口显示位置
     * @return Dialog 的窗口显示位置
     */
    protected abstract val windowGravity: Int

    //zzy 支持设置margin，适应全屏dialog
    open var windowTopMargin: Int? = null

    open var windowBottomMargin: Int? = null

    //是否可以下滑关闭弹窗
    open var canScrollCancel: Boolean = false
}