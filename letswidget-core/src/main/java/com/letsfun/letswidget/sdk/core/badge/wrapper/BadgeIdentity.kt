package com.letsfun.letswidget.sdk.core.badge.wrapper
import android.content.Context
import android.view.Gravity
import android.view.View
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.sdk.core.badge.interfaces.ILetsBadge
import com.letsfun.letswidget.sdk.core.badge.impl.LetsBadgeImpl

class LetsBadgeIdentity50(context: Context, targetView: View, @DrawableRes resId: Int, badgeView: ILetsBadge = LetsBadgeImpl(context)) :
    ILetsBadge by badgeView{
    init {
        badgeView.bindTarget(targetView)
        badgeView.badgeGravity = Gravity.END or Gravity.BOTTOM
        badgeView.setGravityOffset(0f, 0f, true)
        badgeView.setBadgePadding(7.5f, 7.5f, true)
        badgeView.badgeBackground = ContextCompat.getDrawable(context, resId)
        badgeView.badgeNumber = -1
    }
}