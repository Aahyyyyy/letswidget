package com.letsfun.letswidget.sdk.core.edittext.draw

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.text.TextPaint
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.edittext.base.LetsBaseEditText
import com.letsfun.letswidget.sdk.core.edittext.draw.interfaces.IDrawLine
import com.letsfun.letswidget.sdk.core.utils.UiUtils

class BaseLineImpl(val view: TextView) : IDrawLine {

    private val paint: TextPaint by lazy {
        TextPaint().apply {
            color = ContextCompat.getColor(view.context, R.color.lets_widget_core_color_border_disable)
            style = Paint.Style.STROKE
            strokeWidth = UiUtils.dp2px(view.context, 1f).toFloat()
        }
    }

    private var startPosition: PointF = PointF()
    private var endPosition: PointF = PointF()

    override fun getDrawPaint(): Paint {
        return paint
    }

    override fun setColor(colorId: Int) {
        paint.color = colorId
    }

    override fun setPosition(startPosition: PointF, endPosition: PointF) {
        this.startPosition = startPosition
        this.endPosition = endPosition
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {
        setPosition(
            PointF(
                view.scrollX.toFloat(),
                view.height.toFloat() + view.scrollY.toFloat()
                        - UiUtils.dp2px(view.context, ErrorTextImpl.ERROR_TEXT_HEIGHT)
            ),
            PointF(
                view.scrollX.toFloat() + view.width.toFloat(),
                view.height.toFloat() + view.scrollY.toFloat()
                        - UiUtils.dp2px(view.context, ErrorTextImpl.ERROR_TEXT_HEIGHT)
            )
        )
        canvas?.drawLine(startPosition.x, startPosition.y, endPosition.x, endPosition.y, paint)
    }
}