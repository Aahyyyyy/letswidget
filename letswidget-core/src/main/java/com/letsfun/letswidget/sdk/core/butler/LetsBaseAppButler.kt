package com.letsfun.letswidget.sdk.core.butler

import android.animation.Animator
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Parcelable
import android.text.TextUtils
import android.util.AttributeSet
import android.view.*
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.airbnb.lottie.LottieAnimationView
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.ext.toGone
import com.letsfun.letswidget.sdk.core.ext.toVisible
import com.letsfun.letswidget.sdk.core.utils.UiUtils
import java.util.*

class LetsBaseAppButler(context: Context, attrs: AttributeSet) : LinearLayoutCompat(context, attrs), ILetsBaseAppButler {
    companion object {
        const val PADDING_HORIZONTAL = 20f
        const val TAB_SIZE = 50f
        const val IMAGE_SIZE_S = 24f
        const val IMAGE_SIZE_L = 48f

        const val DEFAULT_MSG_COUNT_LIMIT = 99

        const val MSG_DOT_ANIM_DURATION: Long = 300
    }

    private var butlerItems: List<ButlerItem> = LinkedList()
    private var viewHolders: MutableList<ViewHolder> = LinkedList()

    private var onButlerListener: OnButlerListener? = null
    private var onDoubleClickListener: OnDoubleClickListener? = null
    private var onInterruptSelectListener: OnInterruptSelectListener? = null

    private var selectedHolder: ViewHolder? = null
    private var currentPosition: Int = -1
    private var currentImageSize: Int = 0

    private var maxMsgCountLimit = DEFAULT_MSG_COUNT_LIMIT
    private var animMsgAlpha: ObjectAnimator? = null

    private var viewPager: ViewPager? = null
    private var viewPager2: ViewPager2? = null
    private val onPageChangeListener by lazy {
        object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                selectButler(position, false)
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        }
    }

    private val onPageChangeCallback by lazy {
        object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                selectButler(position, false)
            }
        }
    }

    init {
        gravity = Gravity.CENTER_VERTICAL

        showDividers = SHOW_DIVIDER_MIDDLE
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val measureWidth = MeasureSpec.getSize(widthMeasureSpec)
        val measureHeight = UiUtils.dp2px(context, TAB_SIZE)

        setMeasuredDimension(measureWidth, measureHeight)
    }

    override fun setButlerItems(butlerItems: List<ButlerItem>) {
        this.butlerItems = butlerItems

        updateButlerView()
    }

    override fun selectButler(position: Int, needInterrupt: Boolean, ignoreCurrentPosition: Boolean) {
        if (position >= childCount) {
            throw IndexOutOfBoundsException()
        }
        if (needInterrupt
            && onInterruptSelectListener?.onInterrupt(butlerItems[position], position) == true
        ) {
            return
        }
        if (position != currentPosition || ignoreCurrentPosition) {
            currentPosition = position
            if (ignoreCurrentPosition) {
                selectView(position, position == currentPosition)
            } else {
                selectView(position, ignoreCurrentPosition)
            }

            viewPager?.currentItem = position
            viewPager2?.setCurrentItem(position, false)
            onButlerListener?.onButlerSelected(butlerItems[position], position)
        }
    }

    override fun getCurrentPosition(): Int = currentPosition

    override fun setButlerEnable(position: Int, enable: Boolean) {
        if (position < childCount) {
            setEnabled(getChildAt(position), enable)
        } else {
            throw IndexOutOfBoundsException()
        }
    }

    override fun setMessageCount(position: Int, msgCount: Int) {
        if (position < childCount) {
            setMsgCount(getChildAt(position), msgCount)
        } else {
            throw IndexOutOfBoundsException()
        }
    }

    override fun setMaxMsgCountLimit(limit: Int) {
        maxMsgCountLimit = limit.coerceAtLeast(1)
    }

    override fun setOnButlerListener(listener: OnButlerListener?) {
        this.onButlerListener = listener
    }

    override fun setOnDoubleClickListener(listener: OnDoubleClickListener?) {
        this.onDoubleClickListener = listener
    }

    override fun setOnInterruptSelectListener(listener: OnInterruptSelectListener) {
        onInterruptSelectListener = listener
    }

    override fun setupWithViewPager(viewPager: ViewPager?) {
        this.viewPager = viewPager
        this.viewPager?.addOnPageChangeListener(onPageChangeListener)
    }

    override fun setupWithViewPager(viewPager: ViewPager2?) {
        this.viewPager2 = viewPager
        this.viewPager2?.registerOnPageChangeCallback(onPageChangeCallback)
    }

    private fun updateButlerView() {
        removeAllViews()
        viewHolders.clear()
        for (butlerItem in butlerItems) {
            addButlerView(butlerItem)
        }

        updateLayout()

        if (currentPosition == -1) {
            selectButler(0)
        }
    }

    private fun updateLayout() {
        val dividerPadding = (UiUtils.getScreenWidth(context)
                - UiUtils.dp2px(context, PADDING_HORIZONTAL * 2)
                - UiUtils.dp2px(context, TAB_SIZE * butlerItems.size)) / (butlerItems.size - 1)
        dividerDrawable = GradientDrawable().apply {
            setSize(dividerPadding, 0)
        }
    }

    @SuppressLint("ClickableViewAccessibility", "NewApi")
    private fun addButlerView(butlerItem: ButlerItem) {
        val holder = ViewHolder()

        holder.labelView?.apply {
            if (!TextUtils.isEmpty(butlerItem.text)) {
                toVisible()
                text = butlerItem.text
                if (butlerItem.textColorList != null) {
                    setTextColor(butlerItem.textColorList)
                } else if (butlerItem.textColor != 0) {
                    setTextColor(butlerItem.textColor)
                } else {
                    setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.lets_widget_core_color_text_primary
                        )
                    )
                }
            } else {
                toGone()
            }
        }

        holder.iconView?.apply {
            if (butlerItem.iconRes != 0) {
                toVisible()
                setImageResource(butlerItem.iconRes)
                currentImageSize = if (TextUtils.isEmpty(butlerItem.text)) {
                    UiUtils.dp2px(context, IMAGE_SIZE_L)
                } else {
                    UiUtils.dp2px(context, IMAGE_SIZE_S)
                }
                layoutParams = layoutParams?.apply {
                    width = currentImageSize
                    height = currentImageSize
                }
            } else {
                toGone()
            }
        }

        holder.msgCount?.apply {
            viewTreeObserver.addOnGlobalLayoutListener {
                if (isVisible && layoutParams is RelativeLayout.LayoutParams) {
                    val msgLayoutParams = layoutParams as RelativeLayout.LayoutParams
                    val calcLeftMargin = currentImageSize - width / 2
                    if (calcLeftMargin != msgLayoutParams.leftMargin) {
                        layoutParams = msgLayoutParams.apply {
                            leftMargin = calcLeftMargin
                        }
                    }
                }
            }
        }

        holder.itemView?.apply {
            val itemViewSize = UiUtils.dp2px(context, TAB_SIZE)
            this.layoutParams = RelativeLayout.LayoutParams(itemViewSize, itemViewSize)
            setOnClickListener {
                val position = this@LetsBaseAppButler.indexOfChild(this)
                selectButler(position)
                onButlerListener?.onButlerClicked(butlerItem, position)
            }
            val gestureDetector = GestureDetector(context, ItemViewGestureListener(butlerItem, this))
            setOnTouchListener { _, event -> gestureDetector.onTouchEvent(event) }
        }

        viewHolders.add(holder)
        addView(holder.itemView)
    }

    private fun selectView(position: Int, ignoreCurrentPosition: Boolean) {
        val holder = viewHolders[position]
        val tabItem = butlerItems[position]
        if (selectedHolder != holder) {
            val prePosition = indexOfChild(selectedHolder?.itemView)
            var preTabItem: ButlerItem? = null
            if (prePosition != -1) {
                preTabItem = butlerItems[prePosition]
            }

            playIconAnim(tabItem, preTabItem, holder, ignoreCurrentPosition)

            selectedHolder?.setSelect(false)
            holder.setSelect(true)
            selectedHolder = holder
        }
    }

    private fun playIconAnim(
        tabItem: ButlerItem,
        preTabItem: ButlerItem?,
        holder: ViewHolder,
        ignoreCurrentPosition: Boolean
    ) {
        if (selectedHolder?.iconView?.isAnimating == true) {
            selectedHolder?.iconView?.cancelAnimation()
        }
        preTabItem?.iconRes?.let { selectedHolder?.iconView?.setImageResource(it) }

        var hasAnim = false
        if (tabItem.animRawId > 0) {
            holder.iconView?.setAnimation(tabItem.animRawId)
            hasAnim = true
        } else if (!TextUtils.isEmpty(tabItem.animAssets)) {
            holder.iconView?.setAnimation(tabItem.animAssets)
            hasAnim = true
        }
        if (hasAnim) {
            holder.iconView?.playAnimation()
            if (ignoreCurrentPosition) {
                holder.iconView?.progress = 1f
            }
        }
    }

    private fun setEnabled(itemView: View, enable: Boolean) {
        val labelView = itemView.findViewById<TextView>(R.id.labelView)
        val iconView = itemView.findViewById<ImageView>(R.id.iconView)
        itemView.isEnabled = enable
        labelView.isEnabled = enable
        iconView.isEnabled = enable
    }

    @SuppressLint("NewApi")
    private fun setMsgCount(itemView: View, msgCount: Int) {
        val msgView = itemView.findViewById<TextView>(R.id.msgCount)
        if (msgCount <= 0) {
            if (msgView.isVisible) {
                if (animMsgAlpha == null) {
                    animMsgAlpha = ObjectAnimator.ofFloat(msgView, "alpha", 1f, 0.8f, 0f).apply {
                        duration = MSG_DOT_ANIM_DURATION
                        addListener(
                        object : Animator.AnimatorListener {
                            override fun onAnimationStart(animation: Animator) {
                            }

                            override fun onAnimationEnd(animation: Animator) {
                                msgView.alpha = 1f
                                msgView.toGone()
                            }

                            override fun onAnimationCancel(animation: Animator) {
                            }

                            override fun onAnimationRepeat(animation: Animator) {
                            }
                        })
                    }
                }
                animMsgAlpha?.start()
            }
        } else {
            msgView.toVisible()
            msgView.text = formatMsgText(msgCount)
        }
    }

    private fun formatMsgText(msgCount: Int): String {
        return if (msgCount > maxMsgCountLimit) {
            "$maxMsgCountLimit+"
        } else {
            msgCount.toString()
        }
    }

    @SuppressLint("NewApi")
    override fun onDetachedFromWindow() {
        animMsgAlpha?.removeAllListeners()
        animMsgAlpha?.cancel()
        viewPager?.removeOnPageChangeListener(onPageChangeListener)
        viewPager2?.unregisterOnPageChangeCallback(onPageChangeCallback)
        super.onDetachedFromWindow()
    }

    inner class ItemViewGestureListener(private val butlerItem: ButlerItem, val itemView: View?) :
        GestureDetector.SimpleOnGestureListener() {
        override fun onDoubleTap(e: MotionEvent): Boolean {
            val position = this@LetsBaseAppButler.indexOfChild(itemView)
            onDoubleClickListener?.onDoubleClick(butlerItem, position)
            return super.onDoubleTap(e)
        }
    }

    override fun onSaveInstanceState(): Parcelable? {
        return Bundle().apply {
            val superData = super.onSaveInstanceState()
            putParcelable("super_data", superData)
            putInt("currentPosition", currentPosition)
        }
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        val bundle = state as? Bundle ?: return super.onRestoreInstanceState(state)
        val superData: Parcelable? = bundle.getParcelable("super_data")
        super.onRestoreInstanceState(superData)
        currentPosition = bundle.getInt("currentPosition")
        post {
            updateButlerView()
            selectButler(position = currentPosition, needInterrupt = false, ignoreCurrentPosition = true)
        }
    }


    data class ButlerItem(
        val iconRes: Int = 0,
        val text: String? = null,
        val textColor: Int = 0,
        val textColorList: ColorStateList? = null,
        val animAssets: String? = null,
        val animRawId: Int = 0
    )

    interface OnButlerListener {
        fun onButlerClicked(butlerItem: ButlerItem, position: Int)
        fun onButlerSelected(butlerItem: ButlerItem, position: Int)
    }

    interface OnInterruptSelectListener {
        fun onInterrupt(butlerItem: ButlerItem, position: Int): Boolean
    }

    interface OnDoubleClickListener {
        fun onDoubleClick(butlerItem: ButlerItem, position: Int)
    }

    inner class ViewHolder {
        var itemView: View? = null
        var labelView: TextView? = null
        var iconView: LottieAnimationView? = null
        var msgCount: TextView? = null

        init {
            itemView = LayoutInflater.from(context).inflate(R.layout.lets_widget_core_layout_base_butler, null)
            labelView = itemView?.findViewById(R.id.labelView)
            iconView = itemView?.findViewById(R.id.iconView)
            msgCount = itemView?.findViewById(R.id.msgCount)
        }

        fun setSelect(isSelected: Boolean) {
            itemView?.isSelected = isSelected
            labelView?.isSelected = isSelected
            iconView?.isSelected = isSelected
            msgCount?.isSelected = isSelected
        }
    }

}