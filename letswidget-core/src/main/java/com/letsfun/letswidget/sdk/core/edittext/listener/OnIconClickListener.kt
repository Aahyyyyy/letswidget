package com.letsfun.letswidget.sdk.core.edittext.listener

interface OnIconClickListener {
    fun onIconClicked()
}