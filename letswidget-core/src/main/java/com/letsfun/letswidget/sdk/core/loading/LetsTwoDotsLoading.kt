package com.letsfun.letswidget.sdk.core.loading

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import com.letsfun.letswidget.sdk.core.R

@SuppressLint("NewApi")
class LetsTwoDotsLoading : View {
    companion object {
        private const val MIN_SHOW_TIME = 500
        private const val MIN_DELAY = 500
    }

    private val defaultDot = LetsTwoDots()
    private var startTime: Long = -1
    private var postedHide = false
    private var postedShow = false
    private var dismissed = false

    private val delayedHide = Runnable {
        postedHide = false
        startTime = -1
        visibility = GONE
    }
    private val delayedShow = Runnable {
        postedShow = false
        if (!dismissed) {
            startTime = System.currentTimeMillis()
            visibility = VISIBLE
        }
    }
    var minWidth = 0
    var maxWidth = 0
    var minHeight = 0
    var maxHeight = 0
    private var mDot: LetsBaseDot? = null
    private var dotColor = 0
    private var shouldStartAnimationDrawable = false

    constructor(context: Context) : super(context) {
        init(context, null, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs, 0, R.style.lets_widget_core_blue_loading)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs, defStyleAttr, R.style.lets_widget_core_blue_loading)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs, defStyleAttr, R.style.lets_widget_core_blue_loading)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) {
        minWidth = 24
        maxWidth = 48
        minHeight = 24
        maxHeight = 48
        val a = context.obtainStyledAttributes(
                attrs, R.styleable.LetsTwoDotsLoading, defStyleAttr, defStyleRes)
        minWidth = a.getDimensionPixelSize(R.styleable.LetsTwoDotsLoading_lets_loading_minWidth, minWidth)
        maxWidth = a.getDimensionPixelSize(R.styleable.LetsTwoDotsLoading_lets_loading_maxWidth, maxWidth)
        minHeight = a.getDimensionPixelSize(R.styleable.LetsTwoDotsLoading_lets_loading_minHeight, minHeight)
        maxHeight = a.getDimensionPixelSize(R.styleable.LetsTwoDotsLoading_lets_loading_maxHeight, maxHeight)
        val dotName = a.getString(R.styleable.LetsTwoDotsLoading_lets_loading_dotName)
        dotColor = a.getColor(R.styleable.LetsTwoDotsLoading_lets_loading_dotColor, Color.WHITE)
        setDot(dotName)
        if (mDot == null) {
            dot = defaultDot
        }
        a.recycle()
    }

    //need to set indicator color again if you didn't specified when you update the indicator .
    var dot: LetsBaseDot?
        get() = mDot
        set(d) {
            if (mDot !== d) {
                if (mDot != null) {
                    mDot!!.callback = null
                    unscheduleDrawable(mDot)
                }
                mDot = d
                //need to set indicator color again if you didn't specified when you update the indicator .
                setDotColor(dotColor)
                if (d != null) {
                    d.callback = this
                }
                postInvalidate()
            }
        }

    /**
     * setIndicatorColor(0xFF00FF00)
     * or
     * setIndicatorColor(Color.BLUE)
     * or
     * setIndicatorColor(Color.parseColor("#FF4081"))
     * or
     * setIndicatorColor(0xFF00FF00)
     * or
     * setIndicatorColor(getResources().getColor(android.R.color.black))
     *
     * @param color
     */
    fun setDotColor(color: Int) {
        dotColor = color
        mDot!!.color = color
    }

    /**
     * You should pay attention to pass this parameter with two way:
     * for example:
     * 1. Only class Name,like "SimpleIndicator".(This way would use default package name with
     * "com.wang.avi.indicators")
     * 2. Class name with full package,like "com.my.android.indicators.SimpleIndicator".
     *
     * @param dotName the class must be extend Indicator .
     */
    fun setDot(dotName: String?) {
        if (TextUtils.isEmpty(dotName)) {
            return
        }
        val drawableClassName = StringBuilder()
        if (!dotName!!.contains(".")) {
            val defaultPackageName = javaClass.getPackage().name
            drawableClassName.append(defaultPackageName)
                    .append(".indicators")
                    .append(".")
        }
        drawableClassName.append(dotName)
        try {
            val drawableClass = Class.forName(drawableClassName.toString())
            var dot = drawableClass.newInstance() as LetsBaseDot
            dot = dot
        } catch (e: ClassNotFoundException) {
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }
    }

    fun smoothToShow() {
        startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in))
        visibility = VISIBLE
    }

    fun smoothToHide() {
        startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_out))
        visibility = GONE
    }

    fun hide() {
        dismissed = true
        removeCallbacks(delayedShow)
        val diff = System.currentTimeMillis() - startTime
        if (diff >= MIN_SHOW_TIME || startTime == -1L) {
            visibility = GONE
        } else {
            if (!postedHide) {
                postDelayed(delayedHide, MIN_SHOW_TIME - diff)
                postedHide = true
            }
        }
    }

    fun show() {
        startTime = -1
        dismissed = false
        removeCallbacks(delayedHide)
        if (!postedShow) {
            postDelayed(delayedShow, MIN_DELAY.toLong())
            postedShow = true
        }
    }

    override fun verifyDrawable(who: Drawable): Boolean {
        return (who === mDot || super.verifyDrawable(who))
    }

    fun startAnimation() {
        if (visibility != VISIBLE) {
            return
        }
        if (mDot is Animatable) {
            shouldStartAnimationDrawable = true
        }
        postInvalidate()
    }

    fun stopAnimation() {
        if (mDot is Animatable) {
            mDot!!.stop()
            shouldStartAnimationDrawable = false
        }
        postInvalidate()
    }

    override fun setVisibility(v: Int) {
        if (visibility != v) {
            super.setVisibility(v)
            if (v == GONE || v == INVISIBLE) {
                stopAnimation()
            } else {
                startAnimation()
            }
        }
    }

    override fun invalidateDrawable(dr: Drawable) {
        if (verifyDrawable(dr)) {
            val dirty = dr.bounds
            val scrollX = scrollX + paddingLeft
            val scrollY = scrollY + paddingTop
            invalidate(dirty.left + scrollX, dirty.top + scrollY,
                    dirty.right + scrollX, dirty.bottom + scrollY)
        } else {
            super.invalidateDrawable(dr)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        updateDrawableBounds(w, h)
    }

    private fun updateDrawableBounds(w: Int, h: Int) {
        var w = w
        var h = h
        w -= paddingRight + paddingLeft
        h -= paddingTop + paddingBottom
        var right = w
        var bottom = h
        var top = 0
        var left = 0
        if (mDot != null) {
            // Maintain aspect ratio. Certain kinds of animated drawables
            // get very confused otherwise.
            val intrinsicWidth = mDot!!.intrinsicWidth
            val intrinsicHeight = mDot!!.intrinsicHeight
            val intrinsicAspect = intrinsicWidth.toFloat() / intrinsicHeight
            val boundAspect = w.toFloat() / h
            if (java.lang.Float.compare(intrinsicAspect, boundAspect) != 0) {
                if (boundAspect > intrinsicAspect) {
                    // New width is larger. Make it smaller to match height.
                    val width = (h * intrinsicAspect).toInt()
                    left = (w - width) / 2
                    right = left + width
                } else {
                    // New height is larger. Make it smaller to match width.
                    val height = (w * (1 / intrinsicAspect)).toInt()
                    top = (h - height) / 2
                    bottom = top + height
                }
            }
            mDot!!.setBounds(left, top, right, bottom)
        }
    }

    @Synchronized
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawTrack(canvas)
    }

    fun drawTrack(canvas: Canvas) {
        val d: Drawable? = mDot
        if (d != null) {
            // Translate canvas so a indeterminate circular progress bar with padding
            // rotates properly in its animation
            val saveCount = canvas.save()
            canvas.translate(paddingLeft.toFloat(), paddingTop.toFloat())
            d.draw(canvas)
            canvas.restoreToCount(saveCount)
            if (shouldStartAnimationDrawable) {
                (d as Animatable).start()
                shouldStartAnimationDrawable = false
            }
        }
    }

    @Synchronized
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var dw = 0
        var dh = 0
        val d: Drawable? = mDot
        if (d != null) {
            dw = Math.max(minWidth, Math.min(maxWidth, d.intrinsicWidth))
            dh = Math.max(minHeight, Math.min(maxHeight, d.intrinsicHeight))
        }
        updateDrawableState()
        dw += paddingLeft + paddingRight
        dh += paddingTop + paddingBottom
        val measuredWidth = resolveSizeAndState(dw, widthMeasureSpec, 0)
        val measuredHeight = resolveSizeAndState(dh, heightMeasureSpec, 0)
        setMeasuredDimension(measuredWidth, measuredHeight)
    }

    override fun drawableStateChanged() {
        super.drawableStateChanged()
        updateDrawableState()
    }

    private fun updateDrawableState() {
        val state = drawableState
        if (mDot != null && mDot!!.isStateful) {
            mDot!!.state = state
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun drawableHotspotChanged(x: Float, y: Float) {
        super.drawableHotspotChanged(x, y)
        if (mDot != null) {
            mDot!!.setHotspot(x, y)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        startAnimation()
        removeCallbacks()
    }

    override fun onDetachedFromWindow() {
        stopAnimation()
        super.onDetachedFromWindow()
        removeCallbacks()
    }

    private fun removeCallbacks() {
        removeCallbacks(delayedHide)
        removeCallbacks(delayedShow)
    }
}