package com.letsfun.letswidget.sdk.core.butler.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.INDICATOR_ANIMATION_MODE_ELASTIC
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.tablayout.LetsTabLayout
import com.letsfun.letswidget.sdk.core.toast.LetsToast
import com.letsfun.letswidget.sdk.core.utils.UiUtils

class LetsFriendButler(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    val tabLayout: LetsTabLayout

    init {
        val v = LayoutInflater.from(context).inflate(R.layout.lets_widget_core_layout_custom_friend_butler, this)

        tabLayout = v.findViewById(R.id.tabLayout)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val measureWidth = MeasureSpec.getSize(widthMeasureSpec)
        val measureHeight = UiUtils.dp2px(context, 50f)

        setMeasuredDimension(measureWidth, measureHeight)
    }
}