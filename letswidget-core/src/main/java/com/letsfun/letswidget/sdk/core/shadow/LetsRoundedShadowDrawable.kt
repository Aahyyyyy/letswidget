package com.letsfun.letswidget.sdk.core.shadow

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils

class LetsRoundedShadowDrawable(
    val context: Context,
    val shadowToken: Int,
    val needShadow: Boolean = true
) : Drawable() {
    private var cr = RectF()

    var radius = UiUtils.dp2px(context, 25f).toFloat()
    var shadowDx = UiUtils.dp2px(context, 0f).toFloat()
    var shadowDy = UiUtils.dp2px(context, 1f).toFloat()
    var shadowRadius = UiUtils.dp2px(context, 4f).toFloat()
    var shadowColor = Color.parseColor("#1A040B29")
    var backgroundColor = context.getColor(R.color.lets_widget_core_color_bg_primary)

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.WHITE
    }

    private val shadowPaint: Paint by lazy {
        val p = Paint(Paint.ANTI_ALIAS_FLAG)
        p.style = Paint.Style.FILL
        p.color = shadowColor
        p.isAntiAlias = true
        p.setShadowLayer(shadowRadius, shadowDx, shadowDy, shadowColor)
        p
    }

    init {
        when (shadowToken) {
            SHADOW_BASE -> {
                shadowDx = UiUtils.dp2px(context,0f).toFloat()
                shadowDy = UiUtils.dp2px(context,1f).toFloat()
                shadowRadius = UiUtils.dp2px(context,4f).toFloat()
                shadowColor = Color.parseColor("#1A040B29")
            }
            SHADOW_OVERLAY -> {
                shadowDx = UiUtils.dp2px(context,0f).toFloat()
                shadowDy = UiUtils.dp2px(context,4f).toFloat()
                shadowRadius = UiUtils.dp2px(context,16f).toFloat()
                shadowColor = Color.parseColor("#0D040B29")
            }
            SHADOW_MAP -> {
                shadowDx = UiUtils.dp2px(context,0f).toFloat()
                shadowDy = UiUtils.dp2px(context,1f).toFloat()
                shadowRadius = UiUtils.dp2px(context,4f).toFloat()
                shadowColor = Color.parseColor("#0A040B29")
            }
        }

//        radius = UiUtils.dp2px(context,bgRadius).toFloat()
    }

    override fun onBoundsChange(bounds: Rect?) {
        super.onBoundsChange(bounds)
        bounds ?: return
        cr = RectF(bounds)
    }

    override fun draw(canvas: Canvas) {
        if (needShadow) {
            canvas.drawRoundRect(cr, radius, radius, shadowPaint)
        }
        paint.style = Paint.Style.FILL
        paint.color = backgroundColor
        canvas.drawRoundRect(cr, radius, radius, paint)
    }

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
        if (needShadow) {
            shadowPaint.alpha = (alpha * 0.07).toInt()
        }
        invalidateSelf()
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.colorFilter = colorFilter
        if (needShadow) {
            shadowPaint.colorFilter = colorFilter
        }
        invalidateSelf()
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    companion object {
        const val SHADOW_BASE = 0
        const val SHADOW_OVERLAY = 1
        const val SHADOW_MAP = 2
    }
}