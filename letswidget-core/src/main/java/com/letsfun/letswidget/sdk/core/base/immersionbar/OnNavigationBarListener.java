package com.letsfun.letswidget.sdk.core.base.immersionbar;

public interface OnNavigationBarListener {
    void onNavigationBarChange(boolean show);
}
