package com.letsfun.letswidget.sdk.core.edittext.icon.interfaces

import com.letsfun.letswidget.sdk.core.edittext.listener.OnIconClickListener

interface IDoubleIconDrawable : IIconDrawable {
    fun setIcon(primaryIconResId: Int, secondaryIconResId: Int): Boolean

    fun setOnPrimaryIconClickListener(onIconClickListener: OnIconClickListener?)

    fun setOnSecondaryIconClickListener(onIconClickListener: OnIconClickListener?)
}