package com.letsfun.letswidget.sdk.core.base.immersionbar;

public interface OnBarListener {
    /**
     * On bar info change.
     *
     * @param barProperties the bar info
     */
    void onBarChange(BarProperties barProperties);
}
