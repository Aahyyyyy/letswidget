package com.letsfun.letswidget.sdk.core.badge.interfaces

import androidx.annotation.ColorInt
import androidx.annotation.IntRange

interface IBadgeNumberDotView {
    fun setDotNumber(@IntRange(from = 0) number: Int)

    fun setNumberDotBgColor(@ColorInt colorResId: Int)
}