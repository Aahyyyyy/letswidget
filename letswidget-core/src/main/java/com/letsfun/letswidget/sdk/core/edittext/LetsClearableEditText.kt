package com.letsfun.letswidget.sdk.core.edittext

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.edittext.listener.OnIconClickListener

open class LetsClearableEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.editTextStyle
) : LetsEditText(context, attrs, defStyleAttr) {

    private var clearable: Boolean = false

    private var secondaryIcon: Int = INVALID_RES_ID

    private val onLeftIconClickListener: OnIconClickListener by lazy {
        object : OnIconClickListener {
            override fun onIconClicked() {
                if (onTextClearListener?.onInterruptClear() != true) {
                    setText("")
                    onTextClearListener?.onCleared()
                }
            }
        }
    }

    private var onTextClearListener: OnTextClearListener? = null

    private val textWatcher: TextWatcher by lazy {
        object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                postInvalidateView()
            }
        }
    }

    init {
        addTextChangedListener(textWatcher)
        setOnTailPrimaryIconClickListener(onLeftIconClickListener)

        postInvalidateView()
    }

    open fun setSecondaryIcon(icon: Int) {
        secondaryIcon = icon
        postInvalidateView()
    }

    fun setOnSecondaryIconClickListener(listener: OnIconClickListener) {
        setOnTailSecondaryIconClickListener(listener)
    }

    fun setOnTextClearListener(listener: OnTextClearListener) {
        this.onTextClearListener = listener
    }

    override fun invalidateView() {
        super.invalidateView()

        clearable = !(text?.trim().isNullOrEmpty())
        if (clearable) {
            setTailIcon(R.drawable.lets_widget_core_icon_close_solid_16, secondaryIcon)
        } else {
            setTailIcon(INVALID_RES_ID, secondaryIcon)
        }
    }

    interface OnTextClearListener {
        fun onInterruptClear(): Boolean = false
        fun onCleared()
    }
}