package com.letsfun.letswidget.sdk.core.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IntDef
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.lib.core.AppContext
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.ImageUtils
import com.letsfun.letswidget.sdk.core.utils.UiUtils

@SuppressLint("NewApi")
class LetsAvatar(context: Context, attrs: AttributeSet? = null) : AppCompatImageView(context, attrs) {
    companion object {
        val paramsCache = SparseArray<AvatarParams>()
    }

    private var avatarSize: Int = AvatarSize.SIZE_40
    private var userType: Int = UserType.TYPE_NONE
    private var showBorder: Boolean = false

    private var imagePaint: Paint
    private var mMatrix: Matrix

    private var borderPaint: Paint

    private var labelPaint: Paint
    private var labelMatrix: Matrix
    private var defaultLabelDrawable: Drawable? = null
    private var customLabelDrawable: Drawable? = null

    private var params: AvatarParams

    private var imgDrawable: Drawable? = null

    private var isViewInvalidating: Boolean = false

    init {
        if (attrs != null) {
            context.obtainStyledAttributes(attrs, R.styleable.LetsAvatar).apply {
                showBorder = getBoolean(R.styleable.LetsAvatar_lets_avatar_show_border, false)
                avatarSize = getInteger(R.styleable.LetsAvatar_lets_avatar_size, AvatarSize.SIZE_40)
                userType =
                    getInteger(R.styleable.LetsAvatar_lets_avatar_user_type, UserType.TYPE_NONE)
                recycle()
            }
        }

        scaleType = ScaleType.CENTER_CROP

        params = getParams()
        mMatrix = Matrix()
        imagePaint = Paint().apply {
            isAntiAlias = true
            setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }

        labelMatrix = Matrix()
        labelPaint = Paint().apply {
            isAntiAlias = true
            setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        borderPaint = Paint().apply {
            style = Paint.Style.STROKE
            isAntiAlias = true
            color = ContextCompat.getColor(context, R.color.lets_widget_core_color_border_on_bg)
        }

        postInvalidateView()
    }

    fun setAvatarSize(@AvatarSize avatarSize: Int) {
        if (this.avatarSize != avatarSize) {
            this.avatarSize = avatarSize

            params = getParams()

            postInvalidateView()
        }
    }

    fun setUserType(@UserType userType: Int) {
        if (this.userType != userType) {
            this.userType = userType
            onUpdateUserType()
            postInvalidateView()
        }
    }

    fun setShowBorder(showBorder: Boolean) {
        if (this.showBorder != showBorder) {
            this.showBorder = showBorder
            postInvalidateView()
        }
    }

    private fun postInvalidateView() {
        if (!isViewInvalidating) {
            isViewInvalidating = true
            post {
                isViewInvalidating = false
                invalidateView()
            }
        }
    }

    @SuppressLint("NewApi")
    private fun onUpdateUserType() {
        if (params.pxLabelSize <= 0) {
            return
        }
        when (userType) {
            UserType.TYPE_USER -> {
                defaultLabelDrawable = BitmapDrawable(
                    resources, ImageUtils.scaleToSquare(
                        ImageUtils.getBitmapFromDrawable(
                            context,
                            R.drawable.lets_widget_core_icon_user
                        ),
                        params.pxLabelSize.toInt(),
                        params.pxLabelSize.toInt()
                    )
                )
            }
            UserType.TYPE_USER_VIP -> {
                defaultLabelDrawable = BitmapDrawable(
                    resources, ImageUtils.scaleToSquare(
                        ImageUtils.getBitmapFromDrawable(
                            context,
                            R.drawable.lets_widget_core_icon_user_vip
                        ),
                        params.pxLabelSize.toInt(),
                        params.pxLabelSize.toInt()
                    )
                )
            }
        }
    }

    private fun invalidateView() {
        // 更新边框属性
        borderPaint.strokeWidth = params.pxBoarderWidth

        // 更新layout尺寸
        if (layoutParams != null) {
            layoutParams = layoutParams.apply {
                width = params.pxSize.toInt()
                height = params.pxSize.toInt()
            }
        } else {
            layoutParams = ViewGroup.LayoutParams(params.pxSize.toInt(), params.pxSize.toInt())
        }

        postInvalidate()
    }

    private fun getParams(): AvatarParams {
        var param = paramsCache.get(avatarSize)
        if (param != null) {
            return param
        }
        param = when (avatarSize) {
            AvatarSize.SIZE_15 -> AvatarParams(15f, 0.5f, 0f)
            AvatarSize.SIZE_25 -> AvatarParams(25f, 1f, 12f)
            AvatarSize.SIZE_40 -> AvatarParams(40f, 1.5f, 16f)
            AvatarSize.SIZE_55 -> AvatarParams(55f, 2f, 16f)
            AvatarSize.SIZE_80 -> AvatarParams(80f, 3f, 20f)
            else -> AvatarParams(40f, 1.5f, 16f)
        }.apply {
            pxSize = UiUtils.dp2px(AppContext.getApp(), size).toFloat()
            pxLabelSize = UiUtils.dp2px(AppContext.getApp(), labelSize).toFloat()
            pxBoarderWidth = UiUtils.dp2px(AppContext.getApp(), boarderWidth).toFloat()
        }
        paramsCache.put(avatarSize, param)
        return param
    }

    fun setLabel(resId: Int) {
        this.customLabelDrawable = ContextCompat.getDrawable(context, resId)
        postInvalidateView()
    }

    fun setLabel(drawable: Drawable) {
        this.customLabelDrawable = drawable
        postInvalidateView()
    }

    fun setLabel(bitmap: Bitmap) {
        this.customLabelDrawable = BitmapDrawable(resources, bitmap)
        postInvalidateView()
    }

    override fun setImageResource(resId: Int) {
        this.imgDrawable = ContextCompat.getDrawable(context, resId)
        postInvalidateView()
    }

    override fun setImageDrawable(drawable: Drawable?) {
        this.imgDrawable = drawable
        postInvalidateView()
    }

    override fun setImageBitmap(bm: Bitmap?) {
        this.imgDrawable = BitmapDrawable(resources, bm)
        postInvalidateView()
    }

    private fun drawImage(canvas: Canvas?) {
        val borderWidth = if (showBorder) {
            params.pxBoarderWidth
        } else {
            0f
        }
        imgDrawable?.let {
            val imgSize = params.pxSize - borderWidth * 2
            val bitmap = ImageUtils.centerCropToSquare(
                ImageUtils.scaleToSquare(
                    ImageUtils.getBitmapFromDrawable(it, imgSize.toInt(), imgSize.toInt()), imgSize.toInt(), imgSize.toInt()
                )
            )

            val bitmapShader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            mMatrix.reset()
            mMatrix.postTranslate(borderWidth, borderWidth)
            bitmapShader.setLocalMatrix(mMatrix)
            imagePaint.shader = bitmapShader
            canvas?.drawCircle(params.pxSize / 2, params.pxSize / 2, imgSize / 2, imagePaint)
        }
    }

    private fun drawBorder(canvas: Canvas?) {
        if (!showBorder) {
            return
        }
        val radius = params.pxSize / 2 - params.pxBoarderWidth
        canvas?.drawCircle(params.pxSize / 2, params.pxSize / 2, radius, borderPaint)
    }

    private fun drawLabel(canvas: Canvas?) {
        if (params.labelSize <= 0) {
            return
        }
        var labelBitmap: Bitmap? = null
        when (userType) {
            UserType.TYPE_CUSTOM -> {
                customLabelDrawable?.let {
                    labelBitmap = ImageUtils.scaleToSquare(
                        ImageUtils.getBitmapFromDrawable(it, params.pxLabelSize.toInt(), params.pxLabelSize.toInt()),
                        params.pxLabelSize.toInt(),
                        params.pxLabelSize.toInt()
                    )
                }
            }
            else -> {
                defaultLabelDrawable?.let {
                    labelBitmap = ImageUtils.scaleToSquare(
                        ImageUtils.getBitmapFromDrawable(it, params.pxLabelSize.toInt(), params.pxLabelSize.toInt()),
                        params.pxLabelSize.toInt(),
                        params.pxLabelSize.toInt()
                    )
                }
            }
        }
        labelBitmap?.let {
            val center = params.pxSize - params.pxLabelSize / 2
            labelPaint.shader =
                BitmapShader(it, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP).apply {
                    labelMatrix.reset()
                    labelMatrix.postTranslate(
                        params.pxSize - params.pxLabelSize,
                        params.pxSize - params.pxLabelSize
                    )
                    setLocalMatrix(labelMatrix)
                }
            canvas?.drawCircle(center, center, it.width / 2f, labelPaint)
        }
    }

    override fun onDraw(canvas: Canvas?) {
        drawImage(canvas)
        drawBorder(canvas)
        drawLabel(canvas)
    }

    @IntDef(
        UserType.TYPE_NONE,
        UserType.TYPE_CUSTOM,
        UserType.TYPE_USER,
        UserType.TYPE_USER_VIP
    )
    @Target(
        AnnotationTarget.TYPE_PARAMETER,
        AnnotationTarget.VALUE_PARAMETER,
        AnnotationTarget.FIELD,
        AnnotationTarget.FUNCTION
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class UserType {
        companion object {
            const val TYPE_NONE = 0
            const val TYPE_CUSTOM = 1
            const val TYPE_USER = 2
            const val TYPE_USER_VIP = 3
        }
    }

    /**
     * 尺寸样式
     */
    @IntDef(
        AvatarSize.SIZE_15,
        AvatarSize.SIZE_25,
        AvatarSize.SIZE_40,
        AvatarSize.SIZE_55,
        AvatarSize.SIZE_80
    )
    @Target(
        AnnotationTarget.TYPE_PARAMETER,
        AnnotationTarget.VALUE_PARAMETER,
        AnnotationTarget.FIELD,
        AnnotationTarget.FUNCTION
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class AvatarSize {
        companion object {
            const val SIZE_15 = 0
            const val SIZE_25 = 1
            const val SIZE_40 = 2
            const val SIZE_55 = 3
            const val SIZE_80 = 4
        }
    }

    class AvatarParams(
        val size: Float,
        val boarderWidth: Float,
        val labelSize: Float
    ) {
        var pxSize: Float = 0f
        var pxBoarderWidth: Float = 0f
        var pxLabelSize: Float = 0f
    }
}