package com.letsfun.letswidget.sdk.core.utils

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import androidx.fragment.app.FragmentActivity

object ActivityUtils {
    fun scanForActivity(context: Context?): Activity? {
        if (context == null) return null
        if (context is Activity) {
            return context
        } else if (context is ContextWrapper) {
            return scanForActivity(context.baseContext)
        }
        return null
    }

    fun findFragmentActivity(context: Context?):FragmentActivity?{
        if (context == null) return null
        if (context is FragmentActivity) {
            return context
        } else if (context is ContextWrapper) {
            return findFragmentActivity(context.baseContext)
        }
        return null
    }
}