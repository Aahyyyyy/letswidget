package com.letsfun.letswidget.sdk.core.edittext

import android.annotation.SuppressLint
import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.edittext.base.LetsBaseEditText

@SuppressLint("NewApi")
open class LetsEditText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.editTextStyle
) : LetsBaseEditText(context, attrs, defStyleAttr) {
    init {
        maxLines = 1
        inputType = InputType.TYPE_CLASS_TEXT
    }
}