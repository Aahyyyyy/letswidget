package com.letsfun.letswidget.sdk.core.utils

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.VectorDrawable
import android.net.Uri
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.bumptech.glide.load.ImageHeaderParser.ImageType
import com.bumptech.glide.load.resource.bitmap.DefaultImageHeaderParser
import com.letsfun.letswidget.lib.core.utils.IoUtils
import java.io.FileInputStream

@SuppressLint("NewApi")
object ImageUtils {
    fun getBitmapFromDrawable(context: Context, @DrawableRes drawableId: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context, drawableId)
        return if (drawable is BitmapDrawable) {
            drawable.bitmap
        } else if (drawable is VectorDrawable || drawable is VectorDrawableCompat) {
            val bitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth,
                drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            bitmap
        } else {
            throw IllegalArgumentException("unsupported drawable type")
        }
    }

    fun getBitmapFromDrawable(drawable: Drawable, viewWidth: Int, viewHeight: Int): Bitmap {
        return when (drawable) {
            is BitmapDrawable -> {
                drawable.bitmap
            }
            is VectorDrawable, is VectorDrawableCompat -> {
                val bitmap = Bitmap.createBitmap(
                    drawable.intrinsicWidth,
                    drawable.intrinsicHeight,
                    Bitmap.Config.ARGB_8888
                )
                val canvas = Canvas(bitmap)
                drawable.setBounds(0, 0, canvas.width, canvas.height)
                drawable.draw(canvas)
                bitmap
            }
            else -> {
                val bitmap = if (drawable.intrinsicWidth > 0 && drawable.intrinsicHeight > 0) {
                    Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
                } else {
                    Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888)
                }
                val canvas = Canvas(bitmap)
                drawable.setBounds(0, 0, canvas.width, canvas.height)
                drawable.draw(canvas)
                bitmap
            }
        }
    }

    /**
     * 缩放至指定尺寸正方形(centerCrop)
     */
    fun scaleToSquare(bitmap: Bitmap, width: Int, height: Int): Bitmap {
        return if ((bitmap.width.toFloat() / bitmap.height) != (width.toFloat() / height)) {
            Bitmap.createScaledBitmap(centerCropToSquare(bitmap), width, height, false)
        } else {
            Bitmap.createScaledBitmap(bitmap, width, height, false)
        }
    }

    /**
     * 根据原图尺寸比例裁剪图片为正方形
     */
    fun centerCropToSquare(bitmap: Bitmap): Bitmap {
        var startX = 0
        var startY = 0
        return if (bitmap.width > bitmap.height) {
            startX = (bitmap.width - bitmap.height) / 2
            Bitmap.createBitmap(bitmap, startX, startY, bitmap.height, bitmap.height)
        } else if (bitmap.width < bitmap.height) {
            startY = (bitmap.height - bitmap.width) / 2
            Bitmap.createBitmap(bitmap, startX, startY, bitmap.width, bitmap.width)
        } else {
            bitmap
        }
    }

    fun getUriFromDrawable(context: Context, @DrawableRes drawableId: Int): Uri {
        return Uri.parse(
            ContentResolver.SCHEME_ANDROID_RESOURCE +
                    "://" + context.resources.getResourcePackageName(drawableId)
                    + '/' + context.resources.getResourceTypeName(drawableId)
                    + '/' + context.resources.getResourceEntryName(drawableId)
        )
    }

    /**
     * IO操作，需要在IO线程调用
     */
    fun getFileExtension(filePath: String): String? {
        val fileInputStream = FileInputStream(filePath)
        val fileExt = when (DefaultImageHeaderParser().getType(fileInputStream)) {
            ImageType.GIF -> "gif"
            ImageType.JPEG -> "jpg"
            ImageType.PNG, ImageType.PNG_A -> "png"
            else -> null
        }
        IoUtils.closeQuietly(fileInputStream)
        return fileExt
    }

}