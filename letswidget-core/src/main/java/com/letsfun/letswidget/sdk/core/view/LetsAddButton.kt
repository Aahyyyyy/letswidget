package com.letsfun.letswidget.sdk.core.view

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.widget.Button
import android.widget.LinearLayout
import com.letsfun.letswidget.sdk.core.R

class LetsAddButton(context: Context, attrs: AttributeSet? = null) : LinearLayout(context, attrs) {

    companion object {
        const val STYLE_PRIMARY = 0
        const val STYLE_SECONDARY = 1
    }

    private var btnAdd: Button

    var text: String? = null
        set(value) {
            field = value
            btnAdd.text = field
        }

    var btnStyle: Int = STYLE_PRIMARY
        set(value) {
            field = value
            updateBtnStyle()
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.lets_widget_core_layout_add_button, this)
        btnAdd = findViewById(R.id.btn_add)

        if (attrs != null) {
            context.obtainStyledAttributes(attrs, R.styleable.LetsAddButton).apply {
                text = getString(R.styleable.LetsAddButton_lets_add_btn_text)
                btnStyle = getInt(R.styleable.LetsAddButton_lets_add_btn_style, btnStyle)
                isEnabled = getBoolean(R.styleable.LetsAddButton_lets_add_btn_enabled, true)
                recycle()
            }
        }

        gravity = Gravity.CENTER
        isClickable = true
        isLongClickable = true
    }

    private fun updateBtnStyle(){
        if (btnStyle == STYLE_PRIMARY){
            setBackgroundResource(R.drawable.lets_widget_core_shape_btn_primary_add)
        } else {
            setBackgroundResource(R.drawable.lets_widget_core_shape_btn_secondary_add)
        }
    }

    fun getInnerBtn(): Button {
        return btnAdd
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        btnAdd.isEnabled = enabled
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return true
    }

}