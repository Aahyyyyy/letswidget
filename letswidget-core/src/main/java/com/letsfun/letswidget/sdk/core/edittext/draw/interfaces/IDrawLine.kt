package com.letsfun.letswidget.sdk.core.edittext.draw.interfaces

import android.graphics.PointF

interface IDrawLine : IDraw {
    fun setColor(colorId: Int)

    fun setPosition(startPosition: PointF, endPosition: PointF)
}