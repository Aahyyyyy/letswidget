package com.letsfun.letswidget.sdk.core.titlebar

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.MenuRes
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.base.immersionbar.ImmersionBar
import com.letsfun.letswidget.sdk.core.utils.UiUtils

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
class LetsTitleBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    companion object {
        const val LARGE_TITLE_HEIGHT = 82f
        const val LARGE_TITLE_MARGIN_TOP = 5f
        const val MENU_ITEM_MARGIN = 22f
        const val TITLE_DRAWABLE_PADDING = 5f

        const val MODE_LOCK_COLLAPSED = 0
        const val MODE_SCROLL_COLLAPSED = 1//暂不支持
        const val MODE_LOCK_EXPANDED = 2
        const val MODE_SCROLL_EXPANDED = 3
        const val MODE_IMMERSION_SCROLL_EXPANDED = 4
        const val MODE_COLLAPSED_ALPHA = 5  //锁定，但是alpha会跟随scroll变化

        const val TITLE_STYLE_NORMAL = 0

        // 展示箭头
        const val TITLE_STYLE_SHOW_ARROW = 1
    }

    var ivNavigation: ImageView
    var llMenuContainer: LinearLayout
    var llCustomContainer: LinearLayout
    var clImmersionContainer: ConstraintLayout
    var clTitleContainer: ConstraintLayout
    var tvTitleBottom: TextView
    var tvTitleTop: TextView

    private var progress = 1f //折叠进度, 0为collapsed，1为expanded
    private var position = 0f

    private var strTitle: CharSequence? = ""
    private var menu: Menu = PopupMenu(context, null).menu
    private var onNavigationListener: OnClickListener? = null
    private var mode = MODE_LOCK_EXPANDED

    private var originalPaddingTop = 0
    private var originalPaddingBottom = 0
    private var originalLayoutHeight = 0

    private var titleStyle = TITLE_STYLE_NORMAL
    private var obtainCustomView = false

    var immersionBar: ImmersionBar? = null
    var darkFont: Boolean = false
    var scrollHeight: Int = 200
    //zeya.zhang   2022-09-27
    var allowScrollChangeMenuTint = true

    var onMenuItemClickListener: MenuItem.OnMenuItemClickListener? = null

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        val customContainer = findViewById<LinearLayout>(R.id.llCustomContainer)
        if (customContainer != null) {
            customContainer.addView(child, params)
        } else {
            super.addView(child, index, params)
        }
    }

    fun setTitle(@StringRes resId: Int) {
        setTitle(context.getString(resId))
    }

    fun setTitle(title: CharSequence?) {
        strTitle = title
        setTitleImpl(strTitle)
        requestLayout()
    }

    fun getTitleView(): TextView {
        return if (mode == MODE_LOCK_COLLAPSED || mode == MODE_SCROLL_COLLAPSED) {
            tvTitleTop
        } else {
            tvTitleBottom
        }
    }

    fun setNavigationOnClickListener(listener: OnClickListener?) {
        onNavigationListener = listener
    }

    fun setMenuRes(@MenuRes resId: Int) {
        if (resId != 0) {
            MenuInflater(context).inflate(resId, menu)
            for (index in 0 until menu.size()) {
                addMenuItemImpl(menu.getItem(index))
            }
        } else {
            clearMenu()
        }
    }

    fun addMenuItem(@DrawableRes resId: Int, onClickListener: OnClickListener): ImageView {
        val imageView = AppCompatImageView(context)
        imageView.setImageResource(resId)
        imageView.setOnClickListener(onClickListener)
        LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        ).apply {
            marginStart = UiUtils.dp2px(context, MENU_ITEM_MARGIN)
            gravity = Gravity.CENTER_VERTICAL
            llMenuContainer.addView(imageView, this)
        }
        return imageView
    }

    fun addMenuItem(content: String, onClickListener: OnClickListener): TextView {
        val textView = inflateTextItem(content)
        textView.setOnClickListener(onClickListener)
        LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        ).apply {
            gravity = Gravity.CENTER_VERTICAL
            marginStart = UiUtils.dp2px(context, MENU_ITEM_MARGIN)
            llMenuContainer.addView(textView, this)
        }
        return textView
    }

    fun clearMenu() {
        menu.clear()
        llMenuContainer.removeAllViews()
    }

    /**
     * MODE_LOCK_COLLAPSED - 折叠不可滚动
     * MODE_SCROLL_COLLAPSED - 折叠可滚动
     * MODE_LOCK_EXPANDED - 展开不可滚动
     * MODE_SCROLL_EXPANDED - 展开可滚动
     * scrollView - Deprecated，使用MotionLayout实现
     */
    fun setMode(value: Int, scrollView: View? = null) {
        mode = value
        setTitleImpl(strTitle)
        setModeImpl(mode)
        requestLayout()
    }

    fun setScrollEnable(enabled: Boolean) {
        if (mode == MODE_SCROLL_EXPANDED || mode == MODE_IMMERSION_SCROLL_EXPANDED) {
            (parent as? MotionLayout?)?.apply {
                getTransition(R.id.lets_transition).setEnable(enabled)
            }
        }
    }

    fun setTitleStyle(titleStyle: Int) {
        this.titleStyle = titleStyle

        var drawableRight: Drawable? = null
        when (titleStyle) {
            TITLE_STYLE_SHOW_ARROW -> {
                drawableRight = ResourcesCompat.getDrawable(
                    context.resources,
                    R.drawable.lets_widget_core_icon_arrows_down_2xs,
                    null
                )
            }
        }
        tvTitleTop.compoundDrawablePadding = UiUtils.dp2px(context, TITLE_DRAWABLE_PADDING)
        tvTitleTop.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, drawableRight, null)
    }

    fun addLargeTitleIcon(@DrawableRes iconRes: Int) {
        if (iconRes == 0 || iconRes == View.NO_ID) {
            return
        }
        val drawableRight = ResourcesCompat.getDrawable(
            context.resources,
            iconRes,
            null
        )
        addLargeTitleIcon(drawableRight)
    }

    private fun getXmlHeight(attrs: AttributeSet? = null): Int {
        val layoutHeight = attrs?.getAttributeValue(
            "http://schemas.android.com/apk/res/android",
            "layout_height"
        )
        var height = 0
        when {
            layoutHeight.equals(ViewGroup.LayoutParams.MATCH_PARENT.toString()) ->
                height = ViewGroup.LayoutParams.MATCH_PARENT
            layoutHeight.equals(ViewGroup.LayoutParams.WRAP_CONTENT.toString()) ->
                height = ViewGroup.LayoutParams.WRAP_CONTENT
            else -> context.obtainStyledAttributes(attrs, intArrayOf(android.R.attr.layout_height))
                .apply {
                    height = getDimensionPixelSize(0, ViewGroup.LayoutParams.WRAP_CONTENT)
                    recycle()
                }
        }

        return height
    }

    private fun setTitleImpl(title: CharSequence?) {
        tvTitleTop.text = title
        tvTitleBottom.text = title
    }

    private fun setModeImpl(value: Int) {
        when (value) {
            MODE_LOCK_COLLAPSED -> {
                tvTitleTop.visibility = VISIBLE
                tvTitleBottom.visibility = GONE
            }
            MODE_LOCK_EXPANDED -> {
                tvTitleTop.visibility = GONE
                tvTitleBottom.visibility = VISIBLE
            }
            MODE_SCROLL_COLLAPSED -> {
                tvTitleTop.visibility = VISIBLE
                tvTitleBottom.visibility = GONE
            }
            MODE_SCROLL_EXPANDED, MODE_IMMERSION_SCROLL_EXPANDED -> {
                tvTitleTop.visibility = VISIBLE
                tvTitleBottom.visibility = VISIBLE
            }
        }
    }

    private fun addMenuItemImpl(item: MenuItem) {
        var itemView: View? = null
        if (item.icon != null) {
            val imageView = AppCompatImageView(context)
            imageView.setImageDrawable(item.icon)
            itemView = imageView
        } else if (item.title != null) {
            itemView = inflateTextItem(item.title.toString())
        }
        if (itemView != null) {
            itemView.setOnClickListener {
                onMenuItemClickListener?.onMenuItemClick(item)
            }
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                gravity = Gravity.CENTER_VERTICAL
                marginStart = UiUtils.dp2px(context, MENU_ITEM_MARGIN)
                llMenuContainer.addView(itemView, this)
            }
        }
    }

    private fun inflateTextItem(content: String): TextView {
        AppCompatTextView(context).apply {
            text = content
            includeFontPadding = false
            setTextSize(
                TypedValue.COMPLEX_UNIT_PX,
                context.resources.getDimension(R.dimen.lets_widget_core_text_size_content)
            )
            setTextColor(
                ContextCompat.getColorStateList(
                    context,
                    R.color.lets_widget_core_color_button_outline_text
                )
            )
            return this
        }
    }

    private fun addLargeTitleIcon(drawable: Drawable?){
        drawable?.let {
            tvTitleBottom.compoundDrawablePadding = UiUtils.dp2px(
                context,
                LetsTitleBar.TITLE_DRAWABLE_PADDING
            )
            tvTitleBottom.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,
                null,
                it,
                null
            )
        }
    }

    init {
        View.inflate(context, R.layout.lets_widget_core_layout_title_bar, this)
        var menuRes = 0
        var largeTitleDrawable: Drawable? = null
        if (attrs != null) {
            val attrArray = context.obtainStyledAttributes(attrs, R.styleable.LetsTitleBar)

            strTitle = attrArray.getString(R.styleable.LetsTitleBar_android_title)
            menuRes = attrArray.getResourceId(R.styleable.LetsTitleBar_tb_menu, 0)
            mode = attrArray.getInteger(R.styleable.LetsTitleBar_tb_mode, mode)
            titleStyle = attrArray.getInteger(R.styleable.LetsTitleBar_tb_title_style, TITLE_STYLE_NORMAL)
            largeTitleDrawable = attrArray.getDrawable(R.styleable.LetsTitleBar_tb_large_title_icon)

            attrArray.recycle()
            originalLayoutHeight = getXmlHeight(attrs)
        }
        ivNavigation = findViewById(R.id.ivNavigation)
        llMenuContainer = findViewById(R.id.llMenuContainer)
        llCustomContainer = findViewById(R.id.llCustomContainer)
        clImmersionContainer = findViewById(R.id.clImmersionContainer)
        clTitleContainer = findViewById(R.id.clTitleContainer)
        tvTitleBottom = findViewById(R.id.tvTitle)
        tvTitleTop = findViewById(R.id.tvTitleTop)

        setTitleImpl(strTitle)
        setModeImpl(mode)
        setMenuRes(menuRes)
        setTitleStyle(titleStyle)
        addLargeTitleIcon(largeTitleDrawable)
        ivNavigation.setOnClickListener {
            onNavigationListener?.onClick(it)
        }

        originalPaddingBottom = paddingBottom
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        //首次测量强制使用WRAP_CONTENT模式，需要得到控件的原始高度
        super.onMeasure(
            widthMeasureSpec,
            MeasureSpec.makeMeasureSpec(UiUtils.getScreenHeight(context), MeasureSpec.AT_MOST)
        )
    }



}