package com.letsfun.letswidget.sdk.core.edittext.icon

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.edittext.base.LetsBaseEditText.Companion.INVALID_RES_ID
import com.letsfun.letswidget.sdk.core.edittext.icon.interfaces.IDoubleIconDrawable
import com.letsfun.letswidget.sdk.core.edittext.listener.OnIconClickListener

@SuppressLint("NewApi")
class TailDrawableImpl(val view: TextView) : IDoubleIconDrawable {

    private var primaryIconResId: Int = INVALID_RES_ID
    private var secondaryIconResId: Int = INVALID_RES_ID

    private var drawable: Drawable? = null

    private var onPrimaryIconClickListener: OnIconClickListener? = null
    private var onSecondaryIconClickListener: OnIconClickListener? = null

    private var hasPrimaryActionDown: Boolean = false
    private var hasSecondaryActionDown: Boolean = false
    private var isPrimaryActive: Boolean = false
    private var isSecondaryActive: Boolean = false

    override fun setIcon(primaryIconResId: Int, secondaryIconResId: Int): Boolean {
        if (this.primaryIconResId != primaryIconResId || this.secondaryIconResId != secondaryIconResId) {
            this.primaryIconResId = primaryIconResId
            this.secondaryIconResId = secondaryIconResId
            drawable = getLayoutDrawableByResIds(primaryIconResId, secondaryIconResId)
            return true
        }
        return false
    }

    override fun getDrawable(): Drawable? {
        return drawable
    }

    override fun setOnPrimaryIconClickListener(onIconClickListener: OnIconClickListener?) {
        this.onPrimaryIconClickListener = onIconClickListener
    }

    override fun setOnSecondaryIconClickListener(onIconClickListener: OnIconClickListener?) {
        this.onSecondaryIconClickListener = onIconClickListener
    }

    override fun dealWithTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (isInsidePrimaryIcon(event)) {
                    hasPrimaryActionDown = true
                    return true
                } else if (isInsideSecondaryIcon(event)) {
                    hasSecondaryActionDown = true
                    return true
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if (hasPrimaryActionDown && !isInsidePrimaryIcon(event)) {
                    hasPrimaryActionDown = false
                } else if (hasSecondaryActionDown && !isInsideSecondaryIcon(event)) {
                    hasSecondaryActionDown = false
                }
            }
            MotionEvent.ACTION_UP -> {
                if (isInsidePrimaryIcon(event) && hasPrimaryActionDown) {
                    onPrimaryIconClickListener?.onIconClicked()
                    return false
                } else if (isInsideSecondaryIcon(event) && hasSecondaryActionDown) {
                    onSecondaryIconClickListener?.onIconClicked()
                    return false
                }
            }
        }
        return false
    }

    override fun setAlpha(alpha: Float) {
        drawable?.mutate()?.alpha = (alpha * 255).toInt()
    }

    private fun isInsidePrimaryIcon(event: MotionEvent): Boolean {
        if (!isPrimaryActive) {
            return false
        }

        return if (isSecondaryActive) {
            (primaryIconResId != INVALID_RES_ID && event.x > view.width - view.paddingRight - (drawable?.intrinsicWidth
                ?: 0) && event.x < view.width - view.paddingRight - (drawable?.intrinsicWidth
                ?: 0) / 2)
        } else {
            (primaryIconResId != INVALID_RES_ID && event.x > view.width - view.paddingRight - (drawable?.intrinsicWidth
                ?: 0) && event.x < view.width - view.paddingRight)
        }
    }

    private fun isInsideSecondaryIcon(event: MotionEvent): Boolean {
        if (!isSecondaryActive) {
            return false
        }
        return if (isSecondaryActive) {
            event.x > view.width - view.paddingRight - (drawable?.intrinsicWidth ?: 0) / 2
                    && event.x < view.width - view.paddingRight
        } else {
            event.x > view.width - view.paddingRight - (drawable?.intrinsicWidth ?: 0)
                    && event.x < view.width - view.paddingRight
        }
    }

    private fun getLayoutDrawableByResIds(resIdLeft: Int, resIdRight: Int): Drawable? {
        if (resIdLeft == INVALID_RES_ID && resIdRight == INVALID_RES_ID) {
            return null
        }
        LayoutInflater.from(view.context)
            .inflate(R.layout.lets_widget_core_layout_edittext_tail_icon, LinearLayout(view.context))
            .apply {
                findViewById<ImageView>(R.id.icon1).apply {
                    visibility = if (resIdLeft != INVALID_RES_ID) {
                        setImageResource(resIdLeft)
                        isPrimaryActive = true
                        View.VISIBLE
                    } else {
                        isPrimaryActive = false
                        View.GONE
                    }
                }
                findViewById<ImageView>(R.id.icon2).apply {
                    visibility = if (resIdRight != INVALID_RES_ID) {
                        setImageResource(resIdRight)
                        isSecondaryActive = true
                        View.VISIBLE
                    } else {
                        isSecondaryActive = false
                        View.GONE
                    }
                }

                isDrawingCacheEnabled = true
                measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                )
                layout(0, 0, measuredWidth, measuredHeight)
                buildDrawingCache()
                val cacheBitmap: Bitmap = drawingCache
                val bitmap = Bitmap.createBitmap(cacheBitmap)

                return BitmapDrawable(resources, bitmap).apply {
                    setBounds(0, 0, intrinsicWidth, intrinsicHeight)
                }
            }
    }
}