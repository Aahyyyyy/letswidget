package com.letsfun.letswidget.sdk.core.edittext.icon

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.sdk.core.edittext.base.LetsBaseEditText.Companion.INVALID_RES_ID
import com.letsfun.letswidget.sdk.core.edittext.icon.interfaces.ISingleIconDrawable
import com.letsfun.letswidget.sdk.core.edittext.listener.OnIconClickListener
import com.letsfun.letswidget.sdk.core.utils.UiUtils
import java.lang.Exception

@SuppressLint("NewApi")
abstract class BaseDrawableImpl(private val view: TextView) : ISingleIconDrawable {

    private var iconResId: Int = INVALID_RES_ID
    private var drawable: Drawable? = null

    private var onIconClickListener: OnIconClickListener? = null

    private var hasActionDown: Boolean = false
    private var isActive: Boolean = false

    override fun setIcon(iconResId: Int): Boolean {
        if (this.iconResId != iconResId) {
            this.iconResId = iconResId
            try {
                drawable = createDrawable()
                isActive = true
            } catch (e: Exception) {
                drawable = null
                isActive = false
                e.printStackTrace()
            }

            return true
        }
        return false
    }

    private fun createDrawable(): Drawable? {
        return ContextCompat.getDrawable(view.context, iconResId)?.apply {
            val dp25 = UiUtils.dp2px(view.context, 25f)
            val layoutWith = if (intrinsicWidth > intrinsicHeight) {
                dp25
            } else {
                dp25 * intrinsicWidth / intrinsicHeight
            }
            val layoutHeight = if (intrinsicHeight >= intrinsicWidth) {
                dp25
            } else {
                dp25 * intrinsicHeight / intrinsicWidth
            }
            setBounds(0, 0, layoutWith, layoutHeight)
        }
    }

    override fun getDrawable(): Drawable? {
        return drawable
    }

    override fun dealWithTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (isInsideIcon(event)) {
                    hasActionDown = true
                    return true
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if (hasActionDown && !isInsideIcon(event)) {
                    hasActionDown = false
                }
            }
            MotionEvent.ACTION_UP -> {
                if (isInsideIcon(event) && hasActionDown) {
                    onIconClickListener?.onIconClicked()
                    return true
                }
            }
        }
        return false
    }

    override fun setAlpha(alpha: Float) {
        drawable?.mutate()?.alpha = (alpha * 255).toInt()
    }

    override fun setOnIconClickListener(onIconClickListener: OnIconClickListener?) {
        this.onIconClickListener = onIconClickListener
    }

    private fun isInsideIcon(event: MotionEvent): Boolean {
        if (!isActive) {
            return false
        }
        return event.x > view.paddingLeft
                && event.x < view.paddingLeft + (drawable?.intrinsicWidth ?: 0)
    }
}