package com.letsfun.letswidget.sdk.core.loading

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.letsfun.letswidget.sdk.core.R

/**
 * 默认不能点击关闭，不响应back键
 */
class LetsPageLoading(private val containerView: ViewGroup) {
    private var whiteBackground: FrameLayout? = null
    private var innerDialogLg: View? = null
    var isShowing = false
    var cancellable = false
    var onDismissListener: (() -> Unit)? = null

    @SuppressLint("NewApi")
    fun show() {
        if (!isShowing) {
            whiteBackground = FrameLayout(containerView.context)
            whiteBackground?.id = View.generateViewId()
            whiteBackground?.setBackgroundColor(containerView.context.getColor(R.color.lets_widget_core_color_bg_primary))
            containerView.addView(
                whiteBackground,
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            )
            innerDialogLg = LayoutInflater.from(containerView.context)
                .inflate(R.layout.lets_widget_core_layout_dialog_loading, whiteBackground, false)
            whiteBackground?.addView(
                innerDialogLg,
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            )
            whiteBackground?.setOnClickListener {
                if (cancellable) {
                    dismiss()
                }
            }
            isShowing = true
        }
    }

    fun dismiss() {
        if (isShowing) {
            containerView.removeView(whiteBackground)
            isShowing = false
            whiteBackground = null
            innerDialogLg = null
            onDismissListener?.invoke()
        }
    }
}