package com.letsfun.letswidget.sdk.core.edittext

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils
import com.letsfun.letswidget.sdk.core.utils.applyStyle

@SuppressLint("NewApi")
class LetsSearchEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    companion object {
        const val TYPE_S = 0
        const val TYPE_M = 1
    }

    private val clEdit: ConstraintLayout
    private val etInput: EditText
    private val ivClear: ImageView
    private val tvAction: TextView

    /**
     * 搜索框尺寸
     */
    var searchSize: Int = TYPE_M
        set(value) {
            field = value
            updateView()
        }

    /**
     * 输入框提示文案
     */
    var editHint: String? = null
        set(value) {
            field = value
            etInput.hint = field
        }
        get() {
            return etInput.hint.toString()
        }

    /**
     * 输入框文案
     */
    var editContent: String? = null
        set(value) {
            field = value
            etInput.setText(field)
        }
        get() {
            return etInput.text.toString()
        }

    var inputType: Int = -1
        set(value) {
            field = value
            if (field != -1) {
                etInput.inputType = field
            }
        }

    var imeOptions: Int = -1
        set(value) {
            field = value
            if (field != -1) {
                etInput.imeOptions = field
            }
        }

    /**
     * 是否显示操作按钮
     */
    var hideSearchIcon: Boolean = false
        set(value) {
            field = value
            updateView()
        }
    /**
     * 是否显示清空文案按钮
     */
    var showClear: Boolean = true
        set(value) {
            field = value
            updateView()
        }

    var clearIcon: Drawable? = null
        set(value) {
            field = value
            if (clearIcon != null){
                ivClear.setImageDrawable(field)
            }
        }

    /**
     * 是否显示操作按钮
     */
    var showAction: Boolean = false
        set(value) {
            field = value
            tvAction.isVisible = field
        }

    /**
     * 操作按钮文案
     */
    var actionText: String? = null
        set(value) {
            field = value
            tvAction.text = field
        }

    var actionIcon: Drawable? = null
        set(value) {
            field = value
            tvAction.setCompoundDrawablesWithIntrinsicBounds(field, null, null, null)
        }

    init {
        View.inflate(context, R.layout.lets_widget_core_layout_edittext_search, this)
        clEdit = findViewById(R.id.cl_edit)
        etInput = findViewById(R.id.et_input)
        ivClear = findViewById(R.id.iv_clear)
        tvAction = findViewById(R.id.tv_action)
        attrs?.let {
            context.obtainStyledAttributes(attrs, R.styleable.LetsSearchEditText).apply {
                searchSize = getInt(R.styleable.LetsSearchEditText_lets_search_edit_size, TYPE_M)
                showClear = getBoolean(R.styleable.LetsSearchEditText_lets_search_show_clear, showClear)
                showAction = getBoolean(R.styleable.LetsSearchEditText_lets_search_show_action, showAction)
                actionText = getString(R.styleable.LetsSearchEditText_lets_search_action_text)
                actionIcon = getDrawable(R.styleable.LetsSearchEditText_lets_search_action_icon)
                editHint = getString(R.styleable.LetsSearchEditText_lets_search_edit_hint)
                inputType = getInt(R.styleable.LetsSearchEditText_android_inputType, inputType)
                imeOptions = getInt(R.styleable.LetsSearchEditText_android_imeOptions, imeOptions)
                getResourceId(R.styleable.LetsSearchEditText_lets_search_edit_style, -1).let {
                    if (it != -1){
                        etInput.applyStyle(it)
                    }
                }
                getDrawable(R.styleable.LetsSearchEditText_android_background).let {
                    if (it != null){
                        background = it
                    } else {
                        setBackgroundColor(context.getColor(R.color.lets_widget_core_color_bg_primary))
                    }
                }
                getDrawable(R.styleable.LetsSearchEditText_lets_search_edit_bg).let {
                    if (it != null){
                        clEdit.background = it
                    }
                }
                recycle()
            }
        }
        updateView()
    }

    private fun updateView(){
        val verticalPadding: Int
        val leftDrawable: Drawable?
        if (searchSize == TYPE_M){
            verticalPadding = UiUtils.dp2px(context, 10f)
            clEdit.layoutParams.height = UiUtils.dp2px(context, 40f)
            resources.getDimension(R.dimen.lets_widget_core_font_size_8).let {
                etInput.setTextSize(TypedValue.COMPLEX_UNIT_PX, it)
                tvAction.setTextSize(TypedValue.COMPLEX_UNIT_PX, it)
            }
            leftDrawable = ContextCompat.getDrawable(context, R.drawable.lets_widget_core_icon_search_s)
            if (clearIcon == null){
                ivClear.setImageResource(R.drawable.lets_widget_core_icon_close_filled_s)
            }
        } else {
            verticalPadding = UiUtils.dp2px(context, 5f)
            clEdit.layoutParams.height = UiUtils.dp2px(context, 32f)
            resources.getDimension(R.dimen.lets_widget_core_font_size_9).let {
                etInput.setTextSize(TypedValue.COMPLEX_UNIT_PX, it)
                tvAction.setTextSize(TypedValue.COMPLEX_UNIT_PX, it)
            }
            resources.getColorStateList(R.color.lets_widget_core_color_text_tertiary, context.theme)
                .let {
                    leftDrawable = ContextCompat.getDrawable(
                        context,
                        R.drawable.lets_widget_core_icon_search_2xs
                    )?.apply {
                        DrawableCompat.setTintList(this, it)
                    }

                    if (clearIcon == null){
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.lets_widget_core_icon_close_filled_2xs
                        )?.apply {
                            DrawableCompat.setTintList(this, it)
                            ivClear.setImageDrawable(this)
                        }
                    }
                }
        }
        val horizontalPadding: Int = resources.getDimensionPixelSize(R.dimen.lets_widget_core_page_margin_normal)
        setPadding(
            if (paddingStart > 0) paddingStart else horizontalPadding,
            verticalPadding,
            if (paddingEnd > 0) paddingEnd else horizontalPadding,
            verticalPadding
        )

        etInput.setCompoundDrawablesWithIntrinsicBounds(
            if (hideSearchIcon) null else leftDrawable,
            null,
            null,
            null
        )

        etInput.doAfterTextChanged {
            if (showClear && etInput.isFocused){
                ivClear.isVisible = !it.isNullOrEmpty()
            } else {
                ivClear.isVisible = false
            }
        }

        etInput.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus && showClear){
                ivClear.isVisible = !etInput.text.isNullOrEmpty()
            } else {
                ivClear.isVisible = false
            }
        }

        ivClear.setOnClickListener {
            etInput.setText("")
        }
    }

    fun setActionClickListener(listener: OnClickListener?){
        tvAction.setOnClickListener(listener)
    }

    fun getEditText(): EditText {
        return etInput
    }

    fun getActionTv(): TextView {
        return tvAction
    }

    fun getClearIv(): ImageView {
        return ivClear;
    }

    fun setEditBgColor(@ColorInt color: Int){
        clEdit.setBackgroundColor(color)
    }

    fun setEditTextListener(textWatcher: TextWatcher) {
        etInput.addTextChangedListener(textWatcher)
    }
}