package com.letsfun.letswidget.sdk.core.badge.impl

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.badge.interfaces.IBadgeNumberDotView
import com.letsfun.letswidget.sdk.core.utils.UiUtils

class BadgeNumberDotView(context: Context, attrs: AttributeSet? = null) : AppCompatTextView(context, attrs), IBadgeNumberDotView {

    companion object {
        const val NUMBER_DOT_SIZE = 14f
        const val DOT_HORIZONTAL_PADDING = 3.5f

        const val MAX_NUMBER_COUNT = 99
    }

    private var dotNumber: Int = 0

    init {
        gravity = Gravity.CENTER

        setBackgroundResource(R.drawable.lets_widget_core_shape_badge_bg_14)
        setTextColor(ContextCompat.getColor(getContext(), R.color.lets_widget_core_color_text_on_bg))
        setTextSize(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.lets_widget_core_font_size_12)
        )

        val padding = UiUtils.dp2px(getContext(), DOT_HORIZONTAL_PADDING)
        setPadding(padding, 0, padding, 0)
        val size = UiUtils.dp2px(getContext(), NUMBER_DOT_SIZE)
        height = size
        minWidth = size

        text = dotNumber.toString()


    }

    override fun setDotNumber(number: Int) {
        dotNumber = number
        text = if (dotNumber <= MAX_NUMBER_COUNT) {
            number.toString()
        } else {
            "${MAX_NUMBER_COUNT}+"
        }
    }

    override fun setNumberDotBgColor(colorResId: Int) {
        (background as GradientDrawable).setColor(colorResId)
    }
}