package com.letsfun.letswidget.sdk.core.utils

import android.animation.AnimatorInflater
import android.animation.StateListAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.widget.TextView
import androidx.annotation.StyleRes
import androidx.core.view.updatePadding
import com.letsfun.letswidget.sdk.core.R

/**
 * Utility class that contains the data from parsing a TextAppearance style resource.
 */
@SuppressLint("NewApi")
internal class LetsTextAppearance(
    context: Context,
    @StyleRes id: Int
) {
    private val layoutWidth: Int
    private val layoutHeight: Int
    var hasLetterSpacing = false
    var letterSpacing = 0f
    val background: Drawable?

    val minHeight: Int
    val minWidth: Int
    val paddingTop: Int
    val paddingStart: Int
    val paddingEnd: Int
    val paddingBottom: Int
    val stateListAnimator: StateListAnimator?
    val gravity: Int
    val singleline: Boolean
    var textColor: ColorStateList? = null

    fun updateMeasureState(textView: TextView, isOverridePadding: Boolean = true) {
        if (hasLetterSpacing) textView.letterSpacing = letterSpacing
        background?.let { textView.background = it }

        if (minWidth != 0) textView.minWidth = minWidth
        if (minHeight != 0) textView.minHeight = minHeight
        textColor?.let {
            textView.setTextColor(it)
        }

        if (isOverridePadding) {
            textView.updatePadding(
                left = if (paddingStart == -1) textView.paddingStart else paddingStart,
                right = if (paddingEnd == -1) textView.paddingEnd else paddingEnd,
                top = if (paddingTop == -1) textView.paddingTop else paddingTop,
                bottom = if (paddingBottom == -1) textView.paddingBottom else paddingBottom
            )
        }

        stateListAnimator?.let { textView.stateListAnimator = it }
        gravity.takeIf { it != -1 }?.let { textView.gravity = it }
        val layoutParams = textView.layoutParams
        layoutParams?.apply {
            if (layoutWidth != -3) width = layoutWidth
            if (layoutHeight != -3) height = layoutHeight
        }
        textView.isSingleLine = singleline
    }

    companion object {
        private const val TAG = "TextAppearance"
    }

    /** Parses the given TextAppearance style resource.  */
    init {
        val a = context.obtainStyledAttributes(id, R.styleable.LetsTextAppearance)

        layoutWidth = a.getLayoutDimension(R.styleable.LetsTextAppearance_android_layout_width, -3)
        layoutHeight = a.getLayoutDimension(R.styleable.LetsTextAppearance_android_layout_height, -3)
        val textColorRes = a.getResourceId(R.styleable.LetsTextAppearance_android_textColor, -1)
        if (textColorRes != -1) textColor = context.resources.getColorStateList(textColorRes, context.theme)
        singleline = a.getBoolean(R.styleable.LetsTextAppearance_android_singleLine, false)
        minHeight = a.getDimensionPixelSize(R.styleable.LetsTextAppearance_android_minHeight, 0)
        minWidth = a.getDimensionPixelSize(R.styleable.LetsTextAppearance_android_minWidth, 0)
        paddingTop = a.getDimensionPixelSize(R.styleable.LetsTextAppearance_android_paddingTop, -1)
        paddingStart =
            a.getDimensionPixelSize(R.styleable.LetsTextAppearance_android_paddingStart, -1)
        paddingEnd = a.getDimensionPixelSize(R.styleable.LetsTextAppearance_android_paddingEnd, -1)
        paddingBottom =
            a.getDimensionPixelSize(R.styleable.LetsTextAppearance_android_paddingBottom, -1)
        stateListAnimator = try {
            AnimatorInflater.loadStateListAnimator(
                context, a.getResourceId(R.styleable.LetsTextAppearance_android_stateListAnimator, 0)
            )
        } catch (_: Resources.NotFoundException) {
            null
        }
        gravity = a.getInt(R.styleable.LetsTextAppearance_android_gravity, -1)

        hasLetterSpacing = a.hasValue(R.styleable.LetsTextAppearance_android_letterSpacing)
        letterSpacing = a.getFloat(R.styleable.LetsTextAppearance_android_letterSpacing, 0f)
        background = MaterialResources.getDrawable(
            context, a, R.styleable.LetsTextAppearance_android_background
        )

        a.recycle()
    }
}