package com.letsfun.letswidget.sdk.core.badge.impl

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils

class BadgeDotView(context: Context?, attrs: AttributeSet? = null) : View(context, attrs) {

    companion object {
        const val DEFAULT_DOT_SIZE = 8f
    }

    private var size: Int = 0

    private var bgColor: Int = R.drawable.lets_widget_core_shape_badge_dot_bg_8

    init {
        setBackgroundResource(bgColor)
        size = UiUtils.dp2px(getContext(), DEFAULT_DOT_SIZE)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        layoutParams.width = size
        layoutParams.height = size
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

}