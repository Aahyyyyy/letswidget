package com.letsfun.letswidget.sdk.core.ext

import android.annotation.SuppressLint
import android.app.Service
import android.graphics.Rect
import android.text.Editable
import android.text.TextWatcher
import android.view.TouchDelegate
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.letsfun.letswidget.lib.core.AppContext
import com.letsfun.letswidget.lib.core.locale.LetsLocaleHelper
import com.letsfun.letswidget.sdk.core.toast.LetsToast
import com.letsfun.letswidget.sdk.core.utils.UiUtils

/**
 * 处理vp2左右切换时候的灵敏度
 */
fun ViewPager2.reduceDragSensitivity() {
    try {
        val recyclerViewField = ViewPager2::class.java.getDeclaredField("mRecyclerView")
        recyclerViewField.isAccessible = true
        val recyclerView = recyclerViewField.get(this) as RecyclerView

        val touchSlopField = RecyclerView::class.java.getDeclaredField("mTouchSlop")
        touchSlopField.isAccessible = true
        val touchSlop = touchSlopField.get(recyclerView) as Int
        touchSlopField.set(recyclerView, touchSlop * 2)       // "8" was obtained experimentally
    } catch (e: Throwable) {
        e.printStackTrace()
    }
}

@SuppressLint("NewApi")
fun View.showKeyboard() {
    (this.context.getSystemService(Service.INPUT_METHOD_SERVICE) as? InputMethodManager)
        ?.showSoftInput(this, 0)
}

@SuppressLint("NewApi")
fun View.hideKeyboard() {
    (this.context.getSystemService(Service.INPUT_METHOD_SERVICE) as? InputMethodManager)
        ?.hideSoftInputFromWindow(this.windowToken, 0)
}

fun View.toEnable() {
    this.isEnabled = true
}

fun View.toDisable() {
    this.isEnabled = false
}

fun View.toVisible() {
    this.visibility = View.VISIBLE
}

fun View.toGone() {
    this.visibility = View.GONE
}

fun View.toInvisible() {
    this.visibility = View.INVISIBLE
}

fun View.showToast(msg: String) {
    LetsToast.showToast(this.context, msg)
}

fun CheckBox.trySetChecked(s: Boolean?) {
    (s ?: false).takeIf { it != this.isChecked }?.let {
        this.isChecked = it
    }
}

fun TextView.trySetText(s: CharSequence?) {
    (s ?: "").takeIf { !it.toString().equals(this.text.toString()) }?.let {
        this.setText(it)
    }
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

fun AppCompatTextView.setTextFutureExt(text: String) = setTextFuture(
    PrecomputedTextCompat.getTextFuture(
        text, TextViewCompat.getTextMetricsParams(this), null
    )
)

fun AppCompatEditText.setTextFutureExt(text: String) = setText(
    PrecomputedTextCompat.create(text, TextViewCompat.getTextMetricsParams(this))
)

fun Float.dip2px(): Int {
    return UiUtils.dp2px(AppContext.getApp(), this)
}

fun View.scaleHotArea(top: Int, bottom: Int, left: Int, right: Int) {
// 增大checkBox的可点击范围
    val parent = this.parent as View
    parent?.post {
        val bounds = Rect()
        this.isEnabled = true
        this.getHitRect(bounds)
        bounds.top -= top
        bounds.bottom += bottom
        bounds.left -= left
        bounds.right += right
        val touchDelegate = TouchDelegate(bounds, this)
        if (View::class.java.isInstance(this.parent)) {
            (this.parent as View).touchDelegate = touchDelegate
        }
    }
}

fun TextView.setVisibleByCount(count: Long?) {
    (count ?: 0).apply {
        text = LetsLocaleHelper.simpleNumber(this)
        if (this > 0) {
            toVisible()
        } else {
            toInvisible()
        }
    }
}

fun View.clickThrottle(intervalTimeMillis: Long = 300L, block: (View) -> Unit) {
    var lastTimeMs = 0L
    setOnClickListener {
        val currentTimeMillis = System.currentTimeMillis()
        if (currentTimeMillis - lastTimeMs >= intervalTimeMillis) {
            lastTimeMs = currentTimeMillis
            block(it)
        }
    }
}
