package com.letsfun.letswidget.sdk.core.butler

import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2

interface ILetsBaseAppButler {

    /**
     * 设置标签列表
     *
     * @param butlerItems 标签列表
     */
    fun setButlerItems(butlerItems: List<LetsBaseAppButler.ButlerItem>)

    /**
     * 代码设置选择的标签
     *
     * @param position 当前位置
     */
    fun selectButler(position: Int, needInterrupt: Boolean = false, ignoreCurrentPosition: Boolean = false)

    /**
     * 获取当前选中标签的 position
     */
    fun getCurrentPosition(): Int

    /**
     * 设置标签enable属性
     * @param position 当前位置
     * @param enable   标签是否可用
     */
    fun setButlerEnable(position: Int, enable: Boolean)

    /**
     * 设置未读消息展示数量
     *
     * @param position 指定的标签位置
     * @param msgCount 未读消息数量，默认最大值99，超过展示99+
     */
    fun setMessageCount(position: Int, msgCount: Int)

    /**
     * 设置未读消息最大展示数量
     *
     * @param limit 未读消息最大数量，若小于1取值1
     */
    fun setMaxMsgCountLimit(limit: Int)

    /**
     * 设置tab监听，包含点击、选中监听回调
     */
    fun setOnButlerListener(listener: LetsBaseAppButler.OnButlerListener?)

    /**
     * 设置双击回调
     */
    fun setOnDoubleClickListener(listener: LetsBaseAppButler.OnDoubleClickListener?)

    /**
     * 设置tab选择拦截监听
     *
     * ViewPager切换不会触发该监听，需要调用方处理ViewPager相关逻辑
     */
    fun setOnInterruptSelectListener(listener: LetsBaseAppButler.OnInterruptSelectListener)

    /**
     * 设置关联ViewPager，同步页面切换
     */
    fun setupWithViewPager(viewPager: ViewPager?)

    /**
     * 设置关联ViewPager2，同步页面切换
     */
    fun setupWithViewPager(viewPager: ViewPager2?)
}