package com.letsfun.letswidget.sdk.core.base

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentActivity
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.base.immersionbar.BarHide
import com.letsfun.letswidget.sdk.core.base.immersionbar.ImmersionBar
import com.letsfun.letswidget.sdk.core.utils.StringUtils
import com.letsfun.letswidget.sdk.core.utils.SystemProperties

abstract class BaseActivity : AppCompatActivity() {

    private val mainHandler: Handler by lazy { Handler(Looper.getMainLooper()) }

    fun post(r: Runnable) {
        mainHandler.post(r)
    }

    fun postDelay(t: Long, r: Runnable) {
        mainHandler.postDelayed(r, t)
    }

    fun remove(r: Runnable) {
        mainHandler.removeCallbacks(r)
    }

    override fun onDestroy() {
        super.onDestroy()
        mainHandler.removeCallbacksAndMessages(null)
    }

    private lateinit var immersionBar: ImmersionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initImmersionBar()
    }

    protected fun initImmersionBar() {
        immersionBar = getImmersionBar()
        immersionBar.init()
    }

    @SuppressLint("NewApi")
    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {
        if (overrideConfiguration != null) {
            val uiMode = overrideConfiguration.uiMode
            overrideConfiguration.setTo(baseContext.resources.configuration)
            overrideConfiguration.uiMode = uiMode
        }
        super.applyOverrideConfiguration(overrideConfiguration)
    }

    @SuppressLint("NewApi")
    open fun getImmersionBar(): ImmersionBar {
//                .transparentBar()  //透明状态栏，不写默认透明色
        val immersionBar = ImmersionBar.with(this)
            .statusBarDarkFont(true)
            .navigationBarColorInt(Color.WHITE)
            .navigationBarDarkIcon(true)
            .navigationBarEnable(true)
            .navigationBarWithKitkatEnable(true)
            .keyboardMode(window.attributes.softInputMode)
            .keyboardEnable(false)//是keyboardMode生效，不能打开，否则标题栏上方会有空白
            .supportActionBar(false) //支持ActionBar使用
            .fullScreen(false) //有导航栏的情况下，activity全屏显示，也就是activity最下面被导航栏覆盖，不写默认非全屏
            .fitsSystemWindows(false) //解决状态栏和布局重叠问题，任选其一，默认为false，当为true时一定要指定statusBarColor()，不然状态栏为透明色，还有一些重载方法
            .transparentStatusBar()
            .hideBar(BarHide.FLAG_SHOW_BAR) //隐藏状态栏或导航栏或两者，不写默认不隐藏

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        if (toolbar != null) {
            immersionBar.titleBar(toolbar)
        }
        return immersionBar
    }

    override fun setTitle(titleId: Int) {
        title = getString(titleId)
    }

    override fun setTitle(title: CharSequence) {
        val textView = findViewById<TextView>(R.id.toolbar_title)
        if (textView != null) {
            textView.text = StringUtils.concat(title)
        }
        super.setTitle(title)
    }

    protected val isNotchScreen: Boolean
        @SuppressLint("NewApi")
        get() = "1" == SystemProperties.getInstance().get("ro.miui.notch")

    val context: FragmentActivity
        get() = this

    val activity: FragmentActivity
        get() = this
}