package com.letsfun.letswidget.sdk.core.butler

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.butler.custom.*
import com.letsfun.letswidget.sdk.core.ext.toGone
import com.letsfun.letswidget.sdk.core.ext.toVisible
import com.letsfun.letswidget.sdk.core.utils.UiUtils
import java.util.*

@SuppressLint("NewApi")
class LetsAppButler(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {
    companion object {
        const val BUTLER_HEIGHT = 105f
        const val PADDING_VERTICAL = 5f
        const val PADDING_HORIZONTAL = 20f

        const val BASE_BUTLER_INDEX = 1
        const val BASE_MUSIC_BUTLER_INDEX = 2
        const val BASE_COMMUNITY_BUTLER_INDEX = 3
        const val BASE_PLAYER_BUTLER_INDEX = 4
        const val BASE_CHAT_BUTLER_INDEX = 5
    }

    private var butlerItems: List<LetsBaseAppButler.ButlerItem> = LinkedList()

    val letsBaseAppButler = LetsBaseAppButler(context, attrs)

    val letsBaseMusicButler = LetsBaseMusicButler(context, attrs)
    val letsBaseCommunityButler = LetsBaseCommunityButler(context, attrs)
    val letsBasePlayerButler = LetsBasePlayerButler(context, attrs)
    val letsBaseChatButler = LetsBaseChatButler(context, attrs)

    val letsMusicButler = LetsMusicButler(context, attrs)
    val letsCommunityButler = LetsCommunityButler(context, attrs)
    val letsPairButler = LetsPairButler(context, attrs)
    val letsFriendButler = LetsFriendButler(context, attrs)
    val letsMeButler = LetsMeButler(context, attrs)

    private val customInAnimation = AnimationUtils.loadAnimation(context, R.anim.lets_widget_core_butler_custom_in)
    private val customOutAnimation = AnimationUtils.loadAnimation(context, R.anim.lets_widget_core_butler_custom_out)
    private val baseInAnimation = AnimationUtils.loadAnimation(context, R.anim.lets_widget_core_butler_base_in)
    private val baseOutAnimation = AnimationUtils.loadAnimation(context, R.anim.lets_widget_core_butler_base_out)

    init {
        orientation = VERTICAL
        gravity = Gravity.CENTER_HORIZONTAL
        elevation = UiUtils.dp2px(context, 10f).toFloat()
        background = ContextCompat.getDrawable(context, R.drawable.lets_widget_core_shape_gradient_butler_bg)

        initButlers()
        initListeners()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val paddingVertical = UiUtils.dp2px(context, PADDING_VERTICAL)
        val paddingHorizontal = UiUtils.dp2px(context, PADDING_HORIZONTAL)
        val measureWidth = MeasureSpec.getSize(widthMeasureSpec)
        val measureHeight = UiUtils.dp2px(context, BUTLER_HEIGHT)

        setPadding(paddingHorizontal, paddingVertical, paddingHorizontal, 0)
        setMeasuredDimension(measureWidth, measureHeight)
    }

    override fun measureChild(
        child: View?,
        parentWidthMeasureSpec: Int,
        parentHeightMeasureSpec: Int
    ) {
        super.measureChild(child, parentWidthMeasureSpec, parentHeightMeasureSpec)
        val measureWith = MeasureSpec.getSize(parentWidthMeasureSpec)
        val measureHeight = UiUtils.dp2px(context, 50f)

        setMeasuredDimension(measureWith, measureHeight)
    }

    fun setButlerItems(butlerItems: List<LetsBaseAppButler.ButlerItem>) {
        this.butlerItems = butlerItems
        letsBaseAppButler.setButlerItems(butlerItems)
    }

    fun updateButlerView() {
        removeViewAt(0)

        when(letsBaseAppButler.getCurrentPosition()) {
            0 -> addView(letsMusicButler, 0)
            1 -> addView(letsCommunityButler, 0)
            2 -> addView(letsPairButler, 0)
            3 -> addView(letsFriendButler, 0)
            4 -> addView(letsMeButler, 0)
        }

        invalidate()
    }

    fun showBaseCustomButler(index: Int) {
        when(index) {
            0 -> outBaseCustomButler(getChildAt(BASE_BUTLER_INDEX), getChildAt(BASE_MUSIC_BUTLER_INDEX))
            1 -> outBaseCustomButler(getChildAt(BASE_BUTLER_INDEX), getChildAt(BASE_COMMUNITY_BUTLER_INDEX))
            2 -> outBaseCustomButler(getChildAt(BASE_BUTLER_INDEX), getChildAt(BASE_PLAYER_BUTLER_INDEX))
            3 -> outBaseCustomButler(getChildAt(BASE_BUTLER_INDEX), getChildAt(BASE_CHAT_BUTLER_INDEX))
            4 -> getChildAt(BASE_BUTLER_INDEX).toVisible()
        }
    }

    fun getCurrentPositions(): Int = letsBaseAppButler.getCurrentPosition()

    private fun initButlers() {
        addView(letsMusicButler, 0)
        addView(letsBaseAppButler, BASE_BUTLER_INDEX)
        addView(letsBaseMusicButler, BASE_MUSIC_BUTLER_INDEX)
        addView(letsBaseCommunityButler, BASE_COMMUNITY_BUTLER_INDEX)
        addView(letsBasePlayerButler, BASE_PLAYER_BUTLER_INDEX)
        addView(letsBaseChatButler, BASE_CHAT_BUTLER_INDEX)

        getChildAt(0).toVisible()
        getChildAt(BASE_BUTLER_INDEX).toVisible()
        getChildAt(BASE_MUSIC_BUTLER_INDEX).toGone()
        getChildAt(BASE_COMMUNITY_BUTLER_INDEX).toGone()
        getChildAt(BASE_PLAYER_BUTLER_INDEX).toGone()
        getChildAt(BASE_CHAT_BUTLER_INDEX).toGone()
    }

    private fun initListeners() {
        initMusicButlerListener()
        initCommunityButlerListener()
        initPlayerButlerListener()
        initChatButlerListener()
    }

    private fun initMusicButlerListener() {
        letsBaseMusicButler.ivBack.setOnClickListener {
            inBaseCustomButler(getChildAt(BASE_MUSIC_BUTLER_INDEX), getChildAt(BASE_BUTLER_INDEX))
        }
    }

    private fun initCommunityButlerListener() {
        letsBaseCommunityButler.ivBack.setOnClickListener {
            inBaseCustomButler(getChildAt(BASE_COMMUNITY_BUTLER_INDEX), getChildAt(BASE_BUTLER_INDEX))
        }
    }

    private fun initPlayerButlerListener() {
        letsBasePlayerButler.ivBack.setOnClickListener {
            inBaseCustomButler(getChildAt(BASE_PLAYER_BUTLER_INDEX), getChildAt(BASE_BUTLER_INDEX))
        }
    }

    private fun initChatButlerListener() {
        letsBaseChatButler.ivBack.setOnClickListener {
            inBaseCustomButler(getChildAt(BASE_CHAT_BUTLER_INDEX), getChildAt(BASE_BUTLER_INDEX))
        }
    }

    private fun outBaseCustomButler(outView: View, inView: View) {
        customInAnimation.setAnimationListener(object : AnimationListener{
            override fun onAnimationStart(p0: Animation?) {
                inView.toVisible()
            }

            override fun onAnimationEnd(p0: Animation?) {}

            override fun onAnimationRepeat(p0: Animation?) {}
        })

        baseOutAnimation.setAnimationListener(object : AnimationListener{
            override fun onAnimationStart(p0: Animation?) {}

            override fun onAnimationEnd(p0: Animation?) {
                outView.toGone()
                inView.startAnimation(customInAnimation)
            }

            override fun onAnimationRepeat(p0: Animation?) {}
        })

        outView.startAnimation(baseOutAnimation)
    }

    private fun inBaseCustomButler(outView: View, inView: View) {
        baseInAnimation.setAnimationListener(object : AnimationListener{
            override fun onAnimationStart(p0: Animation?) {
                inView.toVisible()
            }

            override fun onAnimationEnd(p0: Animation?) {}

            override fun onAnimationRepeat(p0: Animation?) {}
        })

        customOutAnimation.setAnimationListener(object : AnimationListener{
            override fun onAnimationStart(p0: Animation?) {}

            override fun onAnimationEnd(p0: Animation?) {
                outView.toGone()
                inView.startAnimation(baseInAnimation)
            }

            override fun onAnimationRepeat(p0: Animation?) {}
        })

        outView.startAnimation(customOutAnimation)
    }


}