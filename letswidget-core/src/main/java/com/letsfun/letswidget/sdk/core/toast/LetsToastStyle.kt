package com.letsfun.letswidget.sdk.core.toast

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.annotation.LayoutRes
import com.letsfun.letswidget.lib.core.AppContext
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils

data class LetsToastStyle (
    var marginH: Float = 0f,
    var marginV: Float = 0f,
    var xOffset: Int = 0,
    var yOffset: Int = UiUtils.dp2px(AppContext.getApp(), 140f),
    var gravity: Int = Gravity.BOTTOM,
    var duration: Int = Toast.LENGTH_SHORT,
    // Android 11,自定义view为过时方法,但Android12还是可以用，后续推动设计用google推荐的snackbar
    @LayoutRes var layoutId: Int = -1,
    var view: View? = null
){
    fun getModifyView(): View {
        view = view?:let {
            if (layoutId == -1) {//默认C端的自定义view
                LayoutInflater.from(AppContext.getApp()).inflate(R.layout.lets_widget_core_layout_toast_view, null)
            } else {
                LayoutInflater.from(AppContext.getApp()).inflate(layoutId, null)
            }
        }
        return view!!
    }
}