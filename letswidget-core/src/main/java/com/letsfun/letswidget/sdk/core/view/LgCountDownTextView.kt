package com.letsfun.letswidget.sdk.core.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.CountDownTimer
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.letsfun.letswidget.sdk.core.R

@SuppressLint("NewApi")
class LgCountDownTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = android.R.attr.textViewStyle
) : AppCompatTextView(context, attrs, defStyleAttr) {
    private var expireSeconds: Long = 0
    private var listener: OnCountDownListener? = null
    private var countDownTimer: CountDownTimer? = null

    var textPrefix: String = text.toString()
        set(value) {
            field = value
            text = getFormattedText(false)
        }

    init {
        setTextAppearance(R.style.LgCountDownTextView)
        if (attrs != null) {
            val attrArray = context.obtainStyledAttributes(attrs, R.styleable.LgCountDownTextView)
            attrArray.getString(R.styleable.LgCountDownTextView_lg_cdtv_prefixText)?.apply {
                textPrefix = this
            }
            attrArray.recycle()
        }
    }

    interface OnCountDownListener {
        fun onTick(currentTimeInSeconds: Long)
        fun onFinish()
    }

    /**
     *
     * @param expiredSeconds 超时时间
     * @param listener
     */
    fun startCountDown(expiredSeconds: Long, listener: OnCountDownListener?) {
        isEnabled = false
        this.expireSeconds = expiredSeconds
        this.listener = listener
        if (countDownTimer != null) {
            countDownTimer!!.cancel()
        }
        countDownTimer = object : CountDownTimer(expiredSeconds * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                text = getFormattedText(true, ((millisUntilFinished + 400) / 1000).toInt())
                this@LgCountDownTextView.listener?.onTick(millisUntilFinished / 1000)
            }

            override fun onFinish() {
                isEnabled = true
                text = getFormattedText(false)
                this@LgCountDownTextView.listener?.onFinish()
            }
        }
        countDownTimer?.start()
    }

    fun stopCountDown() {
        if (countDownTimer != null) {
            countDownTimer!!.cancel()
            countDownTimer = null
        }
    }

    override fun setVisibility(visibility: Int) {
        stopCountDown()
        super.setVisibility(visibility)
    }

    override fun onDetachedFromWindow() {
        stopCountDown()
        super.onDetachedFromWindow()
    }

    private fun getFormattedText(isCounting: Boolean, seconds: Int = 0): String {
        return if (isCounting && seconds > 0) String.format(TEXT_FORMAT_COUNTING, textPrefix, seconds)
        else String.format(TEXT_FORMAT_NORMAL, textPrefix)
    }

    companion object {
        const val TEXT_FORMAT_NORMAL = "%s"
        const val TEXT_FORMAT_COUNTING = "%s(%ds)"
    }
}