package com.letsfun.letswidget.sdk.core.edittext.listener

interface OnVerifyCodeListener {
    /**
     * 文本变化
     */
    fun onTextChanged(code: String)

    /**
     * 输入完成
     */
    fun onComplete(code: String)
}