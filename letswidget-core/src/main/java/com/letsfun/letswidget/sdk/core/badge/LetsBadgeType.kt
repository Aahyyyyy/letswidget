package com.letsfun.letswidget.sdk.core.badge

object LetsBadgeType {
    /**
     * 仅适用于24x24图片，右上角显示红点
     */
    const val RED_POINT_24 = "RED_POINT_24"
    /**
     * 仅适用于50x50图片，右上角显示红点
     */
    const val RED_POINT_50 = "RED_POINT_50"
    /**
     * 适用于居中情况
     */
    const val RED_POINT_CENTER = "RED_POINT_CENTER"

    /**
     * 需要把24x24的目标图片居中放在一个60x60的RelativeLayout容器中，右上角显示数字
     */
    const val RED_NUMBER_24 = "RED_NUMBER_24"
    /**
     * 仅适用于50x50图片，右上角显示数字
     */
    const val RED_NUMBER_50 = "RED_NUMBER_50"
    /**
     * 适用于居中情况
     */
    const val RED_NUMBER_CENTER = "RED_NUMBER_CENTER"

    /**
     * 仅适用于50x50图片，右下角显示身份徽标
     */
    const val IDENTITY_50 = "IDENTITY_50"

    /**
     * 适用于居中情况，支持传入自定义颜色
     */
    const val CUSTOM_COLOR_NUMBER_CENTER = "CUSTOM_COLOR_NUMBER_CENTER"
}