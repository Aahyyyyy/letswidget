package com.letsfun.letswidget.sdk.core.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils

@SuppressLint("NewApi")
class LetsStatefulButton(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    companion object {
        const val STATE_CLICK_BEFORE = 0
        const val STATE_CLICK_LOADING = 1
        const val STATE_CLICK_AFTER = 2
        const val STATE_CLICK_UNABLE = 3

        const val SIZE_M = 0
        const val SIZE_S = 1
        const val SIZE_L = 2

        private const val DEFAULT_WIDTH_L = 90f
        private const val DEFAULT_WIDTH_M = 70f
        private const val DEFAULT_WIDTH_S = 60f
    }

    private var btnStateful: Button
    private var pbLoading: ProgressBar

    var text: String? = null
        set(value) {
            field = value
            btnStateful.text = field
        }

    var curState: Int = STATE_CLICK_BEFORE
        set(value) {
            field = value
            updateBtnState()
        }

    var btnSize: Int = SIZE_M
        set(value) {
            field = value
            updateBtnState()
        }

    /**
     * 点击前按钮文案颜色和不可点击时的按钮文案颜色
     */
    @ColorRes
    var normalTextColor = R.color.lets_widget_core_color_select_text_secondary
        set(value) {
            field = value
            updateBtnState()
        }

    /**
     * 点击后按钮文案颜色
     */
    @ColorRes
    var afterTextColor = R.color.lets_widget_core_color_text_secondary
        set(value) {
            field = value
            updateBtnState()
        }

    /**
     * 点击前背景和不可点击时的背景
     */
    @DrawableRes
    var normalBackground = R.drawable.lets_widget_core_shape_btn_state_bg
        set(value) {
            field = value
            updateBtnState()
        }

    /**
     * 点击后按钮文案颜色
     */
    @DrawableRes
    var afterBackground = R.drawable.lets_widget_core_shape_btn_secondary_tertiary_small
        set(value) {
            field = value
            updateBtnState()
        }

    private var leftIcon: Drawable? = null
        set(value) {
            field = value
            btnStateful.setCompoundDrawablesWithIntrinsicBounds(value, null, null, null)
        }

    var onBeforeClickListener: ((v: View)-> Unit)? = null
    var onAfterClickListener: ((v: View)-> Unit)? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.lets_widget_core_layout_stateful_button, this)
        btnStateful = findViewById(R.id.btn_stateful)
        pbLoading = findViewById(R.id.pb_loading)

        if (attrs != null) {
            context.obtainStyledAttributes(attrs, R.styleable.LetsStatefulButton).apply {
                text = getString(R.styleable.LetsStatefulButton_lets_state_btn_text)
                curState = getInt(R.styleable.LetsStatefulButton_lets_state_btn_state, curState)
                btnSize = getInt(R.styleable.LetsStatefulButton_lets_state_btn_size, btnSize)
                leftIcon = getDrawable(R.styleable.LetsStatefulButton_lets_state_btn_icon)
                recycle()
            }
        }

        setOnClickListener {
            if (curState == STATE_CLICK_BEFORE) {
                onBeforeClickListener?.invoke(it)
            } else if (curState == STATE_CLICK_AFTER){
                onAfterClickListener?.invoke(it)
            }
        }

        setProgressTint(ContextCompat.getColor(context, R.color.lets_widget_core_color_text_primary))

        if (paddingStart == 0 && paddingEnd == 0 && paddingLeft == 0 && paddingRight == 0){
            val horizontalPadding = resources.getDimensionPixelSize(R.dimen.lets_widget_core_space_3)
            setPadding(horizontalPadding, 0, horizontalPadding, 0)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        minimumWidth = when(btnSize){
            SIZE_L -> UiUtils.dp2px(context, DEFAULT_WIDTH_L)
            SIZE_S -> UiUtils.dp2px(context, DEFAULT_WIDTH_S)
            else -> UiUtils.dp2px(context, DEFAULT_WIDTH_M)
        }
        val height = when(btnSize){
            SIZE_L -> 45f
            SIZE_S -> 25f
            else -> 30f
        }
        val newHeightSpec = MeasureSpec.makeMeasureSpec(UiUtils.dp2px(context, height), MeasureSpec.EXACTLY)
        super.onMeasure(widthMeasureSpec, newHeightSpec)
    }

    private fun updateBtnState() {
        pbLoading.visibility = GONE
        btnStateful.visibility = VISIBLE
        btnStateful.setTextColor(resources.getColorStateList(normalTextColor, context.theme))
        if (btnSize == SIZE_L){
            btnStateful.setTextAppearance(R.style.LetsWidgetCoreTvBase_Body1Normal)
            pbLoading.layoutParams.width = UiUtils.dp2px(context, 24f)
        } else {
            btnStateful.setTextAppearance(R.style.LetsWidgetCoreTvBase_Body3Normal)
            pbLoading.layoutParams.width = UiUtils.dp2px(context, 16f)
        }
        setBackgroundResource(normalBackground)
        isEnabled = true
        when(curState){
            STATE_CLICK_LOADING -> {
                btnStateful.visibility = GONE
                pbLoading.visibility = VISIBLE
            }
            STATE_CLICK_AFTER -> {
                ContextCompat.getColor(context, afterTextColor).let { color ->
                    btnStateful.setTextColor(color)
                    leftIcon?.apply { setTint(color) }
                }
                setBackgroundResource(afterBackground)
            }
            STATE_CLICK_UNABLE -> {
                isEnabled = false
            }
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        btnStateful.isEnabled = enabled
    }

    fun setProgressTint(@ColorInt color: Int){
        val progressDrawable = pbLoading.indeterminateDrawable
        progressDrawable.setTint(color)
        pbLoading.indeterminateDrawable = progressDrawable
    }
}