package com.letsfun.letswidget.sdk.core.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.IntDef
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.badge.impl.*
import com.letsfun.letswidget.sdk.core.badge.interfaces.IBadgeNumberDotView

class LetsBadge @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), IBadgeNumberDotView {

    private var dotType: Int = DotType.DOT

    private var currentBadgeView: View? = null

    init {
        if (attrs != null) {
            context.obtainStyledAttributes(attrs, R.styleable.LetsBadge).apply {
                dotType = getInteger(R.styleable.LetsBadge_lets_badge_type, DotType.DOT)
                recycle()
            }
        }

        when (dotType) {
            DotType.DOT -> {
                currentBadgeView = BadgeDotView(getContext())
            }
            DotType.NUMBER_DOT -> {
                currentBadgeView = BadgeNumberDotView(getContext())
            }
            DotType.LEVEL_20, DotType.LEVEL_26 -> {
                currentBadgeView = BadgeIconView(context = getContext(), dotType = dotType)
            }
        }
        currentBadgeView?.let { view ->
            addView(view)
        }
    }

    @IntDef(DotType.DOT, DotType.NUMBER_DOT, DotType.LEVEL_20, DotType.LEVEL_26)
    @Target(
        AnnotationTarget.TYPE_PARAMETER,
        AnnotationTarget.VALUE_PARAMETER,
        AnnotationTarget.FIELD,
        AnnotationTarget.FUNCTION
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class DotType {
        companion object {
            /**
             * 小红点，尺寸8x8
             */
            const val DOT = 0

            /**
             * 带数字的小红点，尺寸 自适应width*14
             */
            const val NUMBER_DOT = 1

            const val LEVEL_20 = 2

            /**
             * 员工icon 26x26
             */
            const val LEVEL_26 = 3
        }
    }

    /**
     * FIXME 该部分可用注解优化
     * start
     */

    override fun setDotNumber(number: Int) {
        if (currentBadgeView is BadgeNumberDotView) {
            (currentBadgeView as BadgeNumberDotView).setDotNumber(number)
        }
    }

    override fun setNumberDotBgColor(colorResId: Int) {
        if (currentBadgeView is BadgeNumberDotView) {
            (currentBadgeView as BadgeNumberDotView).setNumberDotBgColor(colorResId)
        }
    }

    /**
     * FIXME 该部分可用注解优化
     * end
     */
}