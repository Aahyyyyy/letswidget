package com.letsfun.letswidget.sdk.core.loading

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.ContextWrapper
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils

@SuppressLint("NewApi")
open class LetsToastLoading(context: Context) : Dialog(context, R.style.lets_core_toast_loading_style) {
    private lateinit var tvHint: TextView
    private lateinit var ivDone: ImageView
    private var tdLoadingLg: LetsTwoDotsLoading? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lets_widget_core_layout_toast_loading)
        setCanceledOnTouchOutside(false)
        tvHint = findViewById(R.id.tvHint)
        ivDone = findViewById(R.id.ivDone)
        tdLoadingLg = findViewById(R.id.tdLoading)
    }

    override fun show() {
        if (isDialogValid()) {
            super.show()
            ivDone.visibility = View.GONE
            tdLoadingLg?.visibility = View.VISIBLE
        }
    }

    fun show(hint: String) {
        if (isDialogValid()) {
            super.show()
            UiUtils.setTextAutoShowHide(tvHint, hint)
            ivDone.visibility = View.GONE
            tdLoadingLg?.visibility = View.VISIBLE
        }
    }

    fun showDone(hint: String) {
        if (isDialogValid()) {
            super.show()
            UiUtils.setTextAutoShowHide(tvHint, hint)
            ivDone.setImageResource(R.drawable.lets_widget_core_icon_success_xl)
            ivDone.imageTintList = null
            ivDone.visibility = View.VISIBLE
            tdLoadingLg?.visibility = View.GONE
        }
    }

    fun showCustomer(hint: String, @DrawableRes iconRes: Int, @ColorRes tintColor: Int?) {
        if (isDialogValid()) {
            super.show()
            UiUtils.setTextAutoShowHide(tvHint, hint)
            ivDone.setImageResource(iconRes)
            tintColor?.let {
                ivDone.imageTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        context,
                        it
                    )
                )
            }
            ivDone.visibility = View.VISIBLE
            tdLoadingLg?.visibility = View.GONE
        }
    }

    fun update(hint: String) {
        if (isDialogValid()) {
            tvHint.text = hint
        }
    }

    override fun dismiss() {
        if (isDialogValid() && isShowing) {
            super.dismiss()
        }
    }

    private fun isDialogValid(): Boolean {
        var ctx: Context? = ownerActivity
        if (ctx == null) {
            ctx = context
        }
        if (ctx is ContextWrapper) {
            ctx = ctx.baseContext
        }
        return ctx is Activity && !ctx.isFinishing
    }
}