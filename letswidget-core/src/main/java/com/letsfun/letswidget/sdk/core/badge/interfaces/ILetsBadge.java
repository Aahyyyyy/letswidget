package com.letsfun.letswidget.sdk.core.badge.interfaces;

import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.view.View;

/**
 * @author chqiu
 *         Email:qstumn@163.com
 */

public interface ILetsBadge {

    ILetsBadge setBadgeNumber(int badgeNum);

    int getBadgeNumber();

    ILetsBadge setBadgeText(String badgeText);

    String getBadgeText();

    ILetsBadge setExactMode(boolean isExact);

    boolean isExactMode();

    ILetsBadge setShowShadow(boolean showShadow);

    boolean isShowShadow();

    ILetsBadge setBadgeBackgroundColor(int color);

    ILetsBadge stroke(int color, float width, boolean isDpValue);

    int getBadgeBackgroundColor();

    ILetsBadge setBadgeBackground(Drawable drawable);

    ILetsBadge setBadgeBackground(Drawable drawable, boolean clip);

    Drawable getBadgeBackground();

    ILetsBadge setBadgeTextColor(int color);

    int getBadgeTextColor();

    ILetsBadge setBadgeTextSize(float size, boolean isSpValue);

    float getBadgeTextSize(boolean isSpValue);

    /**
     * 设置圆形红点内部padding，如果无数字则为圆形红点半径
     */
    ILetsBadge setBadgePadding(float padding, boolean isDpValue);

    /**
     * 设置圆形红点内部padding，如果无数字则为椭圆形
     * @param paddingH
     * @param paddingV
     * @param isDpValue
     * @return
     */
    ILetsBadge setBadgePadding(float paddingH, float paddingV, boolean isDpValue);

    float getBadgePaddingH(boolean isDpValue);

    float getBadgePaddingV(boolean isDpValue);

    boolean isDraggable();

    ILetsBadge setBadgeGravity(int gravity);

    int getBadgeGravity();

    ILetsBadge setGravityOffset(float offset, boolean isDpValue);

    ILetsBadge setGravityOffset(float offsetX, float offsetY, boolean isDpValue);

    float getGravityOffsetX(boolean isDpValue);

    float getGravityOffsetY(boolean isDpValue);

    ILetsBadge setOnDragStateChangedListener(OnDragStateChangedListener l);

    PointF getDragCenter();

    ILetsBadge bindTarget(View view);

    View getTargetView();

    void setVisibility(int visibility);

    int getVisibility();

    void hide(boolean animate);

    ILetsBadge setRectWidthIsOnlyUseBadgeTextRectWidth(boolean use);

    boolean getRectWidthIsOnlyUseBadgeTextRectWidth();

    interface OnDragStateChangedListener {
        int STATE_START = 1;
        int STATE_DRAGGING = 2;
        int STATE_DRAGGING_OUT_OF_RANGE = 3;
        int STATE_CANCELED = 4;
        int STATE_SUCCEED = 5;

        void onDragStateChanged(int dragState, ILetsBadge badge, View targetView);
    }
}

