package com.letsfun.letswidget.sdk.core.edittext.icon.interfaces

import android.graphics.drawable.Drawable
import android.view.MotionEvent

interface IIconDrawable {
    fun getDrawable(): Drawable?

    fun dealWithTouchEvent(event: MotionEvent): Boolean

    fun setAlpha(alpha: Float)
}