package com.letsfun.letswidget.sdk.core.toast

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes
import com.letsfun.letswidget.lib.core.AppContext

object LetsToast {

    private var toastInstance: LetsToastImpl? = null

    /**
     * 优先使用application context
     */
    @Deprecated("Use default application context")
    @JvmStatic
    fun showToast(context: Context, message: String, duration: Int = Toast.LENGTH_SHORT) {
        showToast(message, duration)
    }

    @Deprecated("Use default application context")
    @JvmStatic
    fun showToast(context: Context, message: String) {
        showToast(message, Toast.LENGTH_SHORT)
    }

    @JvmStatic
    fun showToast(message: String) {
        showToast(message, Toast.LENGTH_SHORT)
    }

    @Deprecated("Use default application context")
    @JvmStatic
    fun showToast(context: Context, @StringRes resId: Int) {
        showToast(resId, Toast.LENGTH_SHORT)
    }

    @JvmStatic
    fun showToast(@StringRes resId: Int) {
        showToast(resId, Toast.LENGTH_SHORT)
    }

    /**
     * 默认显示样式，可自定义view（需要带id为message的textview）
     */
    @JvmStatic
    fun showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
        toastInstance = toastInstance ?: LetsToastImpl()
        toastInstance!!.showToast(message, duration)
    }


    @JvmStatic
    fun showToast(@StringRes resId: Int, duration: Int = Toast.LENGTH_SHORT) {
        showToast(AppContext.getApp().resources.getString(resId), duration)
    }

    /**
     * 完全自定义样式，一般不用
     */
    fun showToast(message: String, style: LetsToastStyle) {
        toastInstance = LetsToastImpl()
        toastInstance!!.showToast(message, style.duration, delayMillis = 0, style = style)
    }

    /**
     * 是否开启单位时间内，重复toast检查，
     * 开启：单位时间内不会显示text重复toast(时间为repeatTime)
     */
    fun enableCheckRepeat(enable: Boolean, repeatTime: Long = 2000) {
        toastInstance = toastInstance ?: LetsToastImpl()
        toastInstance!!.repeatTime = repeatTime
        toastInstance!!.checkRepeat = enable
    }


    /**
     * Toast.LENGTH_LONG 显示toast
     *
     * @param context
     * @param text
     */
    @Deprecated("Use default application context")
    @JvmStatic
    fun showLongToast(context: Context, text: String) {
        showToast(text, Toast.LENGTH_LONG)
    }

    @JvmStatic
    fun showLongToast(text: String) {
        showToast(text, Toast.LENGTH_LONG)
    }

    /**
     * Toast.LENGTH_LONG 显示toast
     *
     * @param context
     * @param resId
     */
    @Deprecated("Use default application context")
    @JvmStatic
    fun showLongToast(context: Context, @StringRes resId: Int) {
        showToast(resId, Toast.LENGTH_LONG)
    }

    @JvmStatic
    fun showLongToast(@StringRes resId: Int) {
        showToast(resId, Toast.LENGTH_LONG)
    }

}