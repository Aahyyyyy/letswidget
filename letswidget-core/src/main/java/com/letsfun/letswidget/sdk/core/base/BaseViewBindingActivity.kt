package com.letsfun.letswidget.sdk.core.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toolbar
import androidx.viewbinding.ViewBinding
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.titlebar.LetsTitleBar

abstract class BaseViewBindingActivity<T : ViewBinding> : BaseActivity() {
    lateinit var v: T

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        v = getViewBinding()
        setContentView(v.root)
        findViewById<LetsTitleBar>(R.id.letsTitleBar)?.apply {
            setNavigationOnClickListener {
                finish()
            }
            setTitle(getPageTitle())
        }
        val toolbar = findViewById<Toolbar>(R.id.demo_core_toolbar)
        toolbar?.title = getPageTitle()
        toolbar?.setNavigationOnClickListener {
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    abstract fun getViewBinding(): T

    abstract fun getPageTitle(): String
}