package com.letsfun.letswidget.sdk.core.edittext.icon.interfaces

import com.letsfun.letswidget.sdk.core.edittext.listener.OnIconClickListener

interface ISingleIconDrawable : IIconDrawable {
    fun setIcon(iconResId: Int): Boolean

    fun setOnIconClickListener(onIconClickListener: OnIconClickListener?)
}