package com.letsfun.letswidget.sdk.core.toast

import android.app.AppOpsManager
import android.app.Application
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Looper
import android.widget.Toast
import androidx.annotation.LayoutRes
import kotlinx.coroutines.*
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference
import java.lang.reflect.Field
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import com.letsfun.letswidget.lib.core.AppContext
import com.letsfun.letswidget.sdk.core.toast.modify.*

open class LetsToastImpl {
    /** Toast 对象  */
    private var mToastReference: WeakReference<Toast>? = null

    /**
     * 是否开启单位时间内，重复toast检查，
     * 开启：单位时间内不会显示重复toast(时间为repeatTime)
     */
    var checkRepeat: Boolean = false
    var repeatTime = 2000L

    private val hashSet: LinkedHashSet<CharSequence> by lazy {
        linkedSetOf()
    }

    private fun createToast(application: Application, style: LetsToastStyle): Toast {
        val toast = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N_MR1) {
            // 处理 Android 7.1 上 Toast 在主线程被阻塞后会导致报错的问题
            SafeToast(application)
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q && !areNotificationsEnabled(
                application
            )
        ) {
            // 处理 Toast 关闭通知栏权限之后无法弹出的问题
            // Android 10 版本修复了这个问题,且Android10禁止了hook
            NotificationToast(application)
        } else {
            SystemToast(application)
        }
        toast.view = style.getModifyView()
        toast.setGravity(
            style.gravity,
            style.xOffset,
            style.yOffset
        )
        toast.setMargin(style.marginH, style.marginV)
        return toast
    }

    /**
     * android 11之后自定义视图，在页面退出之后无法显示toast
     * 所以，finish之后showToast()会导致toast丢失
     * Blocking custom toast from package com.nio.app.** due to package not in the foreground
     */

    fun showToast(
        text: CharSequence,
        duration: Int,
        @LayoutRes layoutId: Int = -1,
        delayMillis: Long = 0,
        style: LetsToastStyle = LetsToastStyle()
    ) {
        if (!checkRepeat || !checkRepeat(text)) {
            style.duration = duration
            //SystemWindow，全局
            if(Looper.myLooper() == Looper.getMainLooper() && delayMillis == 0L){
                showToastSync(layoutId,style, duration, text)
            }else{
                GlobalScope.launch(Dispatchers.Main) {
                    delay(delayMillis)
                    showToastSync(layoutId, style, duration, text)
                }
            }
        }
    }

    private fun showToastSync(@LayoutRes layoutId: Int, style: LetsToastStyle, duration: Int, text: CharSequence){
        style.layoutId = layoutId
        val toast = createToast(AppContext.getApp(), style)
        mToastReference = WeakReference<Toast>(toast)
        toast.duration = duration
        toast.setText(text)
        try {
            toast.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 取消任务
     */
    fun cancelToast() {
        //全局
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                val toast = mToastReference?.get()
                toast?.cancel()
            }
        }
    }

    /**
     * 是否有通知栏权限
     */
    private fun areNotificationsEnabled(context: Context?): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context!!.getSystemService(NotificationManager::class.java)
                .areNotificationsEnabled()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 参考 Support 库中的方法： NotificationManagerCompat.from(context).areNotificationsEnabled()
            val appOps: AppOpsManager =
                context!!.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
            return try {
                val method: Method = appOps.javaClass.getMethod(
                    "checkOpNoThrow",
                    Integer.TYPE, Integer.TYPE, String::class.java
                )
                val field: Field = appOps.javaClass.getDeclaredField("OP_POST_NOTIFICATION")
                val value = field[Int::class.java] as Int
                method.invoke(
                    appOps, value, context.applicationInfo.uid,
                    context.packageName
                ) as Int == AppOpsManager.MODE_ALLOWED
            } catch (e: NoSuchMethodException) {
                e.printStackTrace()
                true
            } catch (e: NoSuchFieldException) {
                e.printStackTrace()
                true
            } catch (e: InvocationTargetException) {
                e.printStackTrace()
                true
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
                true
            } catch (e: RuntimeException) {
                e.printStackTrace()
                true
            }
        }
        return true
    }

    private fun checkRepeat(text: CharSequence): Boolean {
        return if (hashSet.contains(text)) {
            true
        } else {
            hashSet.add(text)
            removeRepeatDelay(text)
            false
        }
    }

    private fun removeRepeatDelay(text: CharSequence) {
        //app全局生效
        GlobalScope.launch {
            delay(repeatTime)
            hashSet.remove(text)
        }
    }
}