@file:JvmName("TextViewHelper")

package com.letsfun.letswidget.sdk.core.utils

import android.annotation.SuppressLint
import android.content.res.Resources
import android.util.TypedValue
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.AttrRes
import androidx.annotation.StyleRes

@SuppressLint("NewApi")
@JvmOverloads
fun <T : TextView> T.applyStyle(
    @StyleRes resId: Int,
    isOverridePadding: Boolean = true
) {
    setTextAppearance(resId)
    LetsTextAppearance(context, resId).updateMeasureState(this, isOverridePadding)
}

@JvmOverloads
fun <T : TextView> T.applyThemeAttr(
    @AttrRes attrResId: Int,
    isOverridePadding: Boolean = true
) {
    val typedValue = TypedValue()
    val theme: Resources.Theme = context.theme
    theme.resolveAttribute(attrResId, typedValue, true)
    applyStyle(typedValue.data, isOverridePadding)
}

@SuppressLint("NewApi")
@JvmOverloads
fun <T : EditText> T.applyEditStyle(
    @StyleRes resId: Int,
    isOverridePadding: Boolean = true
) {
    setTextAppearance(resId)
    LetsEditAppearance(context, resId).updateMeasureState(this)
    LetsTextAppearance(context, resId).updateMeasureState(this, isOverridePadding)
}

@JvmOverloads
fun <T : EditText> T.applyEditThemeAttr(
    @AttrRes attrResId: Int,
    isOverridePadding: Boolean = true
) {
    val typedValue = TypedValue()
    val theme: Resources.Theme = context.theme
    theme.resolveAttribute(attrResId, typedValue, true)
    applyEditStyle(typedValue.data, isOverridePadding)
}