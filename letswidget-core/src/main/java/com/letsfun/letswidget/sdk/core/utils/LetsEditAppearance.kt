package com.letsfun.letswidget.sdk.core.utils

import android.annotation.SuppressLint
import android.content.Context
import android.widget.EditText
import androidx.annotation.StyleRes
import com.letsfun.letswidget.sdk.core.R

@SuppressLint("NewApi")
internal class LetsEditAppearance(
    context: Context,
    @StyleRes id: Int
) {

    val editable:Boolean
    val hint:String?
    val inputType:Int

    fun updateMeasureState(editText: EditText){
        if(inputType != -1) editText.inputType = inputType
        editText.isFocusable = editable
        editText.isFocusableInTouchMode = editable
        hint?.let {
            editText.hint = it
        }
    }

    init {
        val a = context.obtainStyledAttributes(id, R.styleable.LetsEditAppearance)
        inputType = a.getInt(R.styleable.LetsEditAppearance_android_inputType, -1)
        editable = a.getBoolean(R.styleable.LetsEditAppearance_android_editable,true)
        hint = a.getString(R.styleable.LetsEditAppearance_android_hint)
        a.recycle()
    }

}