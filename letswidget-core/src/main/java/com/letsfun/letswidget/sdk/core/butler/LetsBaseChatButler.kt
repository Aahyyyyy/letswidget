package com.letsfun.letswidget.sdk.core.butler

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils

class LetsBaseChatButler(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    val ivBack : ImageView

    init {
        val v = LayoutInflater.from(context).inflate(R.layout.lets_widget_core_layout_base_chat_butler, this)

        ivBack = v.findViewById(R.id.ivBack)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val measureWidth = MeasureSpec.getSize(widthMeasureSpec)
        val measureHeight = UiUtils.dp2px(context, 50f)

        setMeasuredDimension(measureWidth, measureHeight)
    }
}