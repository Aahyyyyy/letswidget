package com.letsfun.letswidget.sdk.dialog

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.ext.toVisible
import com.letsfun.letswidget.sdk.core.ext.toGone
import com.letsfun.letswidget.sdk.core.utils.UiUtils
import com.letsfun.letswidget.sdk.core.utils.applyStyle

class LetsCommonDialog(context: Context) : LetsBaseDialog(context) {
    override val customResourceId: Int get() = R.layout.lets_widget_core_layout_dialog_common
    override val windowGravity: Int get() = Gravity.CENTER

    private lateinit var tvTitle: TextView
    private lateinit var tvMsg: TextView
    lateinit var btnCancel: Button
    lateinit var btnConfirm: Button
    private lateinit var tvLink: TextView

    //对外开放，用于外部处理check事件
    lateinit var checkBoxAgree: CheckBox

    //对外开放，用于外部处理协议文本样式
    lateinit var tvAgree: TextView

    private lateinit var rlAgreement: RelativeLayout

    var title: String = ""
    var message: String = ""
    var type: Int = 1
    var isTitle: Boolean = true
    var isWarning: Boolean = false
    var isAgreeCenter: Boolean = false
    var linkText: String? = null
    var agreeText: String? = null

    var leftBtnText: String = context.resources.getString(R.string.lets_widget_core_cancel)
    var rightBtnText: String = context.resources.getString(R.string.lets_widget_core_ok)
    var leftClick: (() -> Unit)? = null
    var rightClick: (() -> Unit)? = null
    var linkClick: (() -> Unit)? = null

    fun title(title: String): LetsCommonDialog {
        this.title = title
        return this
    }

    fun message(message: String): LetsCommonDialog {
        this.message = message
        return this
    }

    fun type(type: Int): LetsCommonDialog {
        this.type = type
        return this
    }

    fun warning(isWarning: Boolean): LetsCommonDialog {
        this.isWarning = isWarning
        return this
    }

    fun centerAgree(isAgreeCenter: Boolean): LetsCommonDialog {
        this.isAgreeCenter = isAgreeCenter
        return this
    }

    fun linkText(linkText: String?): LetsCommonDialog {
        this.linkText = linkText
        return this
    }

    fun agreeText(agreeText: String?): LetsCommonDialog {
        this.agreeText = agreeText
        return this
    }

    fun leftBtnText(leftBtnText: String): LetsCommonDialog {
        this.leftBtnText = leftBtnText
        return this
    }

    fun rightBtnText(rightBtnText: String): LetsCommonDialog {
        this.rightBtnText = rightBtnText
        return this
    }

    fun rightClick(rightClick: (() -> Unit)?): LetsCommonDialog {
        this.rightClick = rightClick
        return this
    }

    fun leftClick(leftClick: (() -> Unit)?): LetsCommonDialog {
        this.leftClick = leftClick
        return this
    }

    fun linkClick(linkClick: (() -> Unit)?): LetsCommonDialog {
        this.linkClick = linkClick
        return this
    }

    fun showCommonDialog() {
        showCommonDialog(
            title,
            message,
            leftBtnText,
            rightBtnText,
            leftClick,
            rightClick,
            type,
            isTitle,
            isWarning,
            linkText,
            linkClick
        )
    }

    fun showSingleDialog() {
        showSingleButtonDialog(
            title,
            message,
            rightBtnText,
            rightClick,
            type,
            isTitle,
            isWarning,
            linkText,
            linkClick
        )
    }

    fun isCheckAgree(): Boolean {
        return checkBoxAgree.isChecked
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCanceledOnTouchOutside(false)
        tvTitle = findViewById(R.id.tvTitle)
        tvMsg = findViewById(R.id.tvMsg)
        btnCancel = findViewById(R.id.btnCancel)
        btnConfirm = findViewById(R.id.btnConfirm)
        tvLink = findViewById(R.id.tvLink)
        rlAgreement = findViewById(R.id.rlAgreement)
        checkBoxAgree = findViewById(R.id.checkAgree)
        tvAgree = findViewById(R.id.tvAgreement)
    }

    private fun showDialog(
        title: String,
        message: String,
        leftBtnText: String = context.resources.getString(R.string.lets_widget_core_cancel),
        rightBtnText: String = context.resources.getString(R.string.lets_widget_core_ok),
        leftClick: (() -> Unit)? = null,
        rightClick: (() -> Unit)? = null,
        type: Int = 1,
        isTitle: Boolean = true,
        isWarning: Boolean = false,
        linkText: String? = null,
        linkClick: (() -> Unit)? = null
    ) {
        when (type) {
            1 -> {
                rlAgreement.toGone()
                tvLink.toGone()
            }
            2 -> {
                rlAgreement.toGone()
                tvLink.toVisible()
            }
            3 -> {
                rlAgreement.toVisible()
                checkBoxAgree.toVisible()
                tvAgree.toVisible()
                tvLink.toGone()
            }
        }

        if (linkText?.isNotEmpty() == true) {
            tvLink.toVisible()
        }
        if (agreeText?.isNotEmpty() == true) {
            rlAgreement.toVisible()
            tvAgree.toVisible()
            tvAgree.text = agreeText
        }
        if (isTitle) {
            tvTitle.toVisible()
        } else {
            tvTitle.toGone()
            (tvMsg.layoutParams as ConstraintLayout.LayoutParams).topMargin =
                UiUtils.dp2px(context, 25f)
        }
        btnConfirm.applyStyle(
            if (isWarning) R.style.LetsWidgetCoreButtonBase_Danger else R.style.LetsWidgetCoreButtonBase_Primary
        )
        if (title.isNullOrEmpty()) {
            tvTitle.toGone()
            (tvMsg.layoutParams as ConstraintLayout.LayoutParams).topMargin =
                UiUtils.dp2px(context, 25f)
        } else {
            tvTitle.text = title
        }
        if (message.isNullOrEmpty()) {
            tvMsg.toGone()
        } else {
            tvMsg.text = message
        }
        btnCancel.apply {
            text = leftBtnText
            setOnClickListener {
                dismiss()
                leftClick?.invoke()
            }
        }

        btnConfirm.apply {
            text = rightBtnText
            setOnClickListener {
                dismiss()
                rightClick?.invoke()
            }
        }
        tvLink.apply {
            text = linkText
            setOnClickListener {
                linkClick?.invoke()
            }
        }


        if(isAgreeCenter) {
            rlAgreement.gravity = Gravity.CENTER
        } else {
            rlAgreement.gravity = Gravity.START
        }

        show()
    }

    /**
     * 双按钮
     * @param title 标题
     * @param message 描述信息
     * @param leftBtnText 左边按钮文本
     * @param rightBtnText 右边按钮文本
     * @param leftClick 左边按钮点击事件
     * @param rightClick 右边按钮点击事件
     * @param type 展示类型 默认1，不带link 不带协议；2带link；3带协议内容
     * @param isTitle 是否展示title
     * @param isWarning 是否是警告类型
     * @param linkText 链接文本
     * @param linkClick 链接点击事件
     */
    fun showCommonDialog(
        title: String,
        message: String,
        leftBtnText: String = context.resources.getString(R.string.lets_widget_core_cancel),
        rightBtnText: String = context.resources.getString(R.string.lets_widget_core_ok),
        leftClick: (() -> Unit)? = null,
        rightClick: (() -> Unit)? = null,
        type: Int = 1,
        isTitle: Boolean = true,
        isWarning: Boolean = false,
        linkText: String? = null,
        linkClick: (() -> Unit)? = null
    ) {
        show()
        btnCancel.toVisible()
        showDialog(
            title,
            message,
            leftBtnText,
            rightBtnText,
            leftClick,
            rightClick,
            type,
            isTitle,
            isWarning,
            linkText,
            linkClick
        )
        //先设置style，后设置宽度，不然style会覆盖宽度
        btnConfirm.layoutParams.apply {
            width = 0
        }
    }

    /**
     * 单按钮
     * @param title 标题
     * @param message 描述信息
     * @param rightBtnText 右边按钮文本
     * @param rightClick 右边按钮点击事件
     * @param type 展示类型 默认1，不带link 不带协议；2带link；3带协议内容
     * @param isTitle 是否展示title
     * @param isWarning 是否是警告类型
     * @param linkText 链接文本
     * @param linkClick 链接点击事件
     */
    fun showSingleButtonDialog(
        title: String,
        message: String,
        rightBtnText: String = context.resources.getString(R.string.lets_widget_core_ok),
        rightClick: (() -> Unit)? = null,
        type: Int = 1,
        isTitle: Boolean = true,
        isWarning: Boolean = false,
        linkText: String? = null,
        linkClick: (() -> Unit)? = null
    ) {
        show()
        btnCancel.toGone()
        showDialog(
            title,
            message,
            "",
            rightBtnText,
            null,
            rightClick,
            type,
            isTitle,
            isWarning,
            linkText,
            linkClick
        )
        //先设置style，后设置宽度，不然style会覆盖宽度
        btnConfirm.layoutParams.apply {
            width = ViewGroup.LayoutParams.MATCH_PARENT
        }
    }

}