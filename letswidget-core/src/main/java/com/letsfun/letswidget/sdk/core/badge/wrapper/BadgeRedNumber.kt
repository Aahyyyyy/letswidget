package com.letsfun.letswidget.sdk.core.badge.wrapper

import com.letsfun.letswidget.sdk.core.badge.interfaces.ILetsBadge
import com.letsfun.letswidget.sdk.core.badge.impl.LetsBadgeImpl
import android.content.Context
import android.view.Gravity
import android.view.View

class LetsBadgeRedNumber24(context: Context?, targetView: View?, badgeView: ILetsBadge = LetsBadgeImpl(context)) :
    ILetsBadge by badgeView {
    init {
        badgeView.bindTarget(targetView)
        badgeView.setBadgeTextSize(10f, true)
        badgeView.badgeGravity = Gravity.CENTER or Gravity.TOP
        badgeView.isShowShadow = false
        badgeView.setBadgePadding(3f, 0.5f, true)
        badgeView.setGravityOffset(12f, 18f, true)
        badgeView.badgeNumber = 0
    }
}

class LetsBadgeRedNumber50(context: Context?, targetView: View?, badgeView: ILetsBadge = LetsBadgeImpl(context)) :
    ILetsBadge by badgeView {
    init {
        badgeView.bindTarget(targetView)
        badgeView.badgeGravity = Gravity.END or Gravity.TOP
        badgeView.setBadgeTextSize(10f, true)
        badgeView.isShowShadow = false
        badgeView.setBadgePadding(3f, 0.5f, true)
        badgeView.setGravityOffset(0f, 0f, true)
        badgeView.badgeNumber = 0
    }
}

class LetsBadgeRedNumberCenter(
    context: Context?,
    targetView: View?,
    badgeView: ILetsBadge = LetsBadgeImpl(context),
    xOffSet: Float = 0f,
    yOffSet: Float = 0f,
    paddingH: Float = 3.0f,
    paddingV: Float = 0.5f,
) : ILetsBadge by badgeView {
    init {
        badgeView.bindTarget(targetView)
        badgeView.badgeGravity = Gravity.CENTER or Gravity.END
        badgeView.setBadgeTextSize(10f, true)
        badgeView.isShowShadow = false
        badgeView.setBadgePadding(paddingH, paddingV, true)
        badgeView.setGravityOffset(xOffSet, yOffSet, true)
        badgeView.badgeNumber = 0
        badgeView.rectWidthIsOnlyUseBadgeTextRectWidth = true
    }
}


class LetsBadgeCustomBgColorNumberCenter(
    context: Context?,
    targetView: View?,
    badgeView: ILetsBadge = LetsBadgeImpl(context),
    xOffSet: Float = 0f,
    yOffSet: Float = 0f,
    paddingH: Float = 3.0f,
    paddingV: Float = 0.5f,
    bgColor: Int
) : ILetsBadge by badgeView {
    init {
        badgeView.bindTarget(targetView)
        badgeView.badgeGravity = Gravity.CENTER or Gravity.END
        badgeView.setBadgeTextSize(10f, true)
        badgeView.isShowShadow = false
        badgeView.setBadgePadding(paddingH, paddingV, true)
        badgeView.setGravityOffset(xOffSet, yOffSet, true)
        badgeView.badgeNumber = 0
        badgeView.rectWidthIsOnlyUseBadgeTextRectWidth = true
        badgeView.badgeBackgroundColor = bgColor
    }
}