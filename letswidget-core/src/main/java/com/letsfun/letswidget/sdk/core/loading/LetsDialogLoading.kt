package com.letsfun.letswidget.sdk.core.loading

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.letsfun.letswidget.sdk.core.R

class LetsDialogLoading(context: Context) : Dialog(context, R.style.lets_widget_core_two_dots_loading_dialog_style) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lets_widget_core_layout_dialog_loading)
    }
}