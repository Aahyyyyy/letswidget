package com.letsfun.letswidget.sdk.core.utils

import android.content.Context
import android.view.View
import android.widget.TextView

object UiUtils {
    /**
     * dp转px
     *
     * @param dpValue dp值
     * @return px值
     */
    fun dp2px(context: Context, dpValue: Float): Int {
        val scale = context.resources
            .displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }

    /**
     * px转dp
     *
     * @param pxValue px值
     * @return dp值
     */
    fun px2dp(context: Context, pxValue: Float): Int {
        val scale = context.resources
            .displayMetrics.density
        return (pxValue / scale + 0.5f).toInt()
    }

    /**
     * 获取屏幕的宽度（单位：px）
     *
     * @return 屏幕宽
     */
    fun getScreenWidth(context: Context): Int {
        return context.resources
            .displayMetrics.widthPixels
    }

    /**
     * 获取屏幕的高度（单位：px）
     *
     * @return 屏幕高
     */
    fun getScreenHeight(context: Context): Int {
        return context.resources
            .displayMetrics.heightPixels
    }

    fun setTextAutoShowHide(textview: TextView, content: String?) {
        if (content.isNullOrEmpty()) {
            textview.visibility = View.GONE
        } else {
            textview.visibility = View.VISIBLE
            textview.text = content
        }
    }
}