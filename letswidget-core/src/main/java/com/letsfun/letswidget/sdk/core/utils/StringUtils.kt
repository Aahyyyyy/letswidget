package com.letsfun.letswidget.sdk.core.utils

import android.text.TextUtils

object StringUtils {
    private const val VALUE_NULL = "null"

    fun concat(vararg args: CharSequence?): String {
        val stringBuilder: StringBuilder = StringBuilder()
        if (args.isNotEmpty()) {
            for (arg: CharSequence? in args) {
                if (!TextUtils.isEmpty(arg)) {
                    stringBuilder.append(arg)
                }
            }
        }
        return stringBuilder.toString()
    }
}