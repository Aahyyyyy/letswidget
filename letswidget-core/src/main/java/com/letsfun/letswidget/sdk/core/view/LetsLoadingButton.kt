package com.letsfun.letswidget.sdk.core.view


import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.databinding.LetsWidgetCoreLayoutLoadingButtonBinding
import com.letsfun.letswidget.sdk.core.utils.UiUtils
import com.letsfun.letswidget.sdk.core.utils.applyStyle

class LetsLoadingButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val v: LetsWidgetCoreLayoutLoadingButtonBinding =
        LetsWidgetCoreLayoutLoadingButtonBinding.inflate(
            LayoutInflater.from(context), this, true
        )

    var onLoadingButtonClick: (() -> Unit)? = null

    private var isLoading: Boolean = false

    var buttonStyle = R.style.LetsWidgetCoreButtonBase_Primary
        @SuppressLint("NewApi")
        set(value) {
            field = value
            v.button.applyStyle(value)
            v.button.layoutParams = v.button.layoutParams.apply {
                width = ViewGroup.LayoutParams.MATCH_PARENT
            }
            v.button.maxLines = 2
            v.button.ellipsize = TextUtils.TruncateAt.END
            v.button.setPadding(
                v.button.paddingLeft,
                0,
                v.button.paddingRight,
                0
            )
            val attr = IntArray(1) {
                android.R.attr.textColor
            }
            val typeArray = context.theme.obtainStyledAttributes(value, attr)
            val progressDrawable = v.progressbar.indeterminateDrawable
            progressDrawable.setTint(typeArray.getColor(0, -1))
            v.progressbar.indeterminateDrawable = progressDrawable

            if (v.button.layoutParams.height > UiUtils.dp2px(context, 40f)){
                UiUtils.dp2px(context, 20f)
            } else {
                UiUtils.dp2px(context, 16f)
            }.let {
                v.progressbar.layoutParams.apply {
                    width = it
                    height = it
                }
            }
        }

    var btnText: String = ""
        set(value) {
            field = value
            v.button.text = value
            v.button.layoutParams = v.button.layoutParams.apply {
                width = ViewGroup.LayoutParams.MATCH_PARENT
            }
            invalidate()
        }

    init {
        attrs?.let {
            context.obtainStyledAttributes(it, R.styleable.LetsLoadingButton).apply {
                btnText = getString(R.styleable.LetsLoadingButton_lets_load_btn_text) ?: ""
                buttonStyle = getResourceId(
                    R.styleable.LetsLoadingButton_lets_load_btn_style,
                    R.style.LetsWidgetCoreButtonBase_Primary
                )
                isEnabled = getBoolean(R.styleable.LetsLoadingButton_lets_load_btn_enabled, true)
                recycle()
            }
        }
        v.button.setOnClickListener {
            onLoadingButtonClick?.invoke()
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        v.button.isEnabled = enabled
    }

    fun startLoading() {
        if (isLoading) {
            return
        }
        v.progressbar.visibility = VISIBLE
        v.button.text = ""
        isLoading = true
    }

    fun endLoading() {
        if (!isLoading) {
            return
        }
        v.progressbar.visibility = GONE
        v.button.text = btnText
        isLoading = false
    }

    fun isLoading(): Boolean = isLoading
}