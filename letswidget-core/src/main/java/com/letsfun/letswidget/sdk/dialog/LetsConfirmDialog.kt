package com.letsfun.letswidget.sdk.dialog

import android.content.Context
import androidx.appcompat.app.AlertDialog

class LetsConfirmDialog(var context: Context) {
    private var alertDialog: AlertDialog? = null

    var title: String = ""
    var message: String = ""
    var negativeText: String = ""
    var positiveText: String = ""
    var cancelableOnTouchOutside = true
    var cancelable = true
    var negativeListener: (() -> Unit)? = null
    var positiveListener: (() -> Unit)? = null
    var dismissListener: (() -> Unit)? = null

    fun show() {
        if (alertDialog == null) {
            alertDialog = AlertDialog.Builder(context).run {
                if (title.isNotEmpty()) setTitle(title)
                if (message.isNotEmpty()) setMessage(message)
                if (negativeText.isNotEmpty()) setNegativeButton(negativeText) { _, _ -> negativeListener?.invoke() }
                if (positiveText.isNotEmpty()) setPositiveButton(positiveText) { _, _ -> positiveListener?.invoke() }
                if (dismissListener != null) setOnDismissListener { dismissListener?.invoke() }
                create()
            }.apply {
                setCanceledOnTouchOutside(cancelableOnTouchOutside)
                setCancelable(cancelable)
            }
        }
        alertDialog?.show()
    }

    fun hide() {
        alertDialog?.hide()
    }

    fun dismiss() {
        alertDialog?.dismiss()
        alertDialog = null
    }
}