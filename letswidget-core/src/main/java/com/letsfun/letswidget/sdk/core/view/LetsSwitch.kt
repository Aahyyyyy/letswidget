package com.letsfun.letswidget.sdk.core.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.letsfun.letswidget.sdk.core.R
import com.letsfun.letswidget.sdk.core.utils.UiUtils

@SuppressLint("NewApi")
class LetsSwitch(context: Context, attributeSet: AttributeSet): View(context, attributeSet) {
    private val switchRect: RectF

    private val switchTopPadding = UiUtils.dp2px(context, 3f).toFloat()
    private val switchStartPadding = UiUtils.dp2px(context, 1f).toFloat()

    private val switchWidth = UiUtils.dp2px(context, 48f).toFloat()
    private val switchHeight = UiUtils.dp2px(context, 24f).toFloat()
    private val switchRectRadius = UiUtils.dp2px(context, 14f).toFloat()
    private val switchRadius = UiUtils.dp2px(context, 11f).toFloat()
    private val switchRadiusMargin = UiUtils.dp2px(context, 1f).toFloat()

    private var switchEnabled = true
    private var switchStatus = false
    private var switchThumbColor = context.getColor(R.color.lets_widget_core_color_bg_primary)
    private var switchOnColor = context.getColor(R.color.lets_widget_core_color_basic_primary)
    private var switchOffColor = context.getColor(R.color.lets_widget_core_color_bg_disable)

    private var switchTrackPaint: Paint
    private var switchThumbPaint: Paint

    init {
        val a = context.obtainStyledAttributes(attributeSet,R.styleable.LetsSwitch)
        a.apply {
            switchEnabled = getBoolean(R.styleable.LetsSwitch_lets_switch_enabled, true)
            switchStatus = getBoolean(R.styleable.LetsSwitch_lets_switch_checked,false)
            recycle()
        }

        if (!switchEnabled) {
            switchOnColor = context.getColor(R.color.lets_widget_core_color_support_warning_2)
            switchOffColor = context.getColor(R.color.lets_widget_core_color_bg_secondary)
        }

        switchRect = RectF().apply {
            top = switchTopPadding
            left = switchStartPadding
            bottom = switchTopPadding + switchHeight
            right = switchStartPadding + switchWidth
        }

        switchTrackPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.FILL
            color = switchOffColor
        }

        switchThumbPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.FILL
            color = switchThumbColor
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val measureWidth = UiUtils.dp2px(context, 50f)
        val measureHeight = UiUtils.dp2px(context, 30f)

        setMeasuredDimension(measureWidth, measureHeight)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawSwitch(canvas)
    }

    /**
     * 绘制开关
     */
    private fun drawSwitch(canvas: Canvas) {
        if (switchStatus) {
            canvas.apply {
                switchTrackPaint.color = switchOnColor
                drawRoundRect(switchRect, switchRectRadius, switchRectRadius, switchTrackPaint)
                drawCircle(
                    switchStartPadding + switchWidth - switchRadiusMargin - switchRadius,
                    switchTopPadding + switchRadiusMargin + switchRadius,
                    switchRadius,
                    switchThumbPaint
                )
            }
        } else {
            canvas.apply {
                switchTrackPaint.color = switchOffColor
                drawRoundRect(switchRect, switchRectRadius, switchRectRadius, switchTrackPaint)
                drawCircle(
                    switchStartPadding + switchRadiusMargin + switchRadius,
                    switchTopPadding + switchRadiusMargin + switchRadius,
                    switchRadius,
                    switchThumbPaint
                )
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (switchEnabled) {
            when (event?.action) {
                MotionEvent.ACTION_UP -> {
                    switchStatus = !switchStatus
                    invalidate()
                }
            }
        }
        return true
    }

    fun setSwitchStatus(status: Boolean) {
        switchStatus = status
        invalidate()
    }

    fun getSwitchStatus(): Boolean {
        return switchStatus
    }

    fun setSwitchEnabled(enabled: Boolean) {
        switchEnabled = enabled
        invalidate()
    }

    fun getSwitchEnabled(): Boolean {
        return switchEnabled
    }
}

