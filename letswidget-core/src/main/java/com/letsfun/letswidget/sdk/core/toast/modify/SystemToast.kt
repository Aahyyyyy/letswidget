package com.letsfun.letswidget.sdk.core.toast.modify

import android.app.Application
import android.view.View
import android.widget.TextView
import android.widget.Toast

open class SystemToast(application: Application?) : Toast(application) {

    private var mMessageView: TextView? = null

    override fun setView(view: View?) {
        //Android11之后标记为废弃方法，但Android12上仍然可以用
        super.setView(view)
        view?.let {
            mMessageView = findMessageView(view)
        } ?: let {
            mMessageView = null
        }
    }

    override fun setText(text: CharSequence?) {
        mMessageView?.text = text
    }

    /**
     * 智能获取用于显示消息的 TextView
     */
    private fun findMessageView(view: View?): TextView? {
        if (view == null) {
            return view
        }
        if (view is TextView) {
            if (view.getId() == View.NO_ID) {
                view.setId(android.R.id.message)
            } else require(view.getId() == android.R.id.message) {
                // 必须将 TextView 的 id 值设置成 android.R.id.message
                "You must set the ID value of TextView to android.R.id.message"
            }
            return view
        }
        if (view.findViewById<View>(android.R.id.message) is TextView) {
            return view.findViewById<View>(android.R.id.message) as TextView
        }
        throw IllegalArgumentException("You must include a TextView with an ID value of android.R.id.message")
    }

}