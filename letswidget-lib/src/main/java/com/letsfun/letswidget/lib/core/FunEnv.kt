package com.letsfun.letswidget.lib.core

import androidx.annotation.IntDef

@IntDef(FunEnv.DEV, FunEnv.TEST, FunEnv.STAGING, FunEnv.PROD)
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
annotation class FunEnv {
    companion object {
        const val PROD: Int = 0
        const val STAGING: Int = 1
        const val TEST: Int = 2
        const val DEV: Int = 3
    }
}
