package com.letsfun.letswidget.lib.core.locale

import java.util.*
import kotlin.collections.HashMap

object LetsLocaleConst {
    val LETS_SUPPORTED_LANGUAGES = setOf(LetsLangCode.ZH_CN, LetsLangCode.EN_GB, LetsLangCode.NB_NO)
    val CN_SUPPORTED_LANGUAGES = setOf(LetsLangCode.ZH_CN)
    val MP_SUPPORTED_LANGUAGES = setOf(
        LetsLangCode.EN_GB,
        LetsLangCode.NB_NO,
        LetsLangCode.SV_SE,
        LetsLangCode.DE_DE,
        LetsLangCode.DA_DK,
        LetsLangCode.NL_NL,
    )

    val DISPLAY_NAME_MAP = HashMap<String, String>().apply {
        put(LetsLangCode.ZH_CN, "简体中文")
        put(LetsLangCode.EN_GB, "English (UK)")
        put(LetsLangCode.NB_NO, "Norsk bokmål (Norge)")
        put(LetsLangCode.SV_SE, "Swedish")
        put(LetsLangCode.DA_DK, "Danish")
        put(LetsLangCode.NL_NL, "Dutch（the Netherlands)")
        put(LetsLangCode.DE_DE, "Deutsch")
    }

    val LOCALE_MAP = HashMap<String, Locale>().apply {
        put(LetsLangCode.ZH_CN, Locale.CHINA)
        put(LetsLangCode.EN_GB, Locale.UK)
        put(LetsLangCode.NB_NO, Locale("nb", "NO"))
        put(LetsLangCode.SV_SE, Locale("sv", "SE"))
        put(LetsLangCode.DA_DK, Locale("da", "DK"))
        put(LetsLangCode.NL_NL, Locale("nl", "NL"))
        put(LetsLangCode.DE_DE, Locale.GERMANY)
    }
}