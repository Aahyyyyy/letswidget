package com.letsfun.letswidget.lib.core.utils

import android.annotation.SuppressLint
import android.os.Build
import android.security.KeyPairGeneratorSpec
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.text.TextUtils
import androidx.annotation.RequiresApi
import com.letsfun.letswidget.lib.core.AppContext
import java.io.IOException
import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.security.*
import java.security.cert.Certificate
import java.security.cert.CertificateException
import java.security.spec.AlgorithmParameterSpec
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.security.auth.x500.X500Principal

class KeyStoreUtils private constructor() {
    private var keyStore: KeyStore? = null
    private var spUtils: SpUtils? = null
    private var fastAesKey: String? = null
    private var fastAesIv: String? = null

    private object KeyStoreUtilsHolder {
        val instance: KeyStoreUtils = KeyStoreUtils()
    }

    val isKeyGenerated: Boolean
        get() {
            return isKeyGenerated(KEY_ALIAS_DEFAULT)
        }

    fun isKeyGenerated(alias: String?): Boolean {
        initKeyStore()
        try {
            return keyStore!!.containsAlias(alias)
        } catch (e: KeyStoreException) {
            e.printStackTrace()
            return false
        }
    }

    @Synchronized
    private fun initKeyStore() {
        try {
            if (keyStore == null) {
                keyStore = KeyStore.getInstance(ANDROID_KEYSTORE_PROVIDER)
                keyStore!!.load(null)
            }
        } catch (e: KeyStoreException) {
            throw RuntimeException("Failed to load Keystore", e)
        } catch (e: CertificateException) {
            throw RuntimeException("Failed to load Keystore", e)
        } catch (e: IOException) {
            throw RuntimeException("Failed to load Keystore", e)
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to load Keystore", e)
        }
    }

    @Throws(GeneralSecurityException::class)
    fun encryptWithSecureAes(plainText: String): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) encryptWithSecureAes(
            KEY_ALIAS_DEFAULT, plainText
        ) else (encryptWithFastAes(plainText))!!
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Throws(GeneralSecurityException::class)
    fun encryptWithSecureAes(alias: String, plainText: String?): String {
        val cipher: Cipher
        if (TextUtils.isEmpty(plainText)) {
            return ""
        }
        val secretKey: SecretKey? = getAesKey(alias)
        cipher = Cipher.getInstance(AES_DEFAULT_TRANSFORMATION)
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        var index: Int = 0
        val base64IV: String? = CryptoUtils.encodeWithBase64(cipher.getIV())
        val retBuilder: StringBuilder = StringBuilder(base64IV)
        val plainTextBuilder: StringBuilder = StringBuilder(plainText)
        while (index < plainTextBuilder.length) {
            var end: Int = index + 4 * 1024
            if (end >= plainTextBuilder.length) {
                end = plainTextBuilder.length
                val str: String = plainTextBuilder.substring(index, end)
                val encryptedBytes: ByteArray =
                    cipher.doFinal(str.toByteArray(StandardCharsets.UTF_8))
                val base64Cipher: String? = CryptoUtils.encodeWithBase64(encryptedBytes)
                retBuilder.append(DELIMITER).append(base64Cipher)
                break
            }
            val str: String = plainTextBuilder.substring(index, end)
            val encryptedBytes: ByteArray = cipher.update(str.toByteArray(StandardCharsets.UTF_8))
            val base64Cipher: String? = CryptoUtils.encodeWithBase64(encryptedBytes)
            retBuilder.append(DELIMITER).append(base64Cipher)
            index = end
        }
        return retBuilder.toString()
    }

    @Throws(GeneralSecurityException::class)
    fun decryptWithSecureAes(cipherText: String?): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) decryptWithSecureAes(
            KEY_ALIAS_DEFAULT, cipherText
        ) else (decryptWithFastAes(cipherText))!!
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Throws(GeneralSecurityException::class)
    fun decryptWithSecureAes(alias: String, cipherText: String?): String {
        if (TextUtils.isEmpty(cipherText)) {
            return ""
        }
        val cipher: Cipher
        val inputs: Array<String> = cipherText!!.split(DELIMITER).toTypedArray()
        if (inputs.size < 2) {
            throw GeneralSecurityException("Input data seems to be corrupt.")
        }
        val iv: ByteArray? = CryptoUtils.decodeWithBase64(inputs.get(0))
        throwExceptionIfBytesNull(iv, "Invalid IV")
        val secretKey: SecretKey? = getAesKey(alias)
        cipher = Cipher.getInstance(AES_DEFAULT_TRANSFORMATION)
        cipher.init(Cipher.DECRYPT_MODE, secretKey, IvParameterSpec(iv))
        val stringBuilder: StringBuilder = StringBuilder()
        if (inputs.size == 2) {
            val cipherBytes: ByteArray? = CryptoUtils.decodeWithBase64(inputs.get(1))
            throwExceptionIfBytesNull(cipherBytes, "Invalid cipher bytes")
            val decryptedBytes: ByteArray = cipher.doFinal(cipherBytes)
            throwExceptionIfBytesNull(decryptedBytes, "AES decryption failed")
            stringBuilder.append(StringUtils.fromBytes(decryptedBytes))
        } else {
            for (i in 1 until inputs.size - 1) {
                val cipherBytes: ByteArray? = CryptoUtils.decodeWithBase64(inputs.get(i))
                throwExceptionIfBytesNull(cipherBytes, "Invalid cipher bytes")
                val decryptedBytes: ByteArray = cipher.update(cipherBytes)
                throwExceptionIfBytesNull(decryptedBytes, "AES decryption failed")
                stringBuilder.append(StringUtils.fromBytes(decryptedBytes))
            }
            val cipherBytes: ByteArray? = CryptoUtils.decodeWithBase64(inputs.get(inputs.size - 1))
            throwExceptionIfBytesNull(cipherBytes, "Invalid cipher bytes")
            val decryptedBytes: ByteArray = cipher.doFinal(cipherBytes)
            throwExceptionIfBytesNull(decryptedBytes, "AES decryption failed")
            stringBuilder.append(StringUtils.fromBytes(decryptedBytes))
        }
        return stringBuilder.toString()
    }

    @Throws(GeneralSecurityException::class)
    fun encryptWithFastAes(plainText: String): String? {
        loadFastAesKey()
        return CryptoUtils.encodeWithBase64(
            CryptoUtils.encryptWithAes(
                CryptoUtils.generateAesKey(
                    fastAesKey!!
                ), fastAesIv!!.toByteArray(), plainText.toByteArray()
            )
        )
    }

    @Throws(GeneralSecurityException::class)
    fun decryptWithFastAes(cipherText: String?): String? {
        loadFastAesKey()
        return StringUtils.fromBytes(
            CryptoUtils.decryptWithAes(
                CryptoUtils.generateAesKey(
                    fastAesKey!!
                ), fastAesIv!!.toByteArray(), CryptoUtils.decodeWithBase64(cipherText)
            )
        )
    }

    private fun getAesKey(alias: String): SecretKey? {
        var alias: String? = alias
        var retKey: SecretKey?
        if (TextUtils.isEmpty(alias)) {
            alias = KEY_ALIAS_DEFAULT
        }
        retKey = cachedAesKeys.get(alias)
        if (retKey != null) {
            return retKey
        }
        initKeyStore()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                if (keyStore!!.containsAlias(alias)) {
                    val key: Key = keyStore!!.getKey(alias, null)
                    if (key is SecretKey) {
                        retKey = key
                    } else {
                        keyStore!!.deleteEntry(alias)
                    }
                }
                if (retKey == null) {
                    val keyGenerator: KeyGenerator = KeyGenerator.getInstance(
                        KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE_PROVIDER
                    )
                    val builder: KeyGenParameterSpec.Builder = KeyGenParameterSpec.Builder(
                        (alias)!!, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                    )
                    builder.setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setKeySize(256)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    keyGenerator.init(builder.build())
                    retKey = keyGenerator.generateKey()
                }
            } catch (e: NoSuchAlgorithmException) {
                throw RuntimeException("Failed to retrieve AES key in Keystore", e)
            } catch (e: NoSuchProviderException) {
                throw RuntimeException("Failed to retrieve AES key in Keystore", e)
            } catch (e: InvalidAlgorithmParameterException) {
                throw RuntimeException("Failed to retrieve AES key in Keystore", e)
            } catch (e: KeyStoreException) {
                throw RuntimeException("Failed to retrieve AES key in Keystore", e)
            } catch (e: UnrecoverableKeyException) {
                throw RuntimeException("Failed to retrieve AES key in Keystore", e)
            }
        }
        cachedAesKeys.put((alias)!!, (retKey)!!)
        return retKey
    }

    private fun loadFastAesKey() {
        //Step 1 - try to get key from memory
        if (!TextUtils.isEmpty(fastAesKey) && !TextUtils.isEmpty(fastAesIv)) {
            return
        }
        if (spUtils == null) {
            spUtils = SpUtils(AppContext.getApp(), KEYSTORE_SP_NAME)
        }
        //Step 2 - try to get key from SP
        var encryptedAesKey: String? = spUtils!!.getString(FAST_AES_KEY)
        var encryptedAesIv: String? = spUtils!!.getString(FAST_AES_IV)
        var retryCounter: Int = spUtils!!.getInt(ERROR_RETRY_COUNTER)
        if (!TextUtils.isEmpty(encryptedAesKey) && !TextUtils.isEmpty(encryptedAesIv)) {
            try {
                fastAesKey = StringUtils.fromBytes(
                    decryptWithRSA(
                        KEY_ALIAS_FAST,
                        CryptoUtils.decodeWithBase64(encryptedAesKey)
                    )
                )
                fastAesIv = StringUtils.fromBytes(
                    decryptWithRSA(
                        KEY_ALIAS_FAST,
                        CryptoUtils.decodeWithBase64(encryptedAesIv)
                    )
                )
                return
            } catch (e: GeneralSecurityException) {
                e.printStackTrace()
                retryCounter++
                if (retryCounter >= MAX_RETRY_TIMES) {
                    spUtils!!.clear()
                } else {
                    spUtils!!.putInt(ERROR_RETRY_COUNTER, retryCounter)
                    throw RuntimeException("Failed to load fast AES key", e)
                }
            }
        }
        //Step 3 - try to generate a new one
        fastAesKey = CryptoUtils.getRandomStr(32)
        fastAesIv = CryptoUtils.getRandomStr(16)
        try {
            encryptedAesKey = encryptWithRSA(KEY_ALIAS_FAST, fastAesKey!!.toByteArray())
            encryptedAesIv = encryptWithRSA(KEY_ALIAS_FAST, fastAesIv!!.toByteArray())
            spUtils!!.putString(FAST_AES_KEY, encryptedAesKey)
            spUtils!!.putString(FAST_AES_IV, encryptedAesIv)
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
            throw RuntimeException("Failed to encrypt fast AES key", e)
        }
    }

    @SuppressLint("NewApi")
    @Synchronized
    private fun getRasKey(alias: String): KeyStore.PrivateKeyEntry {
        var alias: String = alias
        var retKey: KeyStore.PrivateKeyEntry?
        if (TextUtils.isEmpty(alias)) {
            alias = KEY_ALIAS_DEFAULT
        }
        retKey = cachedRsaKeys.get(alias)
        if (retKey != null) {
            return retKey
        }
        initKeyStore()
        try {
            if (keyStore!!.containsAlias(alias)) {
                val privateKey: Key = keyStore!!.getKey(alias, null)
                val certificate: Certificate? = keyStore!!.getCertificate(alias)
                if (privateKey is PrivateKey && certificate != null) {
                    retKey =
                        KeyStore.PrivateKeyEntry(privateKey as PrivateKey?, arrayOf(certificate))
                } else {
                    keyStore!!.deleteEntry(alias)
                }
            }
            if (retKey == null) {
                val start: Calendar = GregorianCalendar()
                val end: Calendar = GregorianCalendar()
                end.add(Calendar.YEAR, 30)
                val spec: AlgorithmParameterSpec = KeyPairGeneratorSpec.Builder(AppContext.getApp())
                    .setAlias(alias)
                    .setSubject(X500Principal("CN=" + alias))
                    .setSerialNumber(BigInteger.valueOf(Math.abs(alias.hashCode()).toLong()))
                    .setStartDate(start.getTime())
                    .setEndDate(end.getTime())
                    .setKeySize(2048)
                    .build()
                val kpGenerator: KeyPairGenerator = KeyPairGenerator.getInstance(
                    KEY_ALGORITHM_RSA, ANDROID_KEYSTORE_PROVIDER
                )
                kpGenerator.initialize(spec)
                kpGenerator.generateKeyPair()
            }
            val privateKey: Key = keyStore!!.getKey(alias, null)
            val certificate: Certificate? = keyStore!!.getCertificate(alias)
            if (privateKey is PrivateKey && certificate != null) {
                retKey = KeyStore.PrivateKeyEntry(privateKey as PrivateKey?, arrayOf(certificate))
            } else {
                throw RuntimeException("Failed to retrieve AES key in Keystore")
            }
        } catch (e: GeneralSecurityException) {
            throw RuntimeException("Failed to retrieve AES key in Keystore", e)
        }
        return retKey
    }

    @Synchronized
    @Throws(GeneralSecurityException::class)
    private fun encryptWithRSA(alias: String, plainText: ByteArray?): String? {
        if (plainText == null || plainText.size == 0) {
            return null
        }
        val cipher: Cipher = Cipher.getInstance(RSA_DEFAULT_TRANSFORMATION)
        val publicKey: PublicKey = getRasKey(alias).getCertificate().getPublicKey()
        cipher.init(Cipher.ENCRYPT_MODE, publicKey)
        return CryptoUtils.encodeWithBase64(cipher.doFinal(plainText))
    }

    @Synchronized
    @Throws(GeneralSecurityException::class)
    private fun decryptWithRSA(alias: String, cipherText: ByteArray?): ByteArray? {
        if (cipherText == null || cipherText.size == 0) {
            return null
        }
        val cipher: Cipher = Cipher.getInstance(RSA_DEFAULT_TRANSFORMATION)
        val privateKey: PrivateKey = getRasKey(alias).getPrivateKey()
        cipher.init(Cipher.DECRYPT_MODE, privateKey)
        return cipher.doFinal(cipherText)
    }

    @Throws(GeneralSecurityException::class)
    private fun throwExceptionIfBytesNull(bytes: ByteArray?, errorMsg: String) {
        if (bytes == null || bytes.size == 0) {
            throw GeneralSecurityException(errorMsg)
        }
    }

    companion object {
        val KEY_ALIAS_DEFAULT: String = "NioAesKeyAlias"
        val KEY_ALIAS_FAST: String = "NioKeyAliasFast"
        val KEY_ALGORITHM_RSA: String = "RSA"
        val ANDROID_KEYSTORE_PROVIDER: String = "AndroidKeyStore"
        private val DELIMITER: String = "]"
        private val AES_DEFAULT_TRANSFORMATION: String = "AES/CBC/PKCS7Padding"
        private val RSA_DEFAULT_TRANSFORMATION: String = "RSA/ECB/PKCS1Padding"
        private val KEYSTORE_SP_NAME: String = "keystore_utils"
        private val FAST_AES_IV: String = "FAST_AES_IV"
        private val FAST_AES_KEY: String = "FAST_AES_KEY"
        private val ERROR_RETRY_COUNTER: String = "ERROR_RETRY_COUNTER"
        private val MAX_RETRY_TIMES: Int = 3
        private val cachedAesKeys: ConcurrentHashMap<String?, SecretKey?> =
            ConcurrentHashMap() //Improve AES performance
        private val cachedRsaKeys: ConcurrentHashMap<String, KeyStore.PrivateKeyEntry> =
            ConcurrentHashMap() //Improve RSA performance

        @kotlin.jvm.JvmStatic
        fun get(): KeyStoreUtils {
            return KeyStoreUtilsHolder.instance
        }
    }
}