package com.letsfun.letswidget.lib.core.utils

import android.annotation.SuppressLint
import android.text.TextUtils
import android.util.Base64
import android.util.LruCache
import java.io.*
import java.nio.charset.StandardCharsets
import java.security.*
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.util.*
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PSource
import javax.crypto.spec.SecretKeySpec
import kotlin.math.abs

@SuppressLint("NewApi")
object CryptoUtils {
    private const val RANDOM_SEED: String =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    private const val ALGORITHM_MD5: String = "MD5"
    private const val ALGORITHM_SHA1: String = "SHA-1"
    private const val ALGORITHM_SHA256: String = "SHA-256"
    private const val ALGORITHM_SHA512: String = "SHA-512"
    private const val ALGORITHM_AES: String = "AES"
    private const val ALGORITHM_RSA: String = "RSA"
    private const val ALGORITHM_RSA_SHA256: String = "SHA256withRSA"
    private const val ALGORITHM_RSA_ECB: String = "RSA/ECB/PKCS1Padding"
    private const val ALGORITHM_RSA_OAEP: String = "RSA/ECB/OAEPPadding" //更安全

    private const val AES_MODE: String = "AES/CBC/PKCS7Padding"
    private const val CHARSET: String = "UTF-8"

    //AESCrypt-ObjC uses blank IV (not the best security, but the aim here is compatibility)
    private val ivBytes: ByteArray = byteArrayOf(
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00
    )
    val PP: PSource.PSpecified = PSource.PSpecified(ByteArray(0))
    private val md5Cache: LruCache<String?, String?> = LruCache(100)

    /***************Random & Hash functions */
    val random: Random = SecureRandom()

    /**********************Encoder/Decoder functions */
    fun decodeWithBase64(input: String?): ByteArray? {
        var ret: ByteArray? = null
        try {
            ret = Base64.decode(input, Base64.NO_WRAP)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ret
    }

    fun encodeWithBase64(input: ByteArray?): String {
        return Base64.encodeToString(input, Base64.NO_WRAP)
    }

    @kotlin.jvm.JvmStatic
    fun getRandomNumStr(len: Int): String {
        var len: Int = len
        if (len > 16) {
            len = 16
        } else if (len < 1) {
            len = 1
        }
        return random.nextLong().toString().substring(0, len)
    }

    @kotlin.jvm.JvmStatic
    fun getRandomStr(len: Int): String {
        val builder: StringBuilder = StringBuilder(len)
        for (i in 0 until len) {
            val num: Int = random.nextInt(62)
            builder.append(RANDOM_SEED.get(num).toString())
        }
        return builder.toString()
    }

    /**
     * 融合时间的随机字符串，不在同一毫秒生成不会重复，同一毫秒内重复的概率为1/62^25
     *
     * @return 32位随机字符串，只有数字和字母，不包含字符
     */
    fun generateGuid(): String {
        val timestamp: Long = abs(System.currentTimeMillis() - 1609430400000L) //2021年1月1日0点之后时间戳
        val base: Int = 62
        val result: CharArray = CharArray(32)
        var index: Int = 0
        var remainder: Int
        var left: Long = timestamp
        val random: Random = random
        while (index < 32) {
            remainder = (left % base).toInt()
            left = left / base
            result[index++] = RANDOM_SEED.get(random.nextInt(base))
            result[index++] = RANDOM_SEED.get(random.nextInt(base))
            result[index++] = RANDOM_SEED.get(random.nextInt(base))
            if (index < 30) { //时间戳最多占用7个字符
                result[index++] = RANDOM_SEED.get(remainder)
            } else {
                result[index++] = RANDOM_SEED.get(random.nextInt(base))
            }
        }
        return String(result)
    }

    @Synchronized
    fun md5(input: String?): String {
        if (TextUtils.isEmpty(input)) {
            return ""
        }
        var result: String? = md5Cache.get(input)
        if (!TextUtils.isEmpty(result)) {
            return result!!
        }
        result = StringUtils.toHexString(doHash(ALGORITHM_MD5, input!!.toByteArray()))
        if (!TextUtils.isEmpty(result)) {
            md5Cache.put(input, result)
        }
        return result
    }

    @Synchronized
    fun md5(input: ByteArray?): String? {
        if (input == null || input.isEmpty()) {
            return ""
        }
        var result: String? = md5Cache.get(StringUtils.toHexString(input))
        if (!TextUtils.isEmpty(result)) {
            return result
        }
        result = StringUtils.toHexString(doHash(ALGORITHM_MD5, input))
        md5Cache.put(StringUtils.toHexString(input), result)
        return result
    }

    fun md5(inputStream: InputStream?): String? {
        if (inputStream == null) {
            return null
        }
        val digest: MessageDigest
        val buffer: ByteArray = ByteArray(1024)
        var len: Int = 0
        try {
            digest = MessageDigest.getInstance(ALGORITHM_MD5)
            while ((inputStream.read(buffer, 0, 1024).also { len = it }) != -1) {
                digest.update(buffer, 0, len)
            }
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
            return ""
        } catch (e: IOException) {
            e.printStackTrace()
            return ""
        }
        return StringUtils.toHexString(digest.digest())
    }

    fun md5(file: File): String? {
        if (!file.isFile()) {
            return ""
        }
        try {
            val inputStream: FileInputStream = FileInputStream(file)
            return md5(inputStream)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            return ""
        }
    }

    /**
     * 返回一个8字符哈希值
     */
    fun shortHash(input: String): String {
        return md5("shortHash$input").substring(0, 8)
    }

    fun sha256(byteArray: ByteArray): String? {
        return StringUtils.toHexString(doHash(ALGORITHM_SHA256, byteArray))
    }

    fun sha256(input: String): String {
        return StringUtils.toHexString(doHash(ALGORITHM_SHA256, input.toByteArray()))
    }


    fun sha512(byteArray: ByteArray): String? {
        return StringUtils.toHexString(doHash(ALGORITHM_SHA512, byteArray))
    }

    fun sha512(input: String): String? {
        return StringUtils.toHexString(doHash(ALGORITHM_SHA512, input.toByteArray()))
    }

    fun sha256(inputStream: InputStream?): String? {
        if (inputStream == null) {
            return null
        }
        val digest: MessageDigest
        val buffer: ByteArray = ByteArray(1024)
        var len: Int = 0
        try {
            digest = MessageDigest.getInstance(ALGORITHM_SHA256)
            while ((inputStream.read(buffer, 0, 1024).also { len = it }) != -1) {
                digest.update(buffer, 0, len)
            }
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
            return ""
        } catch (e: IOException) {
            e.printStackTrace()
            return ""
        }
        return StringUtils.toHexString(digest.digest())
    }

    /********************** symmetric encryption  */
    fun generateAesKey128(): SecretKey {//用这种方式兼容性更好，支持Android 6
        return SecretKeySpec(getRandomStr(16).toByteArray(), ALGORITHM_AES)
    }

    fun generateAesKey(password: String): SecretKeySpec {
        return SecretKeySpec(doHash(ALGORITHM_SHA256, password.toByteArray()), ALGORITHM_AES)
    }

    @Throws(GeneralSecurityException::class)
    fun encryptWithAes(password: String, message: String): String {
        if (TextUtils.isEmpty(message)) {
            throw GeneralSecurityException("Invalid input data")
        }
        val key: SecretKeySpec = generateAesKey(password)
        val cipherText: ByteArray = encryptWithAes(
            key, ivBytes, message.toByteArray(
                StandardCharsets.UTF_8
            )
        )
        return encodeWithBase64(cipherText)
    }

    @Throws(GeneralSecurityException::class)
    fun encryptWithAes(message: ByteArray, key: ByteArray, iv: ByteArray): ByteArray? {
        val keySpec = SecretKeySpec(key, ALGORITHM_AES)
        return encryptWithAes(keySpec, iv, message)
    }

    @Throws(GeneralSecurityException::class)
    fun encryptWithAes(key: SecretKeySpec, iv: ByteArray, message: ByteArray): ByteArray {
        val cipher: Cipher = Cipher.getInstance(AES_MODE)
        val ivSpec = IvParameterSpec(iv)
        cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec)
        return cipher.doFinal(message)
    }

    @Throws(GeneralSecurityException::class, IOException::class)
    fun encryptWithAes(
        key: SecretKeySpec,
        iv: ByteArray,
        input: InputStream,
        output: OutputStream
    ) {
        val cipher: Cipher = Cipher.getInstance(AES_MODE)
        val ivSpec: IvParameterSpec = IvParameterSpec(iv)
        cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec)
        val buffer: ByteArray = ByteArray(1024)
        var len: Int = 0
        while (true) {
            if (!((input.read(buffer, 0, 1024).also { len = it }) == 1024)) {
                output.write(cipher.update(buffer, 0, len))
            } else {
                output.write(cipher.doFinal(buffer, 0, len))
                break
            }
        }
    }

    @Throws(GeneralSecurityException::class)
    fun decryptWithAes(decodedCipherText: ByteArray, key: ByteArray, iv: ByteArray): ByteArray {
        val keySpec = SecretKeySpec(key, ALGORITHM_AES)
        return decryptWithAes(keySpec, iv, decodedCipherText)
    }

    @Throws(GeneralSecurityException::class)
    fun decryptWithAes(password: String, base64EncodedCipherText: String): String {
        val key: SecretKeySpec = generateAesKey(password)
        val decodedCipherText: ByteArray? = decodeWithBase64(base64EncodedCipherText)
        if (decodedCipherText == null || decodedCipherText.isEmpty()) {
            throw GeneralSecurityException("Invalid cipher text")
        }
        val decryptedBytes: ByteArray = decryptWithAes(key, ivBytes, decodedCipherText)
        return StringUtils.fromBytes(decryptedBytes)
    }

    @Throws(GeneralSecurityException::class)
    fun decryptWithAes(
        key: SecretKeySpec?,
        iv: ByteArray?,
        decodedCipherText: ByteArray?
    ): ByteArray {
        if (decodedCipherText == null || decodedCipherText.isEmpty()) {
            throw GeneralSecurityException("Invalid cipher text")
        }
        val cipher: Cipher = Cipher.getInstance(AES_MODE)
        val ivSpec = IvParameterSpec(iv)
        cipher.init(Cipher.DECRYPT_MODE, key, ivSpec)
        return cipher.doFinal(decodedCipherText)
    }

    @Throws(GeneralSecurityException::class, IOException::class)
    fun decryptWithAes(
        key: SecretKeySpec?,
        iv: ByteArray?,
        input: InputStream,
        output: OutputStream
    ) {
        val cipher: Cipher = Cipher.getInstance(AES_MODE)
        val ivSpec: IvParameterSpec = IvParameterSpec(iv)
        cipher.init(Cipher.DECRYPT_MODE, key, ivSpec)
        val buffer: ByteArray = ByteArray(1024)
        var len: Int = 0
        while (true) {
            if (!((input.read(buffer, 0, 1024).also { len = it }) == 1024)) {
                output.write(cipher.update(buffer, 0, len))
            } else {
                output.write(cipher.doFinal(buffer, 0, len))
                break
            }
        }
    }

    /********************** asymmetric encryption  *****************************/
    /**
     * 推荐使用2048 OAEP方式加密
     */
    fun encryptWithRsa(data: ByteArray, rsaPublicKey: RSAPublicKey): ByteArray {
        val c = Cipher.getInstance(ALGORITHM_RSA_ECB)
        c.init(Cipher.ENCRYPT_MODE, rsaPublicKey)
        return c.doFinal(data)
    }

    fun decryptWithRsa(s: String?, rsaPrivateKey: RSAPrivateKey): ByteArray? {
        if (TextUtils.isEmpty(s)) {
            return null
        }
        val data = Base64.decode(s, Base64.NO_WRAP)
        return decryptWithRsa(data, rsaPrivateKey)
    }

    fun encryptWithRsaOaep(data: ByteArray, rsaPublicKey: RSAPublicKey): ByteArray {
        val c = Cipher.getInstance(ALGORITHM_RSA_OAEP)
        c.init(Cipher.ENCRYPT_MODE, rsaPublicKey)
        return c.doFinal(data)
    }

    fun decryptWithRsaOaep(s: String?, rsaPrivateKey: RSAPrivateKey): ByteArray? {
        if (TextUtils.isEmpty(s)) {
            return null
        }
        val data = Base64.decode(s, Base64.NO_WRAP)
        return decryptWithRsaOaep(data, rsaPrivateKey)
    }

    fun decryptWithRsa(data: ByteArray, rsaPrivateKey: RSAPrivateKey): ByteArray {
        val cipher = Cipher.getInstance(ALGORITHM_RSA_ECB)
        cipher.init(Cipher.DECRYPT_MODE, rsaPrivateKey)
        val outputStream = ByteArrayOutputStream()
        val inputLength = data.size
        var offset = 0
        var cache: ByteArray?
        var i = 0
        val len = 256 //缓存问题，缓存了旧的钥匙加密数据
        while (inputLength - offset > 0) {
            cache = if (inputLength - offset > len) {
                cipher.doFinal(data, offset, len)
            } else {
                cipher.doFinal(data, offset, inputLength - offset)
            }
            outputStream.write(cache)
            i++
            offset = i * len
        }
        val bytes = outputStream.toByteArray()
        IoUtils.closeQuietly(outputStream)
        return bytes
    }

    fun decryptWithRsaOaep(data: ByteArray, rsaPrivateKey: RSAPrivateKey): ByteArray {
        val cipher = Cipher.getInstance(ALGORITHM_RSA_OAEP)
        cipher.init(Cipher.DECRYPT_MODE, rsaPrivateKey)
        val outputStream = ByteArrayOutputStream()
        val inputLength = data.size
        var offset = 0
        var cache: ByteArray?
        var i = 0
        val len = 256 //缓存问题，缓存了旧的钥匙加密数据
        while (inputLength - offset > 0) {
            cache = if (inputLength - offset > len) {
                cipher.doFinal(data, offset, len)
            } else {
                cipher.doFinal(data, offset, inputLength - offset)
            }
            outputStream.write(cache)
            i++
            offset = i * len
        }
        val bytes = outputStream.toByteArray()
        IoUtils.closeQuietly(outputStream)
        return bytes
    }

    fun signWithRsa(data: ByteArray, rsaPrivateKey: RSAPrivateKey): ByteArray {
        val signature = Signature.getInstance(ALGORITHM_RSA_SHA256)
        signature.initSign(rsaPrivateKey)
        signature.update(data)
        return signature.sign()
    }

    fun decodePublicKeyFromCert(encodedCert: String): RSAPublicKey {
        val cf = CertificateFactory.getInstance("X.509")
        val stream: InputStream = ByteArrayInputStream(encodedCert.trimIndent().toByteArray())
        val certificate = cf.generateCertificate(stream) as X509Certificate
        IoUtils.closeQuietly(stream)
        return certificate.publicKey as RSAPublicKey
    }

    fun decodePrivateKeyFromPem(encodedPrivateKey: String): ByteArray {
        var privateKey = encodedPrivateKey.replace("-----BEGIN PRIVATE KEY-----", "")
        privateKey = privateKey.replace("-----END PRIVATE KEY-----", "")
        privateKey = privateKey.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        privateKey = privateKey.replace("-----END RSA PRIVATE KEY-----", "")
        privateKey = privateKey.replace("\\s+".toRegex(), "")
        return Base64.decode(privateKey, Base64.DEFAULT)
    }

    fun generateRsaPrivateKey(encodedPrivateKey: ByteArray): RSAPrivateKey {
        val keySpec = PKCS8EncodedKeySpec(encodedPrivateKey)
        val kf = KeyFactory.getInstance(ALGORITHM_RSA)
        return kf.generatePrivate(keySpec) as RSAPrivateKey
    }

    fun getRsaPrivateKey(privateKey: String): RSAPrivateKey? {
        if (!TextUtils.isEmpty(privateKey)) {
            val beginDelimiter = "-----BEGIN RSA PRIVATE KEY-----"
            val endDelimiter = "-----END RSA PRIVATE KEY-----"
            var tokens = privateKey.split(beginDelimiter.toRegex()).toTypedArray()
            if (tokens.size >= 2) {
                tokens = tokens[1].split(endDelimiter.toRegex()).toTypedArray()
                val keyBytes = Base64.decode(tokens[0].trim { it <= ' ' }
                    .replace("\r", "").replace("\n", ""), 2)
                val spec = PKCS8EncodedKeySpec(keyBytes)
                val factory = KeyFactory.getInstance(ALGORITHM_RSA)
                return factory.generatePrivate(spec) as RSAPrivateKey
            }
        }
        return null
    }

    /**
     * 证书编号为大整数，不要在前面补0
     */
    @Throws(
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        IOException::class
    )
    fun getCertSerialNumber(cert: String): String? {
        try {
            val x509Certificates: List<X509Certificate> = getX509Certificates(cert)
            val x509Certificate = if (x509Certificates.isNotEmpty()) {
                x509Certificates[0]
            } else {
                return null
            }
            return x509Certificate.serialNumber.toString(16)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    @Throws(CertificateException::class)
    fun getX509Certificates(cert: String): List<X509Certificate> {
        val beginDelimiter = "-----BEGIN CERTIFICATE-----"
        val endDelimiter = "-----END CERTIFICATE-----"
        val x509Certificates: MutableList<X509Certificate> = ArrayList()
        val tokensFirst = cert.split(beginDelimiter.toRegex()).toTypedArray()
        for (token in tokensFirst) {
            if ((token.trim { it <= ' ' }).isNotEmpty()) {
                val tokensSecond = token.split(endDelimiter.toRegex()).toTypedArray()
                val certBytes = Base64.decode(tokensSecond[0].trim { it <= ' ' }
                    .replace("\r", "").replace("\n", ""), Base64.NO_WRAP)
                val factory = CertificateFactory.getInstance("X.509")
                x509Certificates.add(
                    factory.generateCertificate(ByteArrayInputStream(certBytes)) as X509Certificate
                )
            }
        }
        return x509Certificates
    }

    /*****************private methods */
    private fun doHash(algorithm: String, input: ByteArray): ByteArray? {
        return try {
            val messageDigest: MessageDigest = MessageDigest.getInstance(algorithm)
            messageDigest.digest(input)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
            null
        }
    }
}