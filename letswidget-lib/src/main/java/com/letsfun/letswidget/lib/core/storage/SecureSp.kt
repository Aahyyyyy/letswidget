package com.letsfun.letswidget.lib.core.storage

import android.text.TextUtils
import com.letsfun.letswidget.lib.core.AppContext

object SecureSp {
    operator fun get(@SspType sspType: String?, sspName: String?): BaseSsp {
        var sspType: String? = sspType
        if (TextUtils.isEmpty(sspType)) {
            sspType = SspType.ANDROID_SSP
        }
        when (sspType) {
            SspType.DUMMY_SSP -> return DummySsp(AppContext.getApp())
            SspType.ANDROID_SSP -> return AndroidSsp(AppContext.getApp(), sspName)
            else -> return AndroidSsp(AppContext.getApp(), sspName)
        }
    }
}