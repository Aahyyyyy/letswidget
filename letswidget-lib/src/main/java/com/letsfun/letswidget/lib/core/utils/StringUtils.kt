package com.letsfun.letswidget.lib.core.utils

import android.net.Uri
import android.text.TextUtils
import java.text.DecimalFormat
import java.util.regex.Matcher
import java.util.regex.Pattern

object StringUtils {
    private const val VALUE_NULL: String = "null"

    @Deprecated("Use kotlin method")
    fun isEmptyWithNullChecking(content: String?): Boolean {
        return TextUtils.isEmpty(content) || (VALUE_NULL.equals(content, ignoreCase = true))
    }

    fun isEmail(email: String?): Boolean {
        if (TextUtils.isEmpty(email)) return false
        val str =
            "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,63}|[0-9]{1,63})(\\]?)$"
        val p: Pattern = Pattern.compile(str)
        val m: Matcher = p.matcher(email!!)
        return m.matches()
    }

    fun isIp(ip: String?): Boolean {
        if (TextUtils.isEmpty(ip)) return false
        val num = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)"
        val regex = "^$num\\.$num\\.$num\\.$num$"
        val p: Pattern = Pattern.compile(regex)
        val m: Matcher = p.matcher(ip!!)
        return m.matches()
    }

    fun isTelephone(phone: String?): Boolean {
        if (TextUtils.isEmpty(phone)) return false
        val regex = "^(\\d{3,4}-)?\\d{6,8}$"
        val p: Pattern = Pattern.compile(regex)
        val m: Matcher = p.matcher(phone!!)
        return m.matches()
    }

    fun isPostalCode(code: String?): Boolean {
        if (TextUtils.isEmpty(code)) return false
        val regex = "^\\d{6}$"
        val p: Pattern = Pattern.compile(regex)
        val m: Matcher = p.matcher(code!!)
        return m.matches()
    }

    fun blurPhoneNumber(phone: String): String {
        if (!isPhoneNumber(phone)) {
            return phone
        }
        return phone.substring(0, 3) + "****" + phone.substring(phone.length - 4)
    }

    fun isPhoneNumber(phone: String?): Boolean {
        if (TextUtils.isEmpty(phone)) return false
        val regex = "^[1-9][0-9]\\d{9}$"
        val p: Pattern = Pattern.compile(regex)
        val m: Matcher = p.matcher(phone!!)
        return m.matches()
    }

    fun isIdCard(id: String): Boolean {
        if (TextUtils.isEmpty(id)) return false
        val regex = "^(\\d{6})(\\d{4})(\\d{2})(\\d{2})(\\d{3})([0-9]|[Xx])$"
        val p: Pattern = Pattern.compile(regex)
        val m: Matcher = p.matcher(id)
        return m.matches()
    }

    fun isLetterDigit(str: String): Boolean {
        val regex: String = "^[a-z0-9A-Z]+$"
        return str.matches(Regex(regex))
    }

    @kotlin.jvm.JvmStatic
    fun isFilePath(path: String?): Boolean {
        if (TextUtils.isEmpty(path)) {
            return false
        }
        val pattern: String = "^/?([\\w-]+/?)+$"
        val p: Pattern = Pattern.compile(pattern)
        val matcher: Matcher = p.matcher(path!!)
        return matcher.matches()
    }

    @kotlin.jvm.JvmStatic
    fun isUrl(url: String?): Boolean {
        if (TextUtils.isEmpty(url)) {
            return false
        }
        val pattern: String = "[a-zA-z]+://[^\\s]*"
        val p: Pattern = Pattern.compile(pattern)
        val matcher: Matcher = p.matcher(url!!)
        return matcher.matches()
    }

    @kotlin.jvm.JvmStatic
    fun isHtml(`val`: String): Boolean {
        if (TextUtils.isEmpty(`val`)) {
            return false
        }
        val telRegex: String = "<(\\S*?)[^>]*>.*?|<.*? />"
        return `val`.matches(Regex(telRegex))
    }

    fun concat(vararg args: CharSequence?): String {
        val stringBuilder: StringBuilder = StringBuilder()
        if (args.isNotEmpty()) {
            for (arg: CharSequence? in args) {
                if (!TextUtils.isEmpty(arg)) {
                    stringBuilder.append(arg)
                }
            }
        }
        return stringBuilder.toString()
    }

    /**
     * 将一个String列表用特定的符号联结
     *
     * @param input
     * @param divider
     * @return
     */
    fun join(input: List<String?>?, divider: String?): String {
        if (input == null) {
            return ""
        }
        return join(input.toTypedArray(), divider)
    }

    fun join(input: Array<String?>?, divider: String?): String {
        if (input == null || input.isEmpty()) {
            return ""
        }
        val stringBuilder: StringBuilder = StringBuilder()
        for (i in 0 until input.size - 1) {
            stringBuilder.append(input[i]).append(divider)
        }
        stringBuilder.append(input[input.size - 1])
        return stringBuilder.toString()
    }

    fun replace(original: String, target: String?, replacement: String?): String {
        var finalReplacement: String? = replacement
        if (TextUtils.isEmpty(original)) {
            return ""
        }
        if (TextUtils.isEmpty(target)) {
            return original
        }
        if (TextUtils.isEmpty(replacement)) {
            finalReplacement = ""
        }
        return original.replace((target)!!, (finalReplacement)!!)
    }

    fun removeEmpty(tags: MutableList<String?>?): MutableList<String?> {
        if (tags == null) {
            return ArrayList()
        }
        val list: MutableList<Int> = ArrayList()
        for (i in tags.indices) {
            if (TextUtils.isEmpty(tags[i])) {
                list.add(i)
            }
        }
        if (list.size > 0) {
            for (index in list.indices.reversed()) {
                tags.removeAt(list[index])
            }
        }
        return tags
    }

    fun compare(from: String?, to: String?): Boolean {
        if (TextUtils.isEmpty(from) && TextUtils.isEmpty(to)) {
            return true
        }
        if (TextUtils.isEmpty(from) || TextUtils.isEmpty(to)) {
            return false
        }
        return (from == to)
    }

    fun compareIgnoreCase(from: String?, to: String?): Boolean {
        if (TextUtils.isEmpty(from) && TextUtils.isEmpty(to)) {
            return true
        }
        if (TextUtils.isEmpty(from) || TextUtils.isEmpty(to)) {
            return false
        }
        return from.equals(to, ignoreCase = true)
    }

    fun getValueNonNull(input: String?): String {
        if (input == null) {
            return ""
        }
        return input
    }

    fun contains(original: String, s: String?): Boolean {
        if (TextUtils.isEmpty(original) || TextUtils.isEmpty(s)) {
            return false
        }
        return original.contains((s)!!)
    }

    fun containsEmoji(source: String): Boolean {
        val len: Int = source.length
        for (i in 0 until len) {
            val codePoint: Char = source[i]
            if (!isEmojiCharacter(codePoint)) {
                return true
            }
        }
        return false
    }

    private fun isEmojiCharacter(codePoint: Char): Boolean {
        return ((codePoint.toInt() == 0x0)
                || (codePoint.toInt() == 0x9)
                || (codePoint.toInt() == 0xA)
                || (codePoint.toInt() == 0xD)
                || ((codePoint.toInt() >= 0x20) && (codePoint.toInt() <= 0xD7FF))
                || ((codePoint.toInt() >= 0xE000) && (codePoint.toInt() <= 0xFFFD))
                || ((codePoint.toInt() >= 0x10000) && (codePoint.toInt() <= 0x10FFFF)))
    }

    fun length(str: String?): Int {
        return str?.length ?: 0
    }

    fun fromBytes(byteArr: ByteArray?): String {
        return if (byteArr == null) "" else String(byteArr)
    }

    fun toHexString(bytes: ByteArray?): String {
        if (bytes == null) {
            return ""
        }
        val result: StringBuilder = StringBuilder()
        for (b: Byte in bytes) {
            val temp: String = Integer.toHexString(b.toInt() and 0xff)
            if (temp.length == 1) {
                result.append("0")
            }
            result.append(temp)
        }
        return result.toString()
    }

    fun str2Uri(string: String?): Uri {
        return Uri.parse(string ?: "")
    }

    fun sizeToString(size: Long): String {
        val strSize: StringBuilder = StringBuilder()
        when {
            size < 1024 -> {
                strSize.append(size).append("B")
            }
            size < 1024 * 1024 -> {
                strSize.append(size / 1024).append("K")
            }
            else -> {
                val dSize: Double = size.toDouble() / 1024 / 1024
                val decimalFormat = DecimalFormat("###################.#")
                println()
                strSize.append(decimalFormat.format(dSize)).append("M")
            }
        }
        return strSize.toString()
    }
}