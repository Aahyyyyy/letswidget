package com.letsfun.letswidget.lib.core.utils

import java.io.Closeable
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

object IoUtils {
    private const val DEFAULT_BUFFER_SIZE: Int = 1024 * 4
    fun closeQuietly(closeable: Closeable?) {
        try {
            closeable?.close()
        } catch (ioe: IOException) {
            ioe.printStackTrace()
        }
    }

    @Throws(IOException::class)
    fun copyStreams(inStream: InputStream, outStream: OutputStream) {
        val buf: ByteArray = ByteArray(DEFAULT_BUFFER_SIZE)
        var size: Int
        while ((inStream.read(buf).also { size = it }) != -1) {
            outStream.write(buf, 0, size)
        }
    }
}