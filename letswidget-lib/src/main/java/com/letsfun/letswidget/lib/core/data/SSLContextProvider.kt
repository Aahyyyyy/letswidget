package com.letsfun.letswidget.lib.core.data

import javax.net.ssl.SSLContext

interface SSLContextProvider {
    fun provide(): SSLContext?
}