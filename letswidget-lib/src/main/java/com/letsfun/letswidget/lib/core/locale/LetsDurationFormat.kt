package com.letsfun.letswidget.lib.core.locale

import android.annotation.SuppressLint
import android.icu.text.MeasureFormat
import android.icu.text.NumberFormat
import android.icu.util.MeasureUnit
import android.os.Build
import java.util.Locale
import kotlin.time.*

data class LetsDurationFormat(val locale: Locale = Locale.getDefault()) {
    enum class Unit {
        DAY, HOUR, MINUTE, SECOND, MILLISECOND
    }

    fun formatAbsolute(duration:Long,unit: Unit):String{
        return "$duration${unitDisplayName(unit)}"
    }

    @SuppressLint("NewApi")
    @OptIn(ExperimentalTime::class)
    fun format(duration: Duration, smallestUnit: Unit = Unit.SECOND): String {
        var formattedList = mutableListOf<String>()
        var remainder = duration

        for (unit in Unit.values()) {
            val component = transitionComponent(unit, remainder)

            remainder = when (unit) {
                Unit.DAY -> remainder - Duration.days(component)
                Unit.HOUR -> remainder - Duration.hours(component)
                Unit.MINUTE -> remainder - Duration.minutes(component)
                Unit.SECOND -> remainder - Duration.seconds(component)
                Unit.MILLISECOND -> remainder - Duration.milliseconds(component)
            }

            val unitDisplayName = unitDisplayName(unit)

            if (component > 0) {
                val formattedComponent = NumberFormat.getInstance(locale).format(component)
                formattedList.add("$formattedComponent$unitDisplayName")
            }

            if (unit == smallestUnit) {
                val formattedZero = NumberFormat.getInstance(locale).format(0)
                if (formattedList.isEmpty()) formattedList.add("$formattedZero$unitDisplayName")
                break
            }
        }

        return formattedList.joinToString("")
    }

    @OptIn(ExperimentalTime::class)
    private fun transitionComponent(unit: Unit, remainder: Duration) = when (unit) {
        Unit.DAY -> remainder.toDouble(DurationUnit.DAYS).toLong()
        Unit.HOUR -> remainder.toDouble(DurationUnit.HOURS).toLong()
        Unit.MINUTE -> remainder.toDouble(DurationUnit.MINUTES).toLong()
        Unit.SECOND -> remainder.toDouble(DurationUnit.SECONDS).toLong()
        Unit.MILLISECOND -> remainder.toDouble(DurationUnit.MILLISECONDS).toLong()
    }

    private fun unitDisplayName(unit: Unit) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
        val measureFormat = MeasureFormat.getInstance(locale, MeasureFormat.FormatWidth.NARROW)
        when (unit) {
            Unit.DAY -> measureFormat.getUnitDisplayName(MeasureUnit.DAY)
            Unit.HOUR -> measureFormat.getUnitDisplayName(MeasureUnit.HOUR)
            Unit.MINUTE -> measureFormat.getUnitDisplayName(MeasureUnit.MINUTE)
            Unit.SECOND -> measureFormat.getUnitDisplayName(MeasureUnit.SECOND)
            Unit.MILLISECOND -> measureFormat.getUnitDisplayName(MeasureUnit.MILLISECOND)
        }
    } else {
        when (unit) {
            Unit.DAY -> "day"
            Unit.HOUR -> "hour"
            Unit.MINUTE -> "min"
            Unit.SECOND -> "sec"
            Unit.MILLISECOND -> "msec"
        }
    }
}