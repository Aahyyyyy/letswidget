package com.letsfun.letswidget.lib.core.locale

import androidx.annotation.StringDef

@StringDef(
    LetsLangCode.ZH_CN,
    LetsLangCode.EN_GB,
    LetsLangCode.NB_NO,
    LetsLangCode.SV_SE,
    LetsLangCode.DA_DK,
    LetsLangCode.NL_NL,
    LetsLangCode.DE_DE
)
@Target(
    AnnotationTarget.TYPE_PARAMETER,
    AnnotationTarget.VALUE_PARAMETER,
    AnnotationTarget.FIELD,
    AnnotationTarget.FUNCTION
)
@Retention(AnnotationRetention.SOURCE)
annotation class LetsLangCode {
    companion object {
        const val ZH_CN = "zh-CN"
        const val EN_GB = "en-GB"
        const val NB_NO = "nb-NO"
        const val SV_SE = "sv-SE"
        const val DA_DK = "da-DK"
        const val NL_NL = "nl-NL"
        const val DE_DE = "de-DE"
    }
}
