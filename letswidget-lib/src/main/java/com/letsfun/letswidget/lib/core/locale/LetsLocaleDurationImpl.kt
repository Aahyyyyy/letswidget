package com.letsfun.letswidget.lib.core.locale

import java.util.*
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

object LetsLocaleDurationImpl {

    @OptIn(ExperimentalTime::class)
    fun localizedDurationTime(
        timeMillis: Long,
        locale: Locale,
        @LetsDurationToken format: String = LetsDurationToken.TIME_DURATION_SPLICE,
        smallestUnit: LetsDurationFormat.Unit = LetsDurationFormat.Unit.SECOND
    ): String? {
        return when (format) {
            LetsDurationToken.TIME_DURATION_DDD -> {
                LetsDurationFormat(locale).formatAbsolute(
                    timeMillis / ONE_DAY,
                    LetsDurationFormat.Unit.DAY,
                )
            }
            LetsDurationToken.TIME_DURATION_HH -> {
                LetsDurationFormat(locale).formatAbsolute(
                    timeMillis / ONE_HOUR,
                    LetsDurationFormat.Unit.HOUR,
                )
            }
            LetsDurationToken.TIME_DURATION_MM -> {
                LetsDurationFormat(locale).formatAbsolute(
                    timeMillis / ONE_MINUTE,
                    LetsDurationFormat.Unit.MINUTE,
                )
            }
            LetsDurationToken.TIME_DURATION_SS -> {
                LetsDurationFormat(locale).formatAbsolute(
                    timeMillis / 1000,
                    LetsDurationFormat.Unit.SECOND
                )
            }
            LetsDurationToken.TIME_DURATION_HHHMMSS->{
                val hour = timeMillis / ONE_HOUR
                val minute = timeMillis % ONE_HOUR / ONE_MINUTE
                val seconds = timeMillis % ONE_MINUTE
                "${generateNo(hour)}:${generateNo(minute)}:${generateNo(seconds)}"
            }else -> {
                val day = timeMillis / ONE_DAY
                val hour = timeMillis % ONE_DAY / ONE_HOUR
                val minute = timeMillis % ONE_HOUR / ONE_MINUTE
                val seconds = timeMillis % ONE_MINUTE
                val duration = Duration.days(day).plus(Duration.Companion.hours(hour))
                    .plus(Duration.minutes(minute)).plus(Duration.seconds(seconds))
                LetsDurationFormat(locale).format(duration,smallestUnit)
            }
        }
    }

    private fun generateNo(time:Long):String{
        return if (time < 10) "0$time" else "$time"
    }

    private const val ONE_MINUTE = 60 * 1000
    private const val ONE_HOUR = ONE_MINUTE * 60
    private const val ONE_DAY = ONE_HOUR * 24
    private const val ONE_WEAK = ONE_DAY * 7
}