package com.letsfun.letswidget.lib.core.storage

import android.content.Context
import com.letsfun.letswidget.lib.core.utils.CryptoUtils
import com.letsfun.letswidget.lib.core.utils.SpUtils

class AndroidSsp : BaseSsp {
    private var spUtils: SpUtils

    constructor(context: Context) : super(context) {
        spUtils = SpUtils(context)
    }

    constructor(context: Context?, spName: String?) : super(context) {
        spUtils = SpUtils(context, spName)
    }

    override val name: String?
        get() {
            return spUtils.defName
        }

    override fun putBoolean(key: String, value: Boolean) {
        spUtils.putBoolean(CryptoUtils.md5(key), value)
    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return spUtils.getBoolean(CryptoUtils.md5(key), defaultValue)
    }

    override fun putLong(key: String, value: Long) {
        spUtils.putLong(CryptoUtils.md5(key), value)
    }

    override fun getLong(key: String, defaultValue: Long): Long {
        return spUtils.getLong(CryptoUtils.md5(key), defaultValue)
    }

    override fun putInt(key: String, value: Int) {
        spUtils.putInt(CryptoUtils.md5(key), value)
    }

    override fun getInt(key: String, defaultValue: Int): Int {
        return spUtils.getInt(CryptoUtils.md5(key), defaultValue)
    }

    override fun putString(key: String?, value: String?) {
        spUtils.putString(CryptoUtils.md5(key), value)
    }

    override fun getString(key: String, defaultValue: String?): String? {
        return spUtils.getString(CryptoUtils.md5(key), defaultValue)
    }

    override fun contains(key: String): Boolean {
        return spUtils.contains(CryptoUtils.md5(key))
    }

    override val all: Map<String, *>
        get() {
            return spUtils.all
        }

    override fun onRemove(key: String?) {
        spUtils.remove(CryptoUtils.md5(key))
        spUtils.remove(key)
    }

    override fun onClear() {
        spUtils.clear()
    }
}