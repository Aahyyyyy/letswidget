package com.letsfun.letswidget.lib.core.locale

import android.annotation.SuppressLint
import android.content.Context
import android.icu.text.DisplayContext
import android.icu.text.RelativeDateTimeFormatter
import android.icu.text.SimpleDateFormat
import android.icu.util.ULocale
import androidx.annotation.Keep
import com.letsfun.letswidget.lib.core.AppContext
import com.letsfun.letswidget.lib.core.storage.SecureSp
import com.letsfun.letswidget.lib.core.storage.SspType
import java.text.*
import java.util.*
import kotlin.time.ExperimentalTime

/**
 * getLocale获取用户语言设置，后缀不代表用户地理位置
 * country代表用户地理位置
 * 详细的需求/设计/使用说明 参考https://nio.feishu.cn/docs/doccn5biubUguvo8bR55vPipTzL
 */
@Keep
object LetsLocaleHelper {
    //根据项目进行修改，必须在可支持的语言范围内
    var supportedLanguages = LetsLocaleConst.LETS_SUPPORTED_LANGUAGES

    @LetsLangCode
    var defaultLanguage = LetsLangCode.EN_GB

    //各个项目app初始设置（默认设置系统country）
    var defaultRegion: String = Locale.getDefault().country

    /**
     * 内存缓存，language,region和city，用于提高效率，防止每次从ssp取
     */
    @LetsLangCode
    private var currentLanguage: String? = null

    private var currentRegionCode: String? = null
    private var currentCityCode: String? = null
    private var currentCityName: String? = null

    private const val SSP_NAME = "language_config"
    private const val KEY_LANGUAGE = "language"
    private const val KEY_REGION = "region"
    private const val KEY_CITY_CODE = "city_code"
    private const val KEY_CITY_NAME = "city_name"
    private val ssp by lazy {
        SecureSp[SspType.ANDROID_SSP, SSP_NAME]
    }

    /**
     * 获取向用户展示的语言标识
     */
    fun getDisplayLanguage(): String? {
        return LetsLocaleConst.DISPLAY_NAME_MAP[getCurrentLangCode()]
            ?: Locale.getDefault().displayLanguage
    }

    /**
     * 根据语言CODE获取向用户展示的语言标识
     */
    fun getDisplayLanguage(@LetsLangCode languageCode: String): String {
        return LetsLocaleConst.DISPLAY_NAME_MAP[languageCode]!!
    }

    /**
     * 根据语言CODE获取系统Locale
     */
    fun getLocale(@LetsLangCode langCode: String): Locale {
        return LetsLocaleConst.LOCALE_MAP[langCode]!!
    }

    /**
     * 持久化存储语言
     * @param langCode 带有地区的语言标识，传null为跟随系统
     */
    fun saveLanguage(@LetsLangCode langCode: String) {
        //只有在支持语言列表里面才允许存储
        if (supportedLanguages.contains(langCode)) {
            currentLanguage = langCode
            ssp.apply {
                putString(KEY_LANGUAGE, langCode)
            }
        } else {
            if (AppContext.isDebug()) {
                throw IllegalStateException("Language $langCode is not supported")
            }
        }
    }

    /**
     * 持久化存储region
     * @param regionCode，地区码
     */
    fun saveRegion(regionCode: String) {
        currentRegionCode = regionCode
        ssp.apply {
            putString(KEY_REGION, regionCode)
        }
    }

    /**
     * 持久化存储cityCode
     * @param cityCode,当前城市code
     */
    fun saveCityCode(code: String) {
        currentCityCode = code
        ssp.apply {
            putString(KEY_CITY_CODE, currentCityCode)
        }
    }

    /**
     * 持久化存储cityName
     * @param name,当前城市name
     */
    fun saveCityName(name: String) {
        currentCityName = name
        ssp.apply {
            putString(KEY_CITY_NAME, currentCityName)
        }
    }

    /**
     * 是否是当前项目支持的语言
     */
    fun isSupported(langCode: String): Boolean {
        return supportedLanguages.contains(langCode)
    }

    /**
     * 获取不带地区的语言标识，例如en
     */
    @Deprecated("使用带有地区的语言标识")
    fun getLanguageSimple(): String? {
        return getCurrentLangCode().replace("_", "-").split("-").let { it[0] }
    }

    /**
     * 获取符合NIO规范的语言标识，例如en-GB
     * 搜索顺序：SP设置 -> 系统语言匹配
     */
    @LetsLangCode
    fun getCurrentLangCode(): String {
        return currentLanguage ?: let {
            currentLanguage = ssp.getString(KEY_LANGUAGE)?.takeIf { it.isNotEmpty() }
                ?: findOptimalLangCode(Locale.getDefault().language)
            currentLanguage ?: defaultLanguage
        }
    }

    /**
     * 找出当前项目支持的最接近的语言
     */
    @LetsLangCode
    fun findOptimalLangCode(language: String): String {
        var lang = defaultLanguage
        if (supportedLanguages.contains(language)) {
            lang = language
        } else {
            supportedLanguages.filter {
                it.contains(language.substring(0, 2))
            }.forEach { lang = it }
        }
        if (AppContext.isDebug() && !supportedLanguages.contains(lang)) {
            throw IllegalStateException("Default language $lang is not supported")
        }
        return lang
    }

    /**
     * 获取用户区域，poi->用户手机
     */
    fun getCurrentRegionCode(): String {
        return currentRegionCode ?: let {
            currentRegionCode =
                ssp.getString(KEY_REGION)?.takeIf { it.isNotEmpty() } ?: defaultRegion
            currentRegionCode!!
        }
    }

    /**
     * 获取当前城市的cityCode，可能为空，defaultCity未定
     */
    fun getCurrentCityCode(): String? {
        return currentCityCode ?: let {
            currentCityCode = ssp.getString(KEY_CITY_CODE)
            currentCityCode
        }
    }

    /**
     * 获取当前城市的cityName，可能为空，defaultCity未定
     */
    fun getCurrentCityName(): String? {
        return currentCityName ?: let {
            currentCityName = ssp.getString(KEY_CITY_NAME)
            currentCityName
        }
    }

    /**
     *获取当前国家对应语言的name
     */
    fun getRegionName(
        langCode: String = getCurrentLangCode(),
        regionCode: String = getCurrentRegionCode()
    ): String? {
        return Locale(langCode).getDisplayCountry(Locale(langCode, regionCode))
    }

    @Deprecated("Use getCurrentLocale", ReplaceWith("getCurrentLocale"))
    fun getLocale(): Locale {
        val langFull = getCurrentLangCode()!!.replace("_", "-").split("-")
        return Locale(langFull[0], langFull[1])
    }

    /**
     * 通过language确定的locale
     */
    fun getCurrentLocale(): Locale {
        return getLocale(getCurrentLangCode())
    }

    private const val ONE_MINUTE = 60 * 1000
    private const val ONE_HOUR = ONE_MINUTE * 60
    private const val ONE_DAY = ONE_HOUR * 24
    private const val ONE_WEAK = ONE_DAY * 7

    /**
     * 绝对时间，用于订单日期显示
     */
    fun formatDate(timeMillis: Long, locale: Locale = getCurrentLocale()): String {
        return DateFormat.getDateTimeInstance(
            DateFormat.SHORT, DateFormat.SHORT, locale
        ).format(Date(timeMillis))
    }

    /**
     * 绝对时间，
     * @param timeMillis 当前时间戳
     * @param locale
     * @param formatStyle 可以指定时间风格，short、long、full等
     */
    fun formatDate(
        timeMillis: Long,
        locale: Locale = getCurrentLocale(),
        formatStyle: Int = DateFormat.LONG
    ): String {
        return DateFormat.getDateTimeInstance(
            formatStyle, formatStyle, locale
        ).format(Date(timeMillis))
    }

    /**
     * date和time本地格式化方法
     * @param timeMillis 当前时间戳
     * @param locale
     * @param format 无需关心顺序和当地的格式，只需指定：yyyyMMdd、MMMdd、MMdd、yyyyMM、yyyyMMddHHmm、MMMddHHmm等等
     * 会自动转换成当地的时间格式
     * 比如:yyyyMMdd在各国的表现
     *  中：2022/08/16
     *  英：16/08/2022
     *  挪威：16.08.2022
     *  丹麦：16.08.2022
     *  瑞典：2022-08-16
     *  德国：16.08.2022
     */
    @SuppressLint("NewApi")
    fun localizedDateFormat(
        timeMillis: Long,
        format: String,
        locale: Locale = getCurrentLocale()
    ): String {
        return SimpleDateFormat(
            android.text.format.DateFormat.getBestDateTimePattern(locale, format),
            locale
        ).format(
            Date(
                timeMillis
            )
        )
    }

    /**
     * @param timeMillis 需要转换的时间，单位：ms
     * @param locale 本地化
     * @param durationToken ,分为唯一单位：比如天、时、分钟、秒；拼接单位：比如2天4小时
     * @param smallestUnit ,拼接单位时，传入最小单位，比如传时，就是2天4小时，传分钟就是2天4小时5分钟
     */
    @OptIn(ExperimentalTime::class)
    fun localizeDurationFormat(
        timeMillis: Long,
        locale: Locale= getCurrentLocale(),
        @LetsDurationToken durationToken: String = LetsDurationToken.TIME_DURATION_SPLICE,
        smallestUnit: LetsDurationFormat.Unit = LetsDurationFormat.Unit.SECOND
    ):String?{
        return LetsLocaleDurationImpl.localizedDurationTime(timeMillis, locale, durationToken, smallestUnit)
    }

    /**
     * 格式化货币
     * @param number        金额
     * @param currencyCode  币种
     * @param locale        语言
     * @param maxDigits     最大保留有效数
     * @param minDigits     最小保留有效数
     *
     * 若最大、最小保留有效数之前有0，则自动抹去。如maxDigits=5,minDigits=1,12.34000001格式化
     * 后的结果为 12.34
     */
    fun formatCurrency(
        number: Any,
        currencyCode: String,
        locale: Locale = getLocale(),
        maxDigits: Int = 2,
        minDigits: Int = 2
    ): String {
        // 处理部分机型可能出现的中文单杠场景
        if (currencyCode == "CNY" && (locale == Locale.CHINA || locale == Locale.CHINESE || locale == Locale.SIMPLIFIED_CHINESE)) {
            return formatCNCurrency(number, maxDigits, minDigits)
        }
        return NumberFormat.getCurrencyInstance(locale).apply {
            currency = Currency.getInstance(currencyCode)
            maximumFractionDigits = maxDigits
            minimumFractionDigits = minDigits
        }.format(number)
    }

    fun formatCNCurrency(
        number: Any,
        maxDigits: Int = 2,
        minDigits: Int = 2
    ): String {
        return NumberFormat.getCurrencyInstance(Locale.TRADITIONAL_CHINESE).apply {
            currency = Currency.getInstance("CNY")
            maximumFractionDigits = maxDigits
            minimumFractionDigits = minDigits
        }.format(number).replace("CN", "")
    }

    fun formatNumber(
        number: Any,
        locale: Locale = getLocale(),
        maxDigits: Int = 2,
        minDigits: Int = 2,
    ): String {
        return NumberFormat.getInstance(locale).apply {
            maximumFractionDigits = maxDigits
            minimumFractionDigits = minDigits
        }.format(number)
    }

    private fun removeTailZero(str: String): String {
        var index = 0
        val len = str.length
        while (str[len - index - 1] == '0') {
            index++
        }
        return str.subSequence(0, len - index).toString().let {
            if (it.endsWith(".") || it.endsWith(",")) {
                it.subSequence(0, it.length - 1).toString()
            } else {
                it
            }
        }
    }

    /**
     * 用于处理相对时间的显示类型，long or short
     */
    const val RELATIVE_SHORT = 1
    const val RELATIVE_LONG = 2

    /**
     * 获取支持本地化的相对时间
     */
    fun getRelativeDateTime(
        timeMillis: Long,
        locale: Locale = getLocale(),
        relativeType: Int = RELATIVE_SHORT
    ): String {
        return formatDateRelative(timeMillis, locale, relativeType)
    }

    @SuppressLint("NewApi")
    private fun formatDateRelative(
        timeMillis: Long,
        locale: Locale,
        relativeType: Int = RELATIVE_SHORT
    ): String {
        val now = System.currentTimeMillis()
        val diff = now - timeMillis
        val fmt: RelativeDateTimeFormatter = if (relativeType == RELATIVE_LONG) {
            RelativeDateTimeFormatter.getInstance(locale)
        } else {
            RelativeDateTimeFormatter.getInstance(
                ULocale.forLocale(locale),
                null,
                RelativeDateTimeFormatter.Style.SHORT,
                DisplayContext.CAPITALIZATION_NONE
            )
        }

        if (diff < ONE_MINUTE) {
            return fmt.format(
                RelativeDateTimeFormatter.Direction.PLAIN,
                RelativeDateTimeFormatter.AbsoluteUnit.NOW,
            )
        } else if (diff < ONE_HOUR) {
            val a = diff / ONE_MINUTE
            return fmt.format(
                a.toDouble(),
                RelativeDateTimeFormatter.Direction.LAST,
                RelativeDateTimeFormatter.RelativeUnit.MINUTES
            )
        } else if (diff < ONE_DAY) {
            val a = diff / ONE_HOUR
            return fmt.format(
                a.toDouble(),
                RelativeDateTimeFormatter.Direction.LAST,
                RelativeDateTimeFormatter.RelativeUnit.HOURS
            )
        } else if (diff < ONE_WEAK) {
            val a = diff / ONE_DAY
            return fmt.format(
                a.toDouble(),
                RelativeDateTimeFormatter.Direction.LAST,
                RelativeDateTimeFormatter.RelativeUnit.DAYS
            )
        } else {
            val timeCalendar = Calendar.getInstance().apply { timeInMillis = timeMillis }
            val nowCalendar = Calendar.getInstance().apply { timeInMillis = now }

            return if (timeCalendar[Calendar.ERA] == nowCalendar[Calendar.ERA]
                && timeCalendar[Calendar.YEAR] == nowCalendar[Calendar.YEAR]
            ) { //当年内显示MMMdd
                localizedDateFormat(timeMillis, "MMMdd", locale)
            } else {//往年显示yyyyMMdd
                localizedDateFormat(timeMillis, "yyyyMMdd", locale)
            }
        }
    }

    fun updateConfig(context: Context) {
        context.resources.apply {
            updateConfiguration(configuration.apply {
                locale = getCurrentLocale()
            }, displayMetrics)
        }
    }

    var localeHandler: OnLocaleHandler? = null

    private const val K = 1000L
    private const val BK = 100000L
    private const val M = 1000000L

    @SuppressLint("NewApi")
    fun simpleNumber(number: Long): String {

        val simpleDecimalFormatDecimal =
            DecimalFormat("#.0", DecimalFormatSymbols.getInstance(getLocale()))
        val simpleDecimalFormatInteger =
            DecimalFormat("#", DecimalFormatSymbols.getInstance(getLocale()))

        return if (number >= M) {
            "${simpleDecimalFormatDecimal.format((number * 100L / M).toFloat() / 100f)}M"
        } else if (number >= BK) {
            "${simpleDecimalFormatInteger.format(number / K)}K"
        } else if (number >= K) {
            "${simpleDecimalFormatDecimal.format((number * 100L / K).toFloat() / 100f)}K"
        } else {
            "$number"
        }
    }
}