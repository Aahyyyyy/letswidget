package com.letsfun.letswidget.lib.core.locale

import androidx.annotation.StringDef

@StringDef(
    LetsDurationToken.TIME_DURATION_DDD,
    LetsDurationToken.TIME_DURATION_HH,
    LetsDurationToken.TIME_DURATION_MM,
    LetsDurationToken.TIME_DURATION_SS,
    LetsDurationToken.TIME_DURATION_HHHMMSS,
    LetsDurationToken.TIME_DURATION_SPLICE
)
@Target(
    AnnotationTarget.TYPE_PARAMETER,
    AnnotationTarget.VALUE_PARAMETER,
    AnnotationTarget.FIELD,
    AnnotationTarget.FUNCTION
)
@Retention(AnnotationRetention.SOURCE)
annotation class LetsDurationToken {
    companion object {
        const val TIME_DURATION_DDD = "time_duration_ddd"
        const val TIME_DURATION_HH = "time_duration_hh"
        const val TIME_DURATION_MM = "time_duration_mm"
        const val TIME_DURATION_SS = "time_duration_ss"
        const val TIME_DURATION_HHHMMSS = "time_duration_hhhmmss"//一般用于视频播放进度，一般是23:30:56
        const val TIME_DURATION_SPLICE = "time_duration_splice"
    }
}