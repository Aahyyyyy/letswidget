package com.letsfun.letswidget.lib.core.locale

interface OnLocaleHandler {
    fun onCurrentLanguageCode(): String?
    fun onCurrentRegionCode(): String?
    fun onCurrentCityCode(): String?
    fun onCurrentCityName(): String?
}