package com.letsfun.letswidget.lib.core.storage

import android.content.Context
import android.util.Log

class DummySsp constructor(context: Context?) : BaseSsp(context) {
    override val name: String?
        get() {
            return "DummySsp"
        }

    public override fun putBoolean(key: String, value: Boolean) {
        Log.w(TAG, "A dummy SP operation putBoolean, key = " + key)
    }

    public override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        Log.w(TAG, "A dummy SP operation getBoolean, key = " + key)
        return false
    }

    public override fun putLong(key: String, value: Long) {
        Log.w(TAG, "A dummy SP operation putLong, key = " + key)
    }

    public override fun getLong(key: String, value: Long): Long {
        Log.w(TAG, "A dummy SP operation getLong, key = " + key)
        return 0
    }

    public override fun putInt(key: String, value: Int) {
        Log.w(TAG, "A dummy SP operation putInt, key = " + key)
    }

    public override fun getInt(key: String, defaultValue: Int): Int {
        Log.w(TAG, "A dummy SP operation getInt, key = " + key)
        return 0
    }

    public override fun putString(key: String?, value: String?) {
        Log.w(TAG, "A dummy SP operation putString, key = " + key)
    }

    public override fun getString(key: String, defaultValue: String?): String? {
        Log.w(TAG, "A dummy SP operation getString, key = " + key)
        return ""
    }

    override fun onRemove(key: String?) {
        Log.w(TAG, "A dummy SP operation onRemove, key = " + key)
    }

    override fun onClear() {
        Log.w(TAG, "A dummy SP operation onClear")
    }

    public override fun contains(key: String): Boolean {
        Log.w(TAG, "A dummy SP operation contains, key = " + key)
        return false
    }

    override val all: Map<String, *>
        get() {
            Log.w(TAG, "A dummy SP operation getAll")
            return mapOf<String, Any>()
        }

    companion object {
        private val TAG: String = "DummySsp"
    }
}