package com.letsfun.letswidget.lib.core;

import android.app.Application;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.letsfun.letswidget.lib.core.data.SSLContextProvider;
import com.letsfun.letswidget.lib.core.utils.ImageProcessor;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.SSLContext;

/**
 * 不能混淆，也不能修改为kotlin，chamber会调用
 */
@Keep
public class AppContext {
    private static Application app;
    private static boolean isProd = true;
    private static boolean isDebug = false;
    private static ConcurrentHashMap<String,Object> extraMap = new ConcurrentHashMap<>();

    @FunEnv
    private static int env = FunEnv.PROD;
    private static String appId;
    private static String appSecret;
    private static LazyInitCallback appSecretCallback;
    private static String accessToken;
    private static String ssoToken;
    private static final MutableLiveData<String> accessTokenLive = new MutableLiveData<>();
    private static final MutableLiveData<String> ssoTokenLive = new MutableLiveData<>();
    private static String userId;//实时性更好
    private static final MutableLiveData<String> userIdLive = new MutableLiveData<>();
    private static String mobile;
    private static String email;
    private static String channel;

    private static String fileProviderAuthorities;
    private static SSLContextProvider sslContextProvider;
    private static ImageProcessor imageProcessor;
    private static ParametersProvider commonParametersProvider;

    public static String getChannel() {
        return channel;
    }

    public static void setChannel(String channel) {
        AppContext.channel = channel;
    }

    public interface LazyInitCallback {
        String getData();
    }

    public interface ParametersProvider {
        Map<String, String> provide();
    }

    @NonNull
    public static Application getApp() {
        return app;
    }

    public static void setApp(Application app) {
        AppContext.app = app;
    }

    @NonNull
    public static boolean isDebug() {
        return isDebug;
    }

    public static void setDebug(boolean value) {
        isDebug = value;
    }

    @NonNull
    public static boolean isProd() {
        return isProd;
    }

    public static void setProd(boolean prod) {
        isProd = prod;
    }

    public static void setEnv(@FunEnv int value) {
        env = value;
    }

    @NonNull
    @FunEnv
    public static int getEnv() {
        return env;
    }

    @NonNull
    public static String getAppId() {
        return appId;
    }

    public static void setAppId(String value) {
        appId = value;
    }

    public static String getAppSecret() {
        if (appSecret == null) {
            if (appSecretCallback != null) {
                appSecret = appSecretCallback.getData();
            }
        }
        return appSecret;
    }

    public static void setCustomValue(@NonNull String key,@Nullable Object value){
        extraMap.put(key,value);
    }

    @Nullable
    public static Object getCustomValue(@NonNull String key){
        return extraMap.get(key);
    }

    public static void setAppSecret(String appSecret) {
        AppContext.appSecret = appSecret;
    }

    /**
     * 使用时再初始化appSecret，支持解密
     *
     * @param appSecretCallback
     */
    public static void setAppSecretLazy(LazyInitCallback appSecretCallback) {
        AppContext.appSecret = null;
        AppContext.appSecretCallback = appSecretCallback;
    }

    @Nullable
    public static String getAccessToken() {
        return accessToken;
    }

    public static void setAccessToken(String accessToken) {
        AppContext.accessToken = accessToken;
        accessTokenLive.postValue(accessToken);
    }

    public static MutableLiveData<String> getAccessTokenLiveData() {
        return accessTokenLive;
    }

    @Nullable
    public static String getSsoToken() {
        return ssoToken;
    }

    public static void setSsoToken(String ssoToken) {
        AppContext.ssoToken = ssoToken;
        ssoTokenLive.postValue(ssoToken);
    }

    @NonNull
    public static MutableLiveData<String> getSsoTokenLiveData() {
        return ssoTokenLive;
    }


    public static String getUserId() {
        return userId;
    }

    @Nullable
    public static void setUserId(String userId) {
        AppContext.userId = userId;
        userIdLive.postValue(userId);
    }

    public static MutableLiveData<String> getUserIdLiveData() {
        return userIdLive;
    }

    @Nullable
    public static String getMobile() {
        return mobile;
    }

    public static void setMobile(String mobile) {
        AppContext.mobile = mobile;
    }

    @Nullable
    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        AppContext.email = email;
    }

    @Nullable
    public static String getFileProviderAuthorities() {
        return fileProviderAuthorities;
    }

    public static void setFileProviderAuthorities(String fileProviderAuthorities) {
        AppContext.fileProviderAuthorities = fileProviderAuthorities;
    }

    public static void setSslContextProvider(SSLContextProvider provider) {
        sslContextProvider = provider;
    }

    @Nullable
    public static SSLContext getSslContext() {
        if (sslContextProvider != null) {
            return sslContextProvider.provide();
        }
        return null;
    }

    public static ImageProcessor getImageProcessor() {
        return imageProcessor;
    }

    public static void setImageProcessor(ImageProcessor imageProcessor) {
        AppContext.imageProcessor = imageProcessor;
    }

    public static void setCommonParametersProvider(ParametersProvider provider) {
        AppContext.commonParametersProvider = provider;
    }

    @NonNull
    public static Map<String, String> getCommonParameters() {
        return AppContext.commonParametersProvider.provide();
    }
}
