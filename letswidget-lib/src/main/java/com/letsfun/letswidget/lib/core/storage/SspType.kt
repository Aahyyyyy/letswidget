package com.letsfun.letswidget.lib.core.storage

import androidx.annotation.Keep
import androidx.annotation.StringDef

@Keep
@StringDef(SspType.DUMMY_SSP, SspType.ANDROID_SSP)
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
annotation class SspType {
    companion object {
        const val DUMMY_SSP: String = "DUMMY_SSP"
        const val ANDROID_SSP: String = "ANDROID_SSP"
    }
}