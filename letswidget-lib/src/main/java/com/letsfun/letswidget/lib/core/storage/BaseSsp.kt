package com.letsfun.letswidget.lib.core.storage

import android.content.Context
import android.text.TextUtils
import com.google.gson.reflect.TypeToken
import com.letsfun.letswidget.lib.core.utils.JsonUtils
import com.letsfun.letswidget.lib.core.utils.KeyStoreUtils
import java.security.GeneralSecurityException
import java.util.concurrent.ConcurrentHashMap

abstract class BaseSsp protected constructor(context: Context?) {
    private val plainToCipherMap: MutableMap<String?, String?> = ConcurrentHashMap() //写缓存，解决跨进程问题
    private val cipherToPlainMap: MutableMap<String?, String?> = ConcurrentHashMap() //读缓存
    abstract val name: String?
    abstract fun putBoolean(key: String, value: Boolean)
    abstract fun getBoolean(key: String, defaultValue: Boolean): Boolean
    abstract fun putLong(key: String, value: Long)
    abstract fun getLong(key: String, defaultValue: Long): Long
    abstract fun putInt(key: String, value: Int)
    abstract fun getInt(key: String, defaultValue: Int): Int
    abstract fun putString(key: String?, value: String?)
    abstract fun getString(key: String, defaultValue: String?): String?
    protected abstract fun onRemove(key: String?)
    protected abstract fun onClear()
    fun getBoolean(key: String): Boolean {
        return getBoolean(key, false)
    }

    fun getLong(key: String): Long {
        return getLong(key, 0)
    }

    fun getInt(key: String): Int {
        return getInt(key, 0)
    }

    fun getString(key: String): String? {
        return getString(key, "")
    }

    @Synchronized
    fun putEncryptString(key: String?, value: String?) {
        if (TextUtils.isEmpty(key)) {
            return
        }
        var encryptValue: String? = value
        if (value != null) {
            encryptValue = plainToCipherMap[value]
            if (encryptValue == null) {
                encryptValue = try {
                    KeyStoreUtils.get().encryptWithSecureAes(value)
                } catch (e: GeneralSecurityException) {
                    e.printStackTrace()
                    value
                }
            }
            plainToCipherMap[value] = encryptValue
            cipherToPlainMap[encryptValue] = value
        }
        putString(key, encryptValue)
    }

    @Synchronized
    fun getEncryptString(key: String): String? {
        if (TextUtils.isEmpty(key)) {
            return ""
        }
        var plainValue: String?
        val cipherValue: String? = getString(key)
        if (!TextUtils.isEmpty(cipherValue)) {
            plainValue = cipherToPlainMap[cipherValue]
            if (plainValue == null) {
                try {
                    plainValue = KeyStoreUtils.get().decryptWithSecureAes(cipherValue)
                    cipherToPlainMap[cipherValue] = plainValue
                } catch (e: GeneralSecurityException) {
                    e.printStackTrace()
                    cipherToPlainMap.remove(cipherValue)
                }
            }
        } else {
            plainValue = cipherValue
        }
        return plainValue
    }

    fun putObject(key: String?, value: Any?) {
        val jsonValue: String? = JsonUtils.toJson(value)
        putString(key, jsonValue)
    }

    fun <T> getObject(key: String, clazz: Class<T>?): T? {
        val jsonValue: String? = getString(key)
        try {
            return JsonUtils.fromJson(jsonValue, clazz)
        } catch (e: Exception) {
            remove(key)
            return null
        }
    }

    fun <T> getObject(key: String, typeOfT: TypeToken<T>): T? {
        val jsonValue: String? = getString(key)
        try {
            return JsonUtils.fromJson(jsonValue, typeOfT.getType())
        } catch (e: Exception) {
            remove(key)
            return null
        }
    }

    fun putEncryptObject(key: String?, value: Any?) {
        val jsonValue: String? = JsonUtils.toJson(value)
        putEncryptString(key, jsonValue)
    }

    fun <T> getEncryptObject(key: String, clazz: Class<T>?): T? {
        val jsonValue: String? = getEncryptString(key)
        try {
            return JsonUtils.fromJson(jsonValue, clazz)
        } catch (e: Exception) {
            remove(key)
            return null
        }
    }

    fun <T> getEncryptObject(key: String, typeOfT: TypeToken<T>): T? {
        val jsonValue: String? = getEncryptString(key)
        try {
            return JsonUtils.fromJson(jsonValue, typeOfT.getType())
        } catch (e: Exception) {
            remove(key)
            return null
        }
    }

    abstract operator fun contains(key: String): Boolean
    abstract val all: Map<String, *>?
    fun remove(key: String?) {
        onRemove(key)
    }

    fun clear() {
        plainToCipherMap.clear()
        cipherToPlainMap.clear()
        onClear()
    }
}