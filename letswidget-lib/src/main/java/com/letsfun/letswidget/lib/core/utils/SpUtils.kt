package com.letsfun.letswidget.lib.core.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils

@SuppressLint("NewApi")
class SpUtils {
    private var sharedPreferences: SharedPreferences? = null
    var defName: String? = null
        private set

    private constructor() {}
    constructor(context: Context) {
        sharedPreferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE)
        defName = NAME
    }

    constructor(context: Context?, name: String?) {
        var name: String? = name
        if (TextUtils.isEmpty(name)) {
            name = NAME
        }
        sharedPreferences = context!!.getSharedPreferences(name, Context.MODE_PRIVATE)
        defName = name
    }

    fun isNameEquals(name: String): Boolean {
        return defName != null && (defName == name)
    }

    @Synchronized
    fun putBoolean(key: String?, value: Boolean) {
        sharedPreferences!!.edit().putBoolean(key, value).apply()
    }

    fun getBoolean(key: String?): Boolean {
        return sharedPreferences!!.getBoolean(key, false)
    }

    fun getBoolean(key: String?, defaultValue: Boolean): Boolean {
        return sharedPreferences!!.getBoolean(key, defaultValue)
    }

    @Synchronized
    fun putLong(key: String?, value: Long) {
        sharedPreferences!!.edit().putLong(key, value).apply()
    }

    fun getLong(key: String?): Long {
        return sharedPreferences!!.getLong(key, 0)
    }

    fun getLong(key: String?, defaultValue: Long): Long {
        return sharedPreferences!!.getLong(key, defaultValue)
    }

    @Synchronized
    fun putInt(key: String?, value: Int) {
        sharedPreferences!!.edit().putInt(key, value).apply()
    }

    fun getInt(key: String?): Int {
        return getInt(key, 0)
    }

    fun getInt(key: String?, defaultValue: Int): Int {
        return sharedPreferences!!.getInt(key, defaultValue)
    }

    @Synchronized
    fun putString(key: String?, value: String?) {
        if (TextUtils.isEmpty(key)) {
            return
        }
        if (value != null) {
            val saveValue: String? = getString(key)
            if ((value == saveValue)) {
                return
            }
        }
        sharedPreferences!!.edit().putString(key, value).apply()
    }

    fun getString(key: String?): String? {
        return getString(key, "")
    }

    fun getString(key: String?, defaultValue: String?): String? {
        return sharedPreferences!!.getString(key, defaultValue)
    }

    @Synchronized
    operator fun contains(key: String?): Boolean {
        if (sharedPreferences != null && !TextUtils.isEmpty(key)) {
            return sharedPreferences!!.contains(key)
        }
        return false
    }

    val all: Map<String, *>
        get() {
            return sharedPreferences!!.getAll()
        }

    @Synchronized
    fun remove(key: String?) {
        sharedPreferences!!.edit().remove(key).apply()
    }

    @Synchronized
    fun clear() {
        sharedPreferences!!.edit().clear().apply()
    }

    companion object {
        private val TAG: String = "SpUtils"
        private val NAME: String = "com_nio_sp"
    }
}