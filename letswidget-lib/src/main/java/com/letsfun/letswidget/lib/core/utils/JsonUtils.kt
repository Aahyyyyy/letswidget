package com.letsfun.letswidget.lib.core.utils

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.lang.reflect.Type

object JsonUtils {
    private val TAG: String = "JsonUtil"
    private val GSON: Gson =
        GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

    fun toJson(`object`: Any?): String {
        if (`object` == null) {
            return ""
        }
        return GSON.toJson(`object`)
    }

    fun <T> fromJson(json: String?, clazz: Class<T>?): T {
        try {
            return GSON.fromJson(json, clazz)
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }

    fun <T> fromJson(json: String?, typeOfT: Type?): T {
        try {
            return GSON.fromJson(json, typeOfT)
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }

    @Throws(JSONException::class)
    fun toJsonObject(json: String?): JSONObject? {
        try {
            return JSONObject(json)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    fun toGsonObject(json:String?):JsonObject?{
        try {
            return JsonParser.parseString(json).asJsonObject
        }catch (e:Exception){
            e.printStackTrace()
        }
        return null
    }

    fun objectToMap(`object`: Any?): Map<String, Any?> {
        val result: MutableMap<String, Any?> = HashMap()
        if (`object` == null) {
            return result
        }
        val json: String = toJson(`object`)
        val parse: Map<String, Any?>? = fromJson<Map<String, Any?>>(
            json,
            object : TypeToken<HashMap<String?, Any?>?>() {}.getType()
        )
        if (parse != null) {
            for (entry: Map.Entry<String, Any?> in parse.entries) {
                if (entry.value != null) {
                    result.put(entry.key, entry.value)
                }
            }
        }
        return result
    }
}