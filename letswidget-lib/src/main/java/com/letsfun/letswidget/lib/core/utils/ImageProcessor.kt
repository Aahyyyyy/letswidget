package com.letsfun.letswidget.lib.core.utils

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView

open interface ImageProcessor {
    open interface IImageProcessorResult<T> {
        fun onResult(t: T)
    }

    fun init()
    fun loadImage(context: Context?, url: String?, result: IImageProcessorResult<Bitmap?>?)
    fun loadImage(view: ImageView?, url: String?)
    fun loadImage(
        view: ImageView?,
        url: String?,
        scaleType: ImageView.ScaleType?,
        showHolder: Boolean,
        keepOriginal: Boolean
    )
}